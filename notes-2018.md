### 31/10, meeting
* Experiments do not have a fixed length. Successful experiments can last up to 8 to 12 hours, but they can be shorter as well. If something goes wrong during experiments, then they are restarted as soon as possible (by re-injecting the particles). In the data, you will then see short consecutive bursts of usage of the machine.
* The beginning of IPOC data could be used as a label for the beginning experiments.
* LHC schedule: Technical Stop (TS) 2 or 3 times a year. Machines are tested in these periods.

### 01/11
To get a list of logbook entries with a certain tag in MongoDB:
```
db.getCollection('MKI.ELOGBOOK_tagged').aggregate(
	{$unwind: '$values'},
	{$match: { 'values.TAG': 'anomaly' } }
)
```
To find all logbook entries between certain dates does not work. EVENTDATE is stored as a string instead of a Date object. => Can do this in a python function if it doesn't exist yet. (DONE)
```
db.getCollection('MKI.ELOGBOOK_tagged').aggregate(
{$unwind: '$values'},
{$match: { 'values.EVENTDATE': {
    $gte: new Date("2000-01-01"),
    $lt: new Date("2020-01-01")} } }
)
```

MKI_data_info.pdf contains a section 'Data filtering'. What are those filters based on? What does it mean if a temperature goes above 120 degrees? Is this an anomaly, or is this a wrong reading by a sensor?
=> These are wrong readings. Just discard those data points. This can be done automatically by editing the query_collection function by adding something like an 'apply_filters' function call. (DONE, filter_extremes)

### 07/11, meeting
* Don't waste too much time on reading all of the work done in the notebooks. Feature extraction is important but all the different experiments on GMM are not -> skim through these.
* Try to create a new notebook as soon as possible and try to apply Density Estimation Trees.
* Check out if Wavelet transform could be useful (see if this has been done by Niels).

### 12/11

#### Latent variable models
Inference in latent variable models - Technical University of Munich:
* https://www.youtube.com/watch?v=ocQCJtNq3P4,
* https://www.youtube.com/watch?v=MBr47eM4hf0,
* https://www.youtube.com/watch?v=lG1AIqIEv9I,
* https://www.youtube.com/watch?v=C8ShaO5bf-E

Example latent factors in an image of a digit: position on canvas, rotation, skewness
Latent variable in mnist example: z in {0, ..., 9} indicates the digit

> Latent variables z are often interpreted as the 'quintessential information'. The posterior _p_(**z** | **x**, theta) then is the holy grail of many areas in machine learning.

A lot of supervised techniques just learn this posterior distribution from the data.
The posterior is hard because we need to specify the latent factors and also specify how they influence the observables (the images for the mnist example). Solving the integral for the posterior in the example was easy because **z** only represented the digit in the image => should use more than that (rotation, skewness, ...) => harder integral and need to add a lot of hardcoded information about how the latent variables influence the observables => too hard.

The posterior _p_ is a function. We can approximate this function using a neural network. In practise, you have an NN which takes **x** as input. As output, which is parametrized by phi (distinct form theta), you get a mean **mu** and a covariance matrix **sigma**. Then you sample **z** from a distribution with these parameters.  
We can use two NNs that work in opposite directions. One maps into the latent space (encodes the data), one maps back to the observables (decoder). Can use these to create "variational auto-encoder" for unsupervised learning.  
Latent variables can cluster into groups in the latent space.

---
[Latent variable models - Neil Lawrence:](https://www.youtube.com/watch?v=nBuzHJXovl0)

PCA as a model and as an algorithm:
> PCA is like 4 different things. It's (1) maximum variance directions, (2) it's a latent variable model, (3) it's classical multi-dimensional scaling, (4) it's the optimal linear representation under linear reconstruction. Which of those 4 things did you want to do?
>
> It is some amazing general model if your world is linear, but when your world becomes linear, it changes. It's so general that people become obsessed with it as an algorithm, not as a model. Separate model and algorithm.

This lecture assumes PCA is a mapping from some latent variables to some data, and that it's a linear mapping. Then you can integrate over those latent variables and see what the probabilistic model is. => It turned out that it doesn't matter whether you choose to integrate over the mapping or integrate over the data. => The mapping can be replaced by a Gaussian process.

* q = dimension of latent space.  
    We observe high dimensional space Y, but we believe that Y is influenced by a low dimensional space X of control variables q.
* p = dimension of data space
* n = number of data points

From latent space to data point = generative model.  
Want mapping of latent space to data space. e.g. generate a digit based on certain rotation, line width, ...

Let's say the mapping is non-linear. Then from each **q** we can generate a certain **p**. But for each **p**, there could be multiple **q** which map to it. Mapping is non-bijective.

Rest of lecture is about proofs for doing the PCA. [Following lecture: latent force modeling](https://www.youtube.com/watch?v=yBRqPhCj8EM). A case where these latent control variables are going to be Gaussian process functions, things that move over time and drive differential equations.  
Interesting point where the model learns physics and can be used to generate new motions that were never seen before in the data: https://www.youtube.com/watch?v=yBRqPhCj8EM&feature=youtu.be&t=33m14s.
> That's a combination of a physical assumption about what's generating this data and the data itself to give us a prediction of what should happen if we reverse the force. It's not smoothness, it's a different type of universal regularity that applies sometimes, and thats what i think we should be putting in models to improve predictions.

##### Relevance to this thesis
Learn some low dimensional model of "normal behavior" in the latent space. Use this model to make predictions in the data space. If the next measurement set differs from the prediction made, then that timestamp can be marked as anomalous. I guess you don't really have interpretability in this case, but getting a well working model was more important than interpretability. Maybe the latent variable model could be explained in some way, I'm not too sure about that.

#### Log-likelihood
Natural logarithm of the likelihood function. More convenient to work with:
* Monotonic transformation. Extrema are still at the same locations.
* Easier to take the derivative of a log of a product than it is of the product itself.
  * log(x^n) = n log(x)
  * ln(exp(x)) = x
  * log( mult_i( P( _xi_ ) ) ) = sum_i( log(P( _xi_ )) )
* The likelihood would probably underflow. Log-likelihood will be a reasonable number. => Numerically stable

> Finding the maximum of a function often involves taking the derivative of a function and solving for the parameter being maximized, and this is often easier when the function being maximized is a log-likelihood rather than the original likelihood function.
>
> -- Likelihood function: <cite>https://en.wikipedia.org/wiki/Likelihood_function#Log-likelihood</cite>


> A sum is a lot easier to optimise as the derivatives of a sum is the sum of derivatives. If we had a sum instead of a product, we could load one datum at a time compute its partial derivatives, accumulating those gradients and apply the optimisation step at the end. Solving the problem of memory space!
>
> ![argmax equality](https://cdn-images-1.medium.com/max/800/1*bbK8J7PF_3LS8tOw3TtFtQ.png)
>
> -- <cite>ML notes: Why the log-likelihood?: https://blog.metaflow.fr/ml-notes-why-the-log-likelihood-24f7b6c40f83</cite>

### 15/11

#### meeting
* The interpretability problem last year was related to the low recall. When the model gave strange/unexpected results, they wanted to know why. But if we come up with a model that has much better precision and recall, interpretability get a lower priority.
* For evaluation/scoring, check notebooks near the end.
* 12 hour window for entries in logbook => instead of identifying which part in the 12 hours caused the anomaly, just check whether anomalies detected in the data fit inside of the labeled windows. Or check whether or not each labeled window contains a detected anomaly.

#### Density Estimation Trees
* Unsupervised nonparametric method.
* Adaptable between and within dimensions.
* More interpretable than usual nonparametric methods.
* Piecewise constant density estimate f_N(x).
* Loss function R(t) derived from Integrated Squared Error (ISE). Gives a robust notion of the error between the estimated and true density.
* Possible to mix different feature types: continuous, categorical, and ordinal.
* Densities for different feature types are calculated in different ways for a leaf l of the tree:
  * Continuous: volume (V_l_d) of leaf l within d dimensional bounding box of S.
  * Ordinal: range of ordinal values in the ordinal dimensions present in l.
  * Categorical: number of categories present in l for the categorical dimensions present in l.
* Areas with fast changes around the peak have very small partitions, while areas with slow changes above 0 have large partitions.
* The importance of dimensions can easily be read from the tree. => implicit feature selection.
* To find outliers, only need check large leaf nodes with low density.
* Problem: discontinuities along the borders of different partitions.
* **Bigger Problem:** No implementation in scikit-learn.

### 21/11, meeting

* Niels's evaluation method wasn't completely finished last year => check and update it. (DONE, completely reworked it from scratch)
* Isolation forest, fit/predict and plot with following inputs:
  * raw data
  * filtered data
  * with Niels' features
  * segment data into pieces where LHC is on (BEAM_INTENSITY) and add time since start of segments as feature
  * parameter tuning later
* Keep these plots as they will be useful for comparing different inputs/features/parameters and might be useful in the thesis text.
* For better visualisation, check Seaborne. Need better visualisation methods because plotting the whole year makes it hard to check specific data points and plotting per month will cause many images.

### 27/11

Anomaly points detected by Isolation Forest can be added to a PCA plot to get a different view.
=> plot/anomalies.pca_anomalies

#### Niels' features for anomaly detection
* All continuous data sampled to 1 second
* Each data point becomes a triple of (data, SW_1, SW_2), where
  * SW_1 = sliding window feature 1 = avg value of window
  * SW_2 = sliding window feature 2 = sum of window
* Window size was chosen to be 30min after checking average length of IPOC segments in a histogram.
* Then, IPOC data was added to this data and then all timestamps where no IPOC data was present was filtered out of the continuous data points.
  * Only learn on data when experiments are being done.
* Then, all features were normalized to make them have the same impact.
  * what about feature importance? (should not be impacted by normalization)

#### Overfitting in random forests
> To avoid over-fitting in random forest, the main thing you need to do is optimize a tuning parameter that governs the number of features that are randomly chosen to grow each tree from the bootstrapped data. Typically, you do this via k-fold cross-validation, where k∈{5,10}, and choose the tuning parameter that minimizes test sample prediction error. In addition, growing a larger forest will improve predictive accuracy, although there are usually diminishing returns once you get up to several hundreds of trees.
> https://stats.stackexchange.com/a/112026

> Relative to other models, Random Forests are less likely to overfit but it is still something that you want to make an explicit effort to avoid. Tuning model parameters is definitely one element of avoiding overfitting but it isn't the only one. In fact I would say that your training features are more likely to lead to overfitting than model parameters, especially with a Random Forests. So I think the key is really having a reliable method to evaluate your model to check for overfitting more than anything else, which brings us to your second question.
>
>As alluded to above, running cross validation will allow to you avoid overfitting. Choosing your best model based on CV results will lead to a model that hasn't overfit, which isn't necessarily the case for something like out of the bag error. The easiest way to run CV in R is with the caret package. A simple example is below:
> https://datascience.stackexchange.com/a/6430

From the Isolation Forest documentation:
* fit(X, ...): X = The input samples. Use dtype=np.float32 for maximum efficiency. Sparse matrices are also supported, use sparse csc_matrix for maximum efficiency.
=> check dtype

Discuss with Elia: train <-> test data in this situation

### 28/11, Meeting
* Before future meetings, generate new html files for notebooks so they can be viewed more easily.
* Check the ExtraTreeRegressors in Isolation Forest's estimators_ member. See how they work, plot them, ... They might be useful for feature generation.
* This week: Do Isolation Forest with Niels' features and then create plots of different feature sets for comparisons. Also create plots of GMM to compare with.
* Need to use the same input features to compare correctly with GMM.
* On the plots, add points for entries in the elogbook.

### 03/12
#### Plotting PCA example
```py
p = PCA.PCA(n_components=2)
data_pca = p.transform(data)
plot.anomalies.pca_anomalies(data_pca, most_anomalous_idx, "PCA plot with anomalies as 1 percentile")
```

Getting elogbook data is a bit different from normal data. The query method returns a dataframe of dictionaries which contain the data of event on a certain date. The dictionaries need to be unpacked into columns. This is done in mongo_client.query_elogbook

In notebook of 170302:
>data on 2016-05-02 between 10:11:00 and 10:27:00 is missing

Check this data in the future. The gap was filled in the notebook by reindexing with method 'bfill'.

#### Short-time Fourier transform
Methods related to STFT are available in fourier_transform.py. The stft method returns a 2D matrix of complex numbers (np.ndarray of np.ndarray of np.complex128).

Note in notebook 170223:
> The Fourier transform can be precise in time or in frequency, but not both. This is why every collection is tested twice.

* Window size for the transform: temporal resolution <-> frequency resolution
* Larger window: Using a longer section of time. Better resolution in frequency domain, but not able to see details that change in time as well.
* Smaller window: Increased blurring in frequency, but more precise in time.
* Can't obtain arbitrary resolution in both time and frequency.

#### Feature extraction
First usage of feature extraction in notebook 170302. See notes on 27/11 for explanations.
```py
# Generate features: sliding window mean, diff, and sum. 10 min windows
SWM_T10 = fe.sliding_window_mean(df, "T", 10)
SWMD_T10 = fe.sliding_window_mean_diff(df, "T", 10)
SWS_T10 = fe.sliding_window_sum(df, "T", 10)
features = pd.concat([df, SWM_T10, SWMD_T10, SWS_T10], axis=1)
```

#### Display entries in elogbook
```py
df_elb = client.query_elogbook(start, end)
df_elb['y'] = 0 # some constant

fig, axes = plt.subplots(nrows=1, ncols=1)
plt.plot_date(x=df_elb["EVENTDATE"], y=df_elb["y"], linestyle='none', marker='x', markersize=15, color='green')
other_dataframes.plot(ax=axes)
```

### 04/12

#### Extracting experiment lengths out of IPOC segments
* Knowing how long the average experiment is is useful for deciding the lengths of windows.
* First done in notebook 170316. The IPOC data is cut into segments based on gaps in the measurements. When there is no data for more than 10 minutes, a new segment is created. This method was discussed in one of the earlier meetings.
* Maybe BEAM_INTENSITY could be used to extract lengths of experiments? Maybe it could be used as a feature to see whether or not the LHC is in use (notes 21/11).

##### Code
```py
# http://stackoverflow.com/a/26916647
cluster = (df["timestamps"].diff() > pd.Timedelta(minutes=10)).cumsum()
dfs = [v for k, v in df.groupby(cluster)]

IPOC_lengths = []
for clust in dfs:
    if len(clust)!=1:
        IPOC_lengths.append(clust["timestamps"].iloc[-1] - clust["timestamps"].iloc[0])

IPOC_lengths = pd.Series(IPOC_lengths)
```
=> Move this to a function (DONE, segmentation.py@df_to_segments)

#### Joining IPOC data with continuous data
To join IPOC and continuous data in a way such that the result contains all IPOC timestamps (notebook 170316):
```py
joined = pd.merge(ipoc, continuous, how='left')
```

#### Retrieving anomaly scores for a certain percentile
```py
pct = np.percentile(anomaly_scores, 1)
display("1pct percentile: %d" % pct)
most_anomalous_idx = np.where((anomaly_scores < pct))
```

#### Retrieving the X most anomalous point
Assuming that the smaller the anomaly score, the more anomalous the point:
```py
# first 50 elements will be the indexes of smallest elements
most_anomalous_idx = np.argpartition(anomaly_scores, X)[:X]
```

#### Filtering NaN values out of a dataframe
```py
df = df[df != np.nan]
```
Does not work, because NaN != np.nan.
Use the following instead:
```py
df = df.dropna()
```

#### Sliding window sum feature
Last year's thesis text says that SW_SUM was used to detect whether measurements are increasing or decreasing.
> SW_MEAN vergelijkt het huidige tijdstip met het gemiddelde wat zeer lokaal is. Een som verandert meer geleidelijk, er zijn dus meerdere opeenvolgende metingen nodig die afwijken van de vorige waarden om een significant verschil in SW2 te merken.

=> How about (also) using the difference between a value and its previous value?

### 05/12, meeting
#### Questions
* Confirm how IPOC data is sampled. Is it sampled at the same timestamps for all sensors on allocations? Or maybe something like sampled at the same timestamps per location/beam.
  * Within one beam, measurements happen at the exact same moments. (CONFIRMED, see ipoc_timestamps_check.py).

* Is IPOC data sampling always per 10s? From notebook 170316 it seems that measurements are made every 10s.
  * Not necessarily, IPOC data is sampled when magnets receive pulses. Every 10s in 'soft start' state. This state is to condition magnets before an experiment starts / particles are injected. This takes 20 to 30 minutes. The magnet receives incrementing pulses up to 20 000 A. At injections, the sampling rate should be higher. Once particles are injected, the magnets are not used anymore. The experiments then go on for about 8 hours.

_**The focus of the anomaly detection is on the activity of magnets, not on the experiments that go on for hours. The machines do nothing anymore while the experiment goes on.**_

* More info about BEAM_INTENSITY. What exactly does it represent. Is it also IPOC data? What is the "charges" unit (MKI_data_info.pdf)?
  * BEAM_INTENSITY has a direct relationship with the amount of particles in the machine. If BEAM_INTENSITY is zero, there is no experiment going on.
In the 'soft start' state, BEAM_INTENSITY is also 0.

* In one of the first meetings, it was said that successful experiments usually take about 8 hours. In the histogram of IPOC duration is visible that most of the experiments are around 30 min long. Are the 30 min long ones the normal ones?
  * See above

* preprocessing.scale_robust: Why was scaling done again? Doesn't this cause information loss w.r.t. feature importance? Was this something that made GMM perform better?
  * GMMs are more sensitive to larger absolute values. Trees are less sensitive to this.
  * Feature importance should remain the same after scaling:
  > Generally, It is not necessary. Scaling inputs helps to avoid the situation, when one or several features dominate others in magnitude, as a result, the model hardly picks up the contribution of the smaller scale variables, even if they are strong. But if you scale the target, your mean squared error is automatically scaled. MSE>1 automatically means that you are doing worse than a constant (naive) prediction.

  > For linear classifiers it would not matter because the weight could be adjusted to cope with the differences, but consider you use some kernel for computing similarities. If different features have not approximately the same size, then the distance or similarity will be mostly based on the feature with the largest variance.

#### Coming week
* filter elogbook per beam (DONE)
* check rest of notebooks (DONE)
* try segmentation on anomaly results => will be important for all future evaluation (DONE)
* use segments for precision/recall checking (DONE)
* plot all logbook entries (for a beam) instead of just anomaly (DONE)

### 10/12
* Something to think about w.r.t. anomaly length: When something goes wrong (anomaly happens), particle injection will be repeated as soon as possible. How many times does it have to repeat injection on average? How long does this process usually last until the experiments can successfully begin?
* At the moment, anomalies are segmented in bands of 12 hours. Maybe this is too long? Instead of making anomaly segments up to 12 hours long, we could let them be shorter and check if for tagged anomalies, there exists a band within 12h before/after the tagged anomaly, instead of checking for overlaps between tagged anomalies and anomaly segments.
* evaluation.compare_detected_w_labeled checks whether detected anomalies and labeled anomalies fall within 12h of each other. (REMOVED METHOD LATER)

### 11/12

| Location | Installation name | LHC beam |
| -------- | ----------------- | -------- |
| UA23     | MKI2              | beam1    |
| UA87     | MKI8              | beam2    |

ELOGBOOK_tagged entries contain 'VALUE' and 'PATH' data which is either {VALUE: MKI8, PATH: LHC.MKI8} or {VALUE: MKI2, PATH: LHC.MKI2}. **This is used to filter the entries for beam 1 or beam 2.**

On names of collections: The collections for continuous data also contain 'L2' for beam1 and 'R8' for beam2. **What do L2 and R8 represent?**

### 13/12

Short overview of what to put in presentation next week (also pretty much what should be in the intermediate thesis text):
* Context
  * Large Hadron Collider
  * Kicker injection magnets & CERN experiments
  * Anomaly detection
  * Problem statement & Motivation
  * Beams
* Pipeline
  * Data collection
  * Feature extraction
  * Isolation Forest
  * Evaluation
* First results
  * Isolation Forest
  * GMM
* Plan
  * Improve feature extraction
  * Explore other methods
  * Maybe show pipeline and mark where adaptions will be made
Make all choices clear. Always have a reasoning "We chose this method, because ..."

### 14/12

#### Meeting
* When comparing distances between detected anomalous segments and labeled anomalies, why did Niels always do 0 < d < 12 instead of abs(d) < 12? abs(d) < 12 allows the label to lie nearby on both sides instead of just one. It might be useful to allow this since the labels don't exactly denote when the anomalies occurred.
  * Labels should always come after anomalies happened. **Try out with absolute values and confirm that there are no differences in results**

* How many graphs did Niels make? Which kinds? I didn't see many at all in the evaluation notebooks. Did time run out? I added some more graphs.
  * It was too time consuming to make more useful visualisations work.

* Elia: Is the next presentation (20/12) in Dutch or in English? Who are the assessors that I got? = The extra people that will read my thesis?
  * English slides will be useful for CERN

#### Future work
* Draw segments as lines instead of drawing just the centers 
* Automate more of the pipeline. Abstract functionality out into functions so that e.g. we can just do
```py
features = feature_extraction.get(params...)
features_scored = anomaly_detection.score(features, "METHOD", params...)
results_stats = evaluation.evaluate(features_scored)
```
* Experiment with Isolation Forest parameters and make ROC curve, and precision and recall graphs

Figure out what to improve next:
  * ML algorithm
  * Feature extraction
  * Segmentation algorithm

### 18/02
Always apply filters before resampling/compressing data. Otherwise, incorrect measurements are used in the resampling process.

### 20/02, meeting
* Look at IPOC lengths during anomalies vs averages
* Examine anomalies individually

### 25/02
How close are beams 1 and 2 to each other? Look for images

### 27/02
In IPOC segment length plots: only 16 segments? (WAS A MISTAKE)

#### Meeting
* Try learning isolation forests on 2 groups of data: data where there are many successive shorter IPOC segments in a short amount of time vs. fewer successive IPOC segments of longer length.
* There is fewer data with longer IPOC segments => performance better when using only shorter segments?
* Try clustering IPOC segments to see how they would be split.
* Maybe we made a step that was too large, try simpler things:
  * Only data, no sliding windows
  * Normalised data
  * Fewer features
  * Only sliding window features
  * Add small groups of features
  * Subsets of above options
  * Data on 1 type of measurement (temperature, pressure, other, combinations, ...)
  * Try training on data when segments are short vs data when segments are long
* Try out isolation forest parameters.
* Check how isolation forest with reasonable defaults varies for different input data.
* Feature extraction: see Tsfresh.

##### Anomalies
* Check where anomalies happen more w.r.t. length of IPOC segments. More with shorter segments or with longer ones?
* Anomalies could be labeled in segments where there is no IPOC data.
Anomalies are logged after event actually occurred -> don't know exactly which segment or group of segments actually caused the anomaly.
* For now, treat all segments within 12h before the logged anomaly as anomalous.
* Check benefit of last year's segmentation algorithm vs using a list of IPOC segments and flagging them as anomalous if there is an anomalous point that lies in/near them. (REMOVED OLD METHODS)
* Read section on segmentation in last year's paper.

##### Focus next week 
* Do evaluation code refactoring.
* Expand pipeline so testing with different data is easy/fast (need functions for getting different feature sets and functions for evaluating).
* Test with different feature sets.

### 04/03
Changed filtering so that cells are changed to NaN instead of removing whole rows (data points). The cells are then filled with ffill and bfill. This way, we don't lose the data in the other columns.

### 06/03, meeting
* Try to add BEAM_INTENSITY to the features, adds info on the load of the machines.
* Find the time measurements I mentioned and add their timestamps in a notebook.
* Add feature extraction functionality per magnet (maybe for later, if there are anomaly labels for magnets instead of whole magnet installations)
* Discussed sampling rate of 10s for IPOC data. Removes a lot of other data when building features.
* Do more todos of last week.

### 13/03, meeting
Not much work done past week

### 20/03, meeting
* Almost done with evaluation notebooks and evaluation/plotting/tables code, then tests with different parameters/features can be done much quicker and more correctly.
* Still need to implement evaluation by creating IPOC segments in input data and checking which segments contain anomalies
* Approach:
  * Use df of features to make predictions.
  * Append scores to df.
  * Turn df into lists of dfs that represent IPOC segments (split the features df into groups where there is a time jump of >30min between successive timestamps).
  * 2 options:
    * Anomaly score of segment = sum of scores in segment. Then use the X most anomalous segments for evaluation.
    * Use the X most anomalous points and then see which segments have the worst anomaly score
  * Calculate precision/recall using the most anomalous segments and ELOGBOOK labels.

### 22/03, meeting
Discuss:
* DTAI tasks (poster & popularising article)
* definitive title
* negative time measurements notebook 
* all new images under src/scripts


* Instead of calculating precision and recall in steps of 250, do steps of 25.
* Check how many segments consist of only 1 point.

### 23/03
Pieter:
> Voor de segment-lengte zou ik het bij 10 minuten houden. Dit is rekening houdend met een een spanningsoverslag (vacuum voltage breakthrough) anomalie waar het tot 10 minuten kan duren voor het vacuüm zich herstelt; langer vraagt het normaal gezien niet. Ook belangrijk voor de evaluatie is het tijdsframe rond een elogbook post; aangezien dit een manuele verwerking is zou ik de 12u behouden.

> Verder realiseerde ik mij dat sommige anomalieën inderdaad de hieronder vermelde features gaan nodig hebben (softstartstate, control, mode, KiTS (Kicker Timing System) variabelen KICK_xxx) om detecteerbaar te zijn, wat misschien de lage precisie & recall van de huidige modellen kan verklaren.

> Wanneer ik door de e-logbook entries ging zag ik dat bepaalde evenementen met de tags (fault, anomaly) enkel detecteer zijn door middel van de 'MODE' variabele. Inzake de KICK_xxx variabelen, dit zijn de configuratiewaarden van het Kicker Timing System (KiTS). De waarden die constant zijn kan je negeren (count_toplay, time_toplay) en wat verdere info:
enable: use this magnet or not (bool)
delay: requested delay between RF prepulse and trigger
length: requested magnet pulse (i.e. kick) length
strength: requested kick strength

## 27/03, meeting
For better evaluation:
* Decide what the ground truth is in input segments, annotate the input segments with existing labels. 
* Review confusion matrix, precision & recall, ROC, F1, accuracy
* Windows around labels of 12. First use a larger window and see if anything inside it is detected (top k anomalies). 
* For segments that go on for longer times, look into splitting them into other segments
* Problem with longer segments: machine can work normally for a while, but go bad later on inside the same segment.

Be explicit with assumptions and write them out. E.g. we assume that the labels are correct, but there is a possibility that they are wrong and that certain anomalous events were not labeled. We assume that all anomalous events were labeled.

New pipeline approach:
* Use df of features to make predictions.
* Append scores to df.
* Turn df into lists of dfs that represent IPOC segments (split the features df into groups where there is a time jump of >30min between successive timestamps).
* Use top-k anomalies to classify IPOC segments as anomalous
* Use labels (ground truth) to annotate which IPOC segments really are anomalous
* Compare ground truth (segments that lie close to a label) to predictions (segments that contain any of the top-k anomalies) for evaluation.

## 10/04, meeting
* Careful with annotating ground truth in segments. In the 12h before the label, many segments could be annotated. If one of those segments is predicted as anomalous, the prediction is correct. Don't forget that there are those other segments that have been marked as anomalous.
* Discuss measurement sets for STATE and CONTROLLER and their possible impact on anomalies.
=> More info will come later. Waiting for KiTS specialist.
For more info about it, see 23/03
* Still need to fix filter_extremes(): _**Always keep in feature set: I_STRENGTH, T_DELAY**_

Pieter:
> Hallo Armin, een nieuwere versie van MKI_data_info.odt vind je in mijn pvt_fix-filter_extremes branch. Intussen heb ik ook mijn twee mongo dbs gemerged, dit gaat simpel door het mongorestore commando te gebruiken dat in jouw readme staat, zonder de --drop parameter.

> Ik besprak ons onderzoek met een collega en we vroegen ons af of we geen betere resultaten zouden hebben als we bijvoorbeeld de fixed-frequency logging data (i.e. temperature, pressure) apart modelleren. Dan hetzelfde toepassen op enkel de IPOC data en zien of beide modellen (enkel of samen) in een betere recall resulteren. Misschien dat we teveel verschillende features samen hebben en dit te complex is voor een anomalie detectie met hoge precisie?

## 16/04
Need to examine which labels are not linked to any segments. Is this because there is no segment near those labels? For which type of labels does this occur?

## 17/04, meeting
* Try shifting the anomaly logging window to partly around the anomaly (e.g. 10h before and 2h after)
* In text, clearly describe different steps of evaluation. E.g. annotating ground truth, annotating predictions, then doing correction on predictions to account for multiple anomalous segments per anomaly label.
When it comes to comparing different methods, the correction step doesn't matter. The best method will remain the best after the correction step. (WRONG, SEE 23.04 EXAMPLES)

Next week:
* Check if there are anomalies which are still completely not found. (create graphs as with previous evaluation method)
* Run evaluation for different parameters and feature sets.
* Compare performance to GMM

## 21/04
[Andrew Ng: Anomaly Detection | Developing And Evaluating An Anomaly Detection System](https://www.youtube.com/watch?v=8DfXJUDjx64)

* Training set: 60%, no anomalous data
* CV set: 20%, half of the anomalous data
* Test set: 20%, other half of the anomalous data
* Fit the model on the training set. Make predictions on validation/test set.
* Evaluation metrics: classification accuracy obviously not good, data is very skewed. Instead TP, FP, FN, TN, Precision/Recall, F1-score.
* When trying to make decision about what features to include or tuning a parameter, continually evaluate the algorithm on the CV set. Then pick the best result and evaluate it on the test set.

### Some useful links

* [Receiver Operating Characteristic (ROC) with cross validation](http://scikit-learn.org/stable/auto_examples/model_selection/plot_roc_crossval.htm)
* [Parameter estimation using grid search with cross-validation](http://scikit-learn.org/stable/auto_examples/model_selection/plot_grid_search_digits.html)
* [Recursive feature elimination with cross-validation](http://scikit-learn.org/stable/auto_examples/feature_selection/plot_rfe_with_cross_validation.html)

## 23/04
### IPOC segments, ground truth annotation, and evaluation
In the current evaluation approach, we mark certain IPOC segments that lie near labels. In the current implementation,
IPOC segments are marked with the label they lie close to. During evaluation, IPOC segments which are marked are viewed
as anomalous (can later be extended to split anomaly labels from the other types of labels). 
This was done to have a consistent basis of ground truth we can use to compare different methods.

A mistake made in last year's work is that Recall was calculated by (correct_anomaly_predictions / amount_of_labels). 
This is incorrect because of the use of amount_of_labels. Recall should be calculated by (TP / (TP + FN)), 
where TP = True Positives, FN = False Negatives, and TP and FN are counted using the classifier's predictions.
Ideally, TP+FN equals amount_of_labels, but this is not always the case, it depends on how the ground truth is annotated on the segments (see approaches later).

We thus need to annotate the IPOC segments with the ground truth, so that we can correctly count TP/FP/FN/TN.
However, because the exact location of anomalies is unknown, this is not a trivial task. Our approach now marks
all segments that lie up to 12h before a label. This means that for every label, there can be multiple segments
that are marked, while in reality the anomaly probably occurs in only one of those segments.

The following example shows a problem that comes with this approach of marking IPOC segments:
Let's say there are 4 segments that lie near an anomaly label. All these 4 segments will then be marked as anomalous 
(= ground truth annotation). If an anomaly classifier predicts that one of these 4 segments is anomalous, then that 
prediction is correct. However, this will not be picked up correctly in evaluation metrics (Precision/Recall/F1/AUROC).
In this case, there would be 1 True Positive and 3 False Negatives, while what we want is 1 TP and 0 FN. 

I believe there are 3 ways to handle this situation to compare different classifiers (already discussed a bit with Elia):

1) Leave it like this and use the metrics to compare different classifiers

You might think that while the approach above does not represent the performance of classifiers correctly, it could still 
be useful to compare different classifiers to find which one performs best. However, this method will not compare 
classifiers correctly. 

To show this by example, imagine there are 3 labels: *a*, *b*, and *c*. For label *a*, there are 5 IPOC segments that lie near
it, while for labels *b* and *c*, there is 1 segment each. Now image two classifiers **X** and **Y**. Classifier **X** predicts all 5 
segments for label *a* correctly as anomalous, but doesn't for the segments accompanying labels *b* and *c*. Classifier **X** thus 
has 5 TP and 2 FN. Classifier **Y** predicts one segment for every label correctly and has 3 TP and 4 FN.
In this case, the classifier **X** will be evaluated as the better one. In reality, classifier **Y** is better, because
it predicted all 3 labels, while classifier **X** only predicted 1 label.

2) Predict all segments for a label as anomalous if one of them is predicted as anomalous

An approach to remedy the problem of the example reviewed above is to predict all segments that belong to a certain
label as anomalous if one of them is predicted as anomalous. In the example above, classifier **Y** predicted one segment for 
label *a* correctly. With this approach, the other 4 segments that are associated with label *a* would then also be 
predicted as anomalous. This would then bring classifier **Y** to 7 TP and 0 FN, making it better than classifier **X**.

Although this approach corrects the comparison depicted above, it does introduce another problem. Using this approach,
labels with many IPOC segments will inflate the True Positives of a classifier and disturb comparisons in a different way.

Imagine this time that classifier **X** only predicted 1 segment for label *a* and that classifier **Y** only predicted 1
segment for labels *b* and *c*. In this setup, classifier **Y** should be evaluated as the better one, but this approach will 
make **X** look better than **Y**, since **X** will have 5 TP, while **Y** will have 2 TP.

3) Only count one segment per label

A final approach to evaluation is to only use one of the segments that lie near a label in determining whether a prediction is a TP or a FN. If one of the segments for an anomaly label is predicted correctly as anomalous, then the other segments are discarded so that there is only 1 TP left, even if there are multiple segments that are predicted as anomalous. If none of the segments are predicted correctly as anomalous, then only one of the segments will be kept so that there is only 1 FN left.

Using this approach, each label will only amount to 1 TP or 1 FN. By discarding all segments except 1 for each label during evaluation, there will still be a consistent ground truth basis we can use to compare different classifiers.

### Evaluation metric
When it comes to the exact evaluation metric to use to compare results, there are many options: Accuracy, Precision, Recall, F1-score, Average Precision, Area under ROC curve, etc.  
Out of the many available options, we prefer to use a single one that can represent the quality of a classifier well. Using a single metric allows us to run many tests using  different classifiers and simply compare them using this single metric. It is important to select a metric that can evaluate results well in a context where the dataset is highly skewed, as it is in our case. 

A commonly used option for this task is the area under ROC (Receiver Operator Characteristic) curves. For our context, where the amount of negatives far outnumber the amount of positives, it makes more sense to use the area under PR Precision-Recall curves. PR curves convey the differences between classifiers better in the context of skewed datasets.

Excerpt from "The Relationship Between Precision-Recall and ROC Curves": 
> a large change in the number of false positives can lead to a small change in the false positive rate used in ROC analysis. Precision, on the other hand, by comparing false positives to true positives rather than true negatives, captures the effect of the large number of negative examples on the algorithm’s performance. classifiers on skewed datasets more effectively. 

(EXPLANATION OF DIFFERENCE HERE?)

> Intuitively, precision is the ability of the classifier not to label as positive a sample that is negative, and recall is the ability of the classifier to find all the positive samples.


Formulas:
* TPR = TP / (TP + FN)
* FPR = FP / (FP + TN)
* Precision = TP / (TP + FP)
* Recall = TP / (TP + FN) = TPR

To illustrate their differences, imagine a dataset with 5000 segments, 15 of which are anomalous. Let's say we want to compare the two classifiers with the following results:
* Classifier **X**: 50 Positives: 10 TP, 40 FP, 5 FN, 4945 TN
* Classifier **Y**: 200 Positives: 10 TP, 190 FP, 5 FN, 4795 TN

Both classifiers return the same amount of TP, but **Y** returns a much larger amount of False Positives than **X**. It's clear that **X** is the better classifier in this case. However, when we look at the TPR and the FPR measures that ROC uses, we don't see such a clear difference:
* Classifier **X**: TPR = 10/(10+5) = 0.8, FPR = 40/(40+4945) = 0.008 
* Classifier **Y**: TPR = 10/(10+5) = 0.8, FPR = 190/(190+4795) = 0.038 (difference of 0.03)

Precision and recall don't consider the large number of True Negatives, so they aren't affected by the imbalance:
* Classifier **X**: Recall = 10/(10+5) = 0.8, Precision = 10/(10+40) = 0.2 
* Classifier **Y**: Recall = 10/(10+5) = 0.8, Precision = 10/(10+190) = 0.05 (difference of 0.15)

Following the reasoning above, we will use the area under the PR curve in the evaluation procedure.

(MAYBE USE AN IMAGE TO SHOW ROC AND PR CURVES)

[Code examples and more text](http://scikit-learn.org/stable/auto_examples/model_selection/plot_precision_recall.html)

## 24/04, meeting
Discuss:
* Evaluation of IPOC segment predictions
* DTAI WIKI incorrect deadlines
* Top k vs threshold
* How to pass segment predictions to sklearn.metrics functions? Methods like PR and ROC curves compute different points for the curve using different probability thresholds. We don't have that currently. Top k anomalies are used to predict segments as anomalous or not (0 or 1, no probability). Need to somehow transfer probabilities (anomaly scores) from data points to segments. Maybe better to write own PR-curve method and use a varying threshold for normal/anomalous instead of using top k anomalies? 


* In order to be able to use scikit-learn's implementation of PR-curves:
  * Apply a function to segments that gives them some kind of anomaly score like different data points have anomaly scores. This way, the PR-curve calculation can automatically set different thresholds on segment's anomaly score to calculate (precision, recall) pairs for the curve.
  * Examples:
    * worst anomaly score of segment
    * mean of 3 worst anomaly scores of segment
    * mean of X% worst
    * etc.

## 28/04
### Multiclass confusion matrix
Confusion matrix can be used in with multiclass data. [Example](http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html)

### Evaluation with mutiple classes
http://scikit-learn.org/stable/modules/model_evaluation.html#from-binary-to-multiclass-and-multilabel

* In extending a binary metric to multiclass or multilabel problems, the data is treated as a collection of binary problems, one for each class. There are then a number of ways to average binary metric calculations across the set of classes, each of which may be useful in some scenario. Where available, you should select among these using the average parameter.
* "weighted" accounts for class imbalance by computing the average of binary metrics in which each class's score is weighted by its presence in the true data sample.
* "micro" gives each sample-class pair an equal contribution to the overall metric (except as a result of sample-weight). Rather than summing the metric per class, this sums the dividends and divisors that make up the per-class metrics to calculate an overall quotient. Micro-averaging may be preferred in multilabel settings, including multiclass classification where a majority class is to be ignored.
* Selecting average = None will return an array with the score for each class.


## 01/05
### Averaging methods for multiclass classification evaluation
* **macro**: sum(scores, for each label) / count(labels)
* **micro**: score for all data, no weighting
* **samples**: sum(scores, for each sample) / count(samples)
* **weighted**: sum(score * count(true_samples_with_label), for each label) / sum(count(true_samples_with_label), for each label)
* **none**: set of scores for each label

http://scikit-learn.org/stable/modules/model_evaluation.html#multiclass-and-multilabel-classification

## 03/05, meeting
What to call a set of features at a specific timestamp? There are many options and they are commonly used interchangeably:
* Data point/row/sample/instance
* Multi-dimensional data point
* Timepoint
* **Tuple**

Idea for problem definition and better graphs overall:
* Add anomaly dots on graphs that show the data/features

## 05/05
* The data we have is timeseries data, but there are many gaps in it.
* Some data is continuous and measured all of the time. Forward fill can be applied to that data to fill gaps.
* IPOC data is different. Not only does it have relatively large gaps between measurements when machines are not in use, it also has a sampling rate of 0.1Hz.
* The magnets need to be precise up to the microsecond/nanosecond, but their data is only measured every 10s when they are in use with millisecond precision (see output/ipoc_timestamps_check.txt).
* Thus, anomalies can only be found if they manifest themselves in the data for more than 10s. All short-lived anomalies that occur between sample points can not be detected.
* How to get feature importance when using Isolation Forest: could investigate the ExtraTreeRegressors in the estimators_ member. See how they work, plot them, etc. estimators_ is a list of DecisionTeeClassifier, which has a member "feature_importances_". The counts/values of features in this list could be used to get feature importance.

## 08/05, meeting
* Discuss:
  * timestamp_differences graphs in scripts/figures/ipoc-segment-length. We can clearly see some patterns (lines) in the differences of the timestamps.
    * This is normal due to the soft-start system (see soft-start conditioning below).
    * There are 2 patterns expected here: 10s pauses because of soft-start system, other pattern because of machine settings.
  * BEAM_INTENSITY
    * If it is nonzero, an experiment is being done.
    * "BEAM_INTENSITY has a direct relationship with the amount of particles in the machine. If BEAM_INTENSITY is zero, there is no experiment going on."
    * It can also be zero while magnets are in use (for testing purposes? read more in LHC design report / technical stop procedures).
    * BEAM_INTENSITY can be zero during KISS soft-start.
  * Titles of people on thesis cover page
  * Outline of text mentioned in todos.txt@TEXT (UPDATED IT)
  * Master's Thesis title:
    * Anomaly Detection for the Large Hadron Collider might be a bit too generic. Maybe "the Large Hadron Collider injection magnets"

* Read more on soft-start system (KISS conditioning)
  * Faster injections will make experiments/physics-runs faster.
  * After year end technical stop, fewer injections will be done at first.
  * Injections will not always follow the same pattern.
  * Google queries for images: "LHC injection cycle", "LHC beam injection cycle"
* Think of implementation for determining which anomalies are found best and worst for Detectors. This way we could use different detectors and see the differences in their results in a way other than just area under PR-curve. If different detectors are good at finding different types of anomalies, some ensemble stacking approach could be implemented.

## 09/05
Thoughts after poster presentation:
* Make splitting and removing of data into segments clear. "Abstraction level of segments". Try to show segments as different lines. 
* Split anomaly dots on figures into dots for different types of anomalies.
* High precision at lower recall in GMMs could be because of one of the components learning some kind of behavior.
* Explain choice for PR-curve over ROC-curve in presentations: show confusion matrix and that the large TN is not used in PR space.

## 10/05
* Good explanations of micro vs. macro averaging of precision and recall: https://datascience.stackexchange.com/questions/15989/micro-average-vs-macro-average-performance-in-a-multiclass-classification-setting

## 13/05
* Scikit-learn's grid search implementation is incompatible with our implementation. It needs target values, which we don't have, and it also does cross-fold validation, which we don't need yet (performance is low anyways).
* Performance of Isolation Forest seems to be very bad all over the board
* Performance of GMM has very high variance

## 16/05
* BEAM_INTENSITY:
  * Is measured almost every second. 12.70 million measurements for each beam.
  * After resampling to 1s, there are 12.96 million measurements.
  * Resampling to 1s (so that timestamps are rounded to the second and every second has data) doesn't change the graph at all.
  * Resampling to 10min doesn't change the graph much. It seems that the values mostly change slowly.
  * Slightly higher values on average in B2
  * **Can be used as a continuous feature with sliding window:**
    * _Could be useful._ => added in the feature building step
  * What is the "Charges" unit again? It has a direct relationship with the amount of particles in the machine, but I'm not sure what exactly it is. Is it the cumulative electric charge of all particles in the machine? With SI unit coulomb?
    * _Measured by measuring the total charge of the beam._ => Changed to "Total charge"

* BUNCH_LENGTH_MEAN:
  * Not measured every second. 1.33 million measurements.
  * Obvious spike in B2 of 7.44e-9 on 2016-06-11 19:13:50.624. Should this be filtered out or is it ok?
    * _Could have happened during MD (machine development)_
  * **Can also be used as a continuous feature with sliding window:**
    * _Could be useful._ => added in the feature building step
  * "Mean measured bunch length" Is the mean length of all the bunches (1 to 2808 bunches?) in the beam?

## 17/05, meeting
Done since previous meeting:
* Area under PR-curve calculations using the trapezoid rule
* Grid search implementation and some results on old features
* Merged Pieter's branch
* Added new MongoDB data file and checked completeness/consistency
* Updated/fixed filter_extremes() and its usage
* Updated feature building for new data and for BEAM_INTENSITY and BUNCH_LENGTH_MEAN measurements
* Did data exploration on new features in notebook 180517

Discuss:
* Check one last time if the filter function is correct the way it is now
  * _is OK_
* BEAM_INTENSITY (16/05)
* BUNCH_LENGTH_MEAN (16/05)
* BEAM_INTENSITY and BUNCH_LENGTH_MEAN have a huge gap from December to April. This is because of the technical stop.
* SW_SUM features look useless, notebook 180517:
  * _They are always very similar to the raw data. => Can be removed, try with and without_
* Check the BEAM_INTENSITY in the feature set vs. the database comparison in notebook 180517:
  * In the feature set, we only see some BEAM_INTENSITY dots when the values are rising. No data in features when it is going down, this is because there is no IPOC data at those points, so that data can be discarded.
* How about training GMMs only using segments that are "normal"? This way the components aren't affected by the anomalous segments.
  * _Is worth a try_
* **Important**: anomaly score of segments is still the very simple one of anomaly_score(segment) = worst anomaly score in the segment

### Database
* Collections on the new file don't have an index on start-time. So the old file must be imported first, then the new file to add the 2015 data.
  * FIXED ON 18/05: "reindex possible from fetch_data.py script", new data file will be created.
* STATE collections have no data from 2015-06-22 to 2016-03-13
  * _will be checked_ => **NO STATE DATA AVAILABLE BEFORE 2016-03-10**, the control system did not publish contents before then
* STATE:STATUS only has 4 or 5 elements per MongoDB document (which each contain 5 days of data)
  * _Could be fine because the values didn't change._
* CONTROLLER data (e.g. KICK_STRENGTH_TOPLAY) contains duplicate documents for some start-time values. Because of this, they cannot be queried correctly.
  * _will be checked_ => FIXED ON 18/05: "duplicate start-time values for e.g. kick_strength_toplay bug fixed", new data file will be created.
* Attribute 'start-time' is not always correct. E.g. "2016-05-31 22:30" was in the document with start time "2016-06-01". Could this be because of wrong timezones when the timestamps are read?
  * Yes, FIXED ON 18/05: "local vs UTC time fixed; timezone now hardcoded however"

To do:
* Implement different anomaly_score methods on Segment.
* See what the effect is of keeping 1 of the 4 features of a type of measurement (e.g. only magnet A of TEMP_MAGNET_UP). Maybe there is a small subset of the features that performs as well as all of the features
* See which anomalies are found and which are not. What are we detecting? Can plot this automatically for the best result after grid search.
* Try adding LOF or SVM

## 18/05
* Grid search with all features:
    * GMM: `see grid_search_gmm.py`
    * Isolation Forest: `see grid_search_iforest.py`

## 21/05
* Small confirmation that evaluation is working properly: did grid search on features for beam 2 but labels for beam 1 noticed that results were much worse than when using the correct pairs of features and labels.
* Grid search on beam 2 (limited set of parameters, notebook 180510):
  * Best AUC for GMM with anomaly labels = 0.305
  * Best AUC for GMM with all labels = 0.186
  * Best AUC for iForest with anomaly labels = 0.031
  * Best AUC for iForest with all labels = 0.053
* Isolation Forest performs worse now because of more data and more labels (confirmed by performing pipeline on data after "2016-04-17" and seeing the old (better) performance)
* After the new data was added, Isolation Forest seems to mark points in 2016 where temperature is higher as anomalies. Temperatures in 2015 were lower overall. See notebook 180513.
* DummyDetector performance with different evaluation parameters:
  * anomaly score method:
    * top_percentage is the worst one by far with AUC of 0.05 at max
    * max reaches as max AUC of 0.16, but it < 0.10 for most curves
    * top_k has an AUC of at least 0.11, and at most 0.23
    * => top_k performs the best for the DummyDetectors
  * labels: AUC is higher when using all labels rather than only anomaly labels
  * segmentation distance: 30 is best, then 20, then 10
  * Best: pr-curve-dummy_detectors-seg_dist_30-anomaly_method_top_k-labels_all.png

## 22/05, meeting
Work done since previous meeting:
* Added option for anomaly_score_method in Segment, evaluation, and pipeline/grid_search
* Added option for segmentation_distance in pipeline/grid_search
* Rewrote grid_search so that per set of detector_parameters, the detector is trained once and evaluated multiple times for different evaluation_parameters. This is faster than re-training the detector for every combination of all parameters.
* Improved output of grid_search and ordered the results summary at the end so all results are seen in descending order of area under curve.
* Made grid_search compatible with Exceptions thrown during training. The AUC is then set to -1.
* Did grid search with all features for GMM and Isolation Forest and added notes on the results (18/05).
* Plotting function for multiple PR-curves in 1 plot and used this for DummyDetectors (and plotted them all in a script), reviewed results on 21/05.
* Implemented Segment anomaly_score methods and tested the 3 different methods in a notebook. Noticed that while some segments had the same score for method 'max', they had different scores for methods 'top_k' or 'top_percentage'.
* Added script for plotting of anomaly scores over the whole time period. See figures/anomaly_scores/
* Added functionality for plotting labels and predictions over time and over features in the data. Predictions are created for 5 different thresholds, so 5 plots are created. These plots are also created automatically for the best results found in grid search, and the figures can be stored automatically.

Discuss:
* Review SEGMENTATION_DISTANCE and SLIDING_WINDOW_SIZE:
  * Default sliding window size has been changed to 10 min.
  * See Pieter's comment noted on 23/03.
* Labels vs. predictions plots in notebook 180520.
* For STATE datasets: no data before 2016-03-10, might be problematic for learning. Also need to implement it in feature building step. => try to add STATE to best performing parameters/feature sets and see if the results improve.

* View 2015 and 2016 as different data sets and try learning on them separately.
* Date for presentation at CERN: 18/07, 20/07, 23/07 not good. => will try for 27/07

## 29/05, meeting
Discuss:
* Presentation 27/07
* Thesis tekst inbinden?
* New IPOC segment length images
* Meaning of UA23, UA87, 5L2, 5R8 so they can be explained in text.
  * 5L2 could be location in ring sector
* Is it possible for effects in one beam to influence the other beam?
  * They are effectively different parts. For experiments, there will always be two beams for collisions. During MDs there could be 1 beam or 2 beam with different intensities.
  * Only thing they have in common is their injector chain. Both beams will have similar properties when injected, but they will not be exactly the same.
* info pdf file says that one pulse generator powers two magnets, state data measures pulse generator states => then why is there only one collection of state data per beam, and not two, one for each pulse generator per MKI installation? Or all 4 magnets for a beam powered by one pulse generator?
  * Same variables set for control system.
* How detailed does the explanation of Isolation Forest, GMM, ROC, PR, etc. need to be?
  * Summaries of methods. References to texts that explain them fully.
* Grid search line of code in text?
  * Ok. In general, keep implementation separate from approach throughout text.
* Do I create a section where I compare the approach to previous year's approach? In results or conclusion chapter?
  * In results

## 31/05
* Checking labels to see if there is a reason for the temperature drop in May:
  * Intervention for both installations at 02/05/2016 12:00:00:
  > "Stopped the MKI2 today around 12.00 am to 'burn' the EPROM to ensure the latest modifications (specifically to the INJECTION_PERMIT) are now inside the PLC.\n\nPut SYSTEM back in STANDBY (with correct masks applied where required).\n\nALL good."
  * If this explains the temperature drop, then the filter is removing evidence of an intervention. Probably has a negative effect on using all labels during evaluation.
  * => Could have been an intervention from another team during TS. ELOGBOOK entries are only from one team. The temperature drop and label entry happening at the same moment is coincidence 
* Link between Controller and IPOC data 
  * Controller data is what IPOC data is supposed to be
* high voltage de-conditioning in simple terms?
  * Sent by email
* confirm kiss pulses up to 20 000 A and length of 20 min 
  * OK
* CALS for all data storage? 
  * No, only machine data
* MKI pulse generators <-> MKI power generators 
  * pulse

# 07/06
* Show feature dataset inspection step in presentation