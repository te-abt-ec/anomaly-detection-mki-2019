# Notes of meetings

## 14/08: initial meeting

with Elia

General plan for this thesis

1. Reproduce figures/pipelines of thesis 2018 & understand source code.
2. Replace GMM/Isolation Forest with new technique from scilearn-kit.
3. Replace ML technique with MERCS (requires changing pipeline).

Tips

* prepare Jupyter Notebook for meetings
* use plots

Performed work

* read thesis by Armin Halilovic (2018)
* read thesis by Niels Wéry (2017)
* reproduce plots 2018 and scripts u notebooks
* Python Data Science Handbook by Jake VanderPlas:
  * chapter 0,1,2: read
  * chapter 3,4,5: -
* read Outlier Analysis - Aggarwal:
  * chapter 1-3: done
  * chapter 4-12: -

## 25/09: second meeting

with Elia

Technical plan

* Spark integreren in pipeline
* Ander anomalie detectie algoritme inpluggen
* Betere plots (flexibeler visualiseren)
  * Matplotlib -> plotly python & dash
  * interactief wisselen tussen bv temperatures
  * time range wijzigen
  
Scientific plan

* Feature Engineering / Algoritme / Evaluatie herbekijken
  
Resources about Python

* Intermediate Python
* PEP 8 - Style Guide for Python Code
* Jeff Knupp
  
Answered questions

* Precision / Recall & AUC: where to find more information
  * Paper of Precision-Recall (Jesse Davis)
* Segmentation: notes-2018 l525
* BIC score not reproducable  
  * see script `gmm_parameters`
  * used for determining parameters by Niels
  * however Armin used an experimental approach
* Preprocessing
  * `fourier_transform`, `wavelet_transform` useful?
  * used for feature engineering by Niels
  
Varia

* [Guidelines](https://dtai.cs.kuleuven.be/education/masterthesis/guidelines) for DTAI students

Performed work

* Read "Writing Idiomatic Python" by Jeff Knupp
* Read "Intermediate Python" by Khalid
* Read ["PEP 8 - Style Guide for Python Code"](https://www.python.org/dev/peps/pep-0008/?) by G. van Rossum e.a.
* Experimented with Plotly/Dash
  * Pretty for small values
  * Takes ~10 seconds to switch between plots -> does not look very responsive, even with preloading
* Experimented with other anomaly detectors
  * General remark: no feature engineering done yet, no grid search for optimal parameters
  * *One-class SVM*
    * Training time: 52 minutes -> much longer than e.g. Isolation Forest or GMM
    * Based on graphical inspection, results are slightly worse than Isolation Forest / GMM; not that bad though
  * *Robust Covariance (Elliptic Envelope)*
    * RuntimeWarning: Warning ! det > previous_det -> correct or not?
    * Needs more investigation
  * *Local Outlier Factor*
    * Training time: 26 minutes

## 04/10/2018: third meeting

with Elia and Pieter

Outline of work for this thesis

* Merge to Apache Spark
* Implement MERCS
* More feature engineering
  * Think about temporal features
  * Dynamic time warping?
* More advanced preprocessing
* Interactive plots (low priority)

Performed work

* Set-up local installation of Apache Spark
* Set-up online SWAN with Spark and NXCALS
* Experiment with Apache Spark
  * Anomaly detection library methods: MLLib

### How to get access to Spark via CERN SWAN

1. Go to ["CERN"](http://cernbox.cern.ch) for the first time to initialize a CERNBox
2. Contact 'acc-logging-team'
  * request membership of egroup: it-hadoop-nxcals-pro-analytics
  * request access to NXCALS data-access api
3. Go to ["SWAN"](http://swan.cern.ch) and configure the environment
  * Software stack: "Development Bleeding Edge Python3 (might be unstable)"
  * Spark cluster: NXCALS
  * Others: default values
4. Open your first notebook: `NxCALS-example`
  * Wait for kernel to start
  * Connect to SPARK Cluster by clicking the 'star', with NXCALS options
  
### Export MKI.ELOGBOOK_tagged

The MKI.ELOGBOOK_tagged collection cannot be queried from the Spark cluster. It contains the labels used in the evaluation step of the pipeline. To use the labels via SWAN, load the data on your local machine and export the collection to JSON as follows:
```sh
mongoexport --db cern_ldb --collection MKI.ELOGBOOK_tagged --out logbook.json
```
  
## 16/10/2018: fourth meeting

with Elia

Detailed sketch of pipeline on the blackboard

Identification of weak points:

* *Temporal* features (sliding windows) are limited
  * Fourier components
  * TSFRESH: automatic extraction of relevant features
* Amount of data actually used
  * Due to usage of IPOC data as core, lots of continuous data is removed
  * Maybe this matters, maybe it does not
  * Consider to do anomaly detection for the IPOC data and Continuous data separately
* *Segmentation*
  * Until now: quite arbitrary, check whether you can isolate e.g. the start of KISS conditioning
  * Search for patterns occurring with anomalies
  * Study *segment pruning*: what is done in the implementation? why? better options?
  
Performed work

* Worked with TSFRESH to extract features
  * Huge amount of features
  * How to select?
  * Needs to be integrated in total
* Updated web application
  * Now showing anomalies, with description
  * All datasets are shown
* Started integration to SWAN
* Prepared presentation for next week

## 23/10/2018

with Elia and Pieter

* Web app with visualisation
  * Looks good
  * Only possible offline now, keep it for later use
* TSFRESH
  * automatically generates new features
  * => choose the right features for our dataset: data inspection, CERN knowledge
  * needs to be further implemented
* Spark
  *Is there a way to execute my python files inside SWAN?
    *Tried: spark_from_python.py, OK if executed using %run -i spark_from_python.py, mixes namespaces
    *Tried: 181022-CERNSparkClient.ipynb, NOT OK
    *Workaround: execute "%run -i ..." once, then use python methods => OK
  *How to get a list of all variables in 'VariableQuery.builder(spark).system("CMW")'?
  *Is it known that there are rows with 'nxcals_value' equal to 'null'? Is it okay to filter them away?
  *Can I directly access NXCALS?
    * Not via CMW
  * Is there documentation of the NXCALS internals?
    * Not yet, since it is still in beta.

## 30/10/2018
with Pieter

* Set-up CERN VM and communication with NXCALS (Spark)
  * Tip: use SSH tunneling to call pyspark
  * Check which data can be obtained
* ELogbook: not in NXCALS
  * The logbook does not contain tags. 
  * The tags were manually added by Pieter.
  * The ELogbook system will be revised next year.
* TSFRESH:
  * Fourier components show good results.
  * Should be integrated in pipeline -> TODO.
* COBRAS: semi-supervised clustering

## 06/11/2018

with Elia and Pieter

Feature engineering

* Extracted features based on (tsfresh) Fourier components
* Meaning of the Fourier component is not clear. What is plotted? What is the meaning of coefficient 0?
  => verify this
* The Fourier component of data is computed after joining continuous data with IPOC data. This means that the FFT computation is done after throwing away quite some data.

* Promising patterns in frequency domain.
* Until now, no improvement in precision or recall.
* Idea by Elia: perform anomaly detection with only Fourier components as features.
* Idea by Pieter: perform grid search on features instead of only training parameters.

Spark

* Data relies on Hadoop files
* They can be found as recent as 2 days ago (verified via SWAN)
* Plan: use 1 year of extra data and compare with other 2 years
* Technical issues to reach NXCALS over Openstack VM.
* MLLib: try to explore the possibilities; maybe using GMM
  
Python virtual environment

* no longer need to update `requirements.txt` manually
* ensures all users have same version of Python/libraries

## 13/11/2018

with Elia

Spark

* Flow Leuven -> LXPLUS -> OpenStack VM: working
* Executing job with `spark-submit` not working
  * Logs: <http://ithdp1065.cern.ch:8042/node/containerlogs/container_e65_1542103675936_0004_01_000001/tdewitte/stderr/?start=0>
* New idea:
  * Leuven -> Codebase -> EOS -> Notebook -> Spark
  * Requires using spark and sparksession inside the notebook
* Tip Elia: use 'anaconda' for package managing

Feature selection

* Now it is possible to give a list of features to be used in grid search
* Many options => long computation time
* Idea: use local search to optimize this problem
* Now manually: using IPOC data (with its Fourier components) gives better result than last year's algorithm with all variables on the extended data-set (3 new months included)

Plan for the coming week: focus on getting Spark working in our implementation.

## 20/11/2018

with Elia and Pieter

Current state:

* CERNSparkClient gets all data from the Spark cluster via SWAN

Plan:

* Feature generation still in Pandas
* Model training in Spark (MLLib)

Other:

* TSFRESH coefficient generatie
  * Currently using coefficient 0, only real part
  * Documentation: <https://tsfresh.readthedocs.io/en/latest/api/tsfresh.feature_extraction.html#tsfresh.feature_extraction.feature_calculators.fft_coefficient>
  * Extracting features from FFT: <https://dsp.stackexchange.com/questions/2818/extracting-frequencies-from-fft>
  * Test: coefficient 0 - 9 voor IPOC data
    * worth to do it?
* Importing ELogBook (MongoDB format) on SWAN
  * Pieter will check if JSON can be read by Spark
* Update README for SWAN
  * Mount EOS on CentOS: <https://cern.service-now.com/service-portal/article.do?n=KB0003846>
* Separate notebooks Mongo <-> SWAN
* Keep in mind: `kinit` might be needed on CentOS VM

## 27/11/2018

with Elia and Pieter

* There should be 3 modes of labels being used in the training:
  * Anomaly
  * Anomaly + intervention
  * All tags
* State & control variables might add a lot of information to the model:
  * Only available for data from March 21, 2016.
  * STATE:CONTROL
    * Local during interventions (engineers have control instead of control room)
    * Also contains useful information, since anomalies also occur
  * Idea: use STATE:MODE for segmentation
    * Value 'FAULTY' is a strong indication for an anomaly
    * However, an anomaly might also occur when there is no indication
* MKI.ELOGBOOK_tagged:
  * Implemented
  * MongoDB -> JSON -> Pandas (-> Spark, if needed)
  * Pieter will provide next 9 months
* Spark GMM
  * Feature generation on Spark is bottleneck
  * Loading features from JSON gives slightly different results (precision?)
* TSFRESH: todo

## 04/12/2018

Performed work:

* Extra data (28/11/2016-24/09/2017)
  * Contained NaNs in the frames at the end of a month -> extra fill step
  * Performed GMM grid search again
    * Results:
    AUC: 0.314
    MAKING PREDICTIONS FOR THRESHOLD: 0.0440042751581621
    TP:    11 | FP:    14  || sum:   25
    FN:    24 | TN:  2447  || sum: 2471
    PRECISION: 0.44, RECALL: 0.31

    MAKING PREDICTIONS FOR THRESHOLD: 0.01247064437224772
    TP:    13 | FP:    37  || sum:   50
    FN:    22 | TN:  2424  || sum: 2446
    PRECISION: 0.26, RECALL: 0.37
* MLLib
* Feature generation
* Issues reaching SWAN: 
  * Accessing SWAN from my local machine does no longer work.
  * In a debug session with Diogo Castro (CERN), the expert saw my credentials coming through to access SWAN.
  * However he could not trace my attempt to acces SWAN any further. He assums it to be a network issue.
  * Current work-around: special SSH tunnelling to CERN for access to SWAN
* Verified all scripts in existing application; found a few issues
* Upgraded requirements.txt to `pipenv` which works quite well on my machine; please try it out

Questions:

* Labels used in pipeline:
  * Last week: anomaly, anomaly+intervention, all
  * Previously: anomaly, anomaly+intervention+research
* Controller variables are arrays of 100 items
  * According to MKI_data_info.pdf only the first item of the array should be used
  * Why is this?
  * Fixed in implementation
  * Not used until now, but seems to be available from July 15?
* Log probabilities for SWAN MLLib
* Scale vs Scale_robust: does it matter? Scale was used before, does it make any difference?

Noticed

* In the thesis of Armin, there is a mistake in the table with the number of labels for beam 2. It summed the beam number instead of counting the number of elements for a type of label.
* IPOC data waveforms: what is the meaning of this data? useful?
  * TMR: Terminating Magnet Resistor
  * TDR: Terminating Dump Resistor
  * VS-PFN: Pulse Forming Network voltage
  * IP-RCPS: RCPS charging current to PFNs

Coming deadlines:

* presentation: 17-21 December
* intermediary text: 23 December

Content:

* Introduction
* Background
  * Anomaly detection
* Related work
* Problem statement
* Planning (brief)

## 18/12/2018

with Elia and Pieter

Performed work:
* Updated web app:
  * split FP and FN
  * select segment score method 
* Optimised segmentation code: approx. same speed as fixed segments
* Started writing text and prepared presentation

Discussion on COBRAS:
* COBRAS: DTAI algorithm for clustering with user interaction
* Idea: use COBRAS as an additional step after obtaining segments
  * Use case 1: improve the obtained results by clustering into 4 clusters TP, FP, TN, FN
  * Use case 2: use for real-time analysis, given the clusters, add a new segment into the best fitting cluster
  
## 11/02/2019

with Elia and Pieter

### MKI case-study

* Local time or UTC time? 
  * ?
* Are they IPOC or STATE segments?
  * They are STATE segments based on variable STATE:MODE.
  * Solution: renamed variable in code, no more appearance of IPOC
  
Cases: 
* 2017-05-09 erratic FN: 
  * TODO: try without the filter and check results
* 2016-06-09 FN: explanation OK; did you see there is an anomaly detected very close to this segment?
* 2016-06-14 FN: not detected? any idea?

* 2017-06-20 10:16:19 - 10:39:25 FP
  * ...
* 2017-06-21 12:34:50 - 18:53:12 FP
  * ...
  
## 19/02/2019 

with Elia and Pieter

### Performed work 

* Filter for T_DELAY was added
* Filter for STATE:CONTROL
    * Filter away during beginning based on segments
        * Not straightforward
            * Sampling is somehow arbitrary
        * Method applied
            * Create segments based on STATE:CONTROL data
            * In evaluation step: delete STATE:MODE segment if inside a STATE:CONTROL segment
* Activated FFT features 
* Adapted code to easily train / evaluate for beam 2 as well
* Missing ELOGBOOK entries are actually used
* Minor improvements to code for training period & building features

* Started again on Spark:
    * No longer using VM from OpenStack, but with Cernbox sync to local folder which has git config; was slow using VM to update code and push changes
    * Minor improvements to existing codebase; removed parameters of SparkContext and SparkSession (only stored in SparkClient once)

* Pruning step (cfr. last week)
    * see: segments_scored_to_truth_and_pred_df() in evaluation.py
    * make each label (y_true) responsible for only 1 TP or FN by reducing the set of segments near a label to 1
    * segment with anomaly score (y_pred) equal to the worst anomaly score of the set of segments (= the largest one)
    
### Feedback 

* Web app is not working properly in Firefox / Chrome? Problem was due to a missing SSL certificate of the CSS files used. Changed source of CSS file. 
* Try to integrate the STATE:CONTROL data in the features. Although it might be more difficult to simple, it makes more sense not to train on data of time instances where STATE:CONTROL == LOCAL. 
* Search on *feature/attribute importance* for GMM to indicate relevant datasets of an anomalous segment
* Search on MERCS and residual analysis. The book of Aggarwal is a good starting point. 
* Adapt the web app to show a list of FP and FN. Give y\_true and y\_pred values. 

## 05/03/2019 
### Planning

1. Fix STATE:CONTROL 
  * Perform full outer join 
  * Ffill state
  * Drop rows without IPOC data

2. FFT spectra
  * Add to feature set
  * Explore images to choose correct range

3. Grid search: feature selection
  * Grid search on all possible combinations is unfeasable
  * Do a greedy search with e.g. 10 features

4. Isolation Forest (IF): feature importance
  * Use trees of IF model
  * Subtle difference meaning of 'importance' IF vs decision trees
  * Take an average over all the trees used

5. Cobras
  * Interactive clustering to decrease the number of false positives

### Decisions
* Spark: no longer focus
  * We have made a good start
  * Currently the amount of work is feasible on our machines
  * Porting the existing codebase to Spark Dataframes might take some weeks of time without added functionality
  * However: keep this in mind for the future!
  
* MERCS: no longer focus
  * MERCS can be implemented as another anomaly detector, but will probably not give that much more information
  * Implementation might take some time, without better results
  * The benefit of MERCS would be the feature importance => do this with Isolation Forest
  
## 12/03/2019
* Web app
   * Crashing on Pieter his machine due to call to `area_under_precision_recall_curve` 
   * Plotting via `Matplotlib` while `Dash` is running seems to conflict, so keep this in mind
* Features: STATE and CONTROLLER data is added now
* Feature selection: 
  * Results up to AUC=0.7 for May - August 2017 with GMM => great
  * Sometimes a memory error occurs during the generation of features on the VM 
* Feature importance:
  * Only for isolation forest
  * See fit() method
* Fourier features: 
  * Use spectrogram as a start
  * Plot intensity for the frequency with the highest variation
* Try current work on a larger timespan

## 19/03/2019
* Apache license
* Web app slow on machine of Pieter
  * Fixed: replaced scatter by scattergl
  * See <https://dash.plot.ly/performance>
* FFT features
  * Intensity for maximum variance
  * Intensity for maximum intensity over all frequencies
  * Intensity for average intensity over all frequencies
  * Intensity for median intensity over all frequencies
  * Also required: frequency as a feature 
* COBRAS 
  * See <https://bitbucket.org/toon_vc/cobras_ts/src>
  * Input: timeseries with a label (FP, FN, TP, TN)
  * Output: improved clustering 
  * Difficulty: we need a fixed length representation as timeseries for COBRAS. Since the segment lengths are different (from 1 tuple to 800 tuples), we cannot simply use e.g. a temperature variable. Another idea is to use the k worst anomaly scores of each segment. Like this we can fix the feature vector size with k.
  
## 26/03/2019

CI

* Added tests to perform entire grid search / feature selection every night automatically 
* CI jobs sometimes failed due to time-out of MongoDB (Gitlab runner failure). I added `retry` to the jobs to work-around this issue. 

CD

* Created a CERN web service: http://anomaly-detection-mki.web.cern.ch (only accessible from CERN intranet)
* Configuration of Python app in Openshift is connected to Gitlab repository. Gitlab is connected to Openshift via a token (see environment variables in Gitlab).
* Goal: deploy the best grid search every night to the server.

COBRAS

* Updated README with info. Segments CSV file is generated during grid search and pipeline. 
* Interaction via a command line and a Jupyter notebook. 
* How to define the 'data' on which we cluster? This should be fixed length for COBRAS. Pieter suggested the following features:
  * Maximum frequency values of continuous data (temperature, pressure)
  * Min / max / avg features
  
* TODO: Implement the new features
* TODO: The clustering results should be stored somehow and re-used later for new instances. 
  
General

* Currently feature selection uses a fixed set of parameters for the grid search. 
* Grid search uses a varying set of parameters for a set of features. 
* It would be ideal to use varying parameters during the feature selection. However this is infeasible with all features on all data. Maybe try this for 1 month


## 02/04/2019

*Cross validation: different time for training / evaluating
  * So far limited to March -> September to only compare months with each other
  * Train on 2016, evaluate/test on 2017: AUC = 0.242
  * Train on 2017, evaluate/test on 2016: AUC = 0.077
  * The last months of 2017 would be useful for training -> TODO ask Pieter
  * Idea: train on first 3 months, evaluate on entire year
  
* Evaluate using ranking
  * Avg ranking of anomalies should be as low as possible
  * Could replace AUC as evaluation metric
  
* COBRAS
  * I proposed the following approach to re-use the result of a COBRAS clustering. So: first we execute the pipeline for a certain period (2016), this gives predictions for segments. Using COBRAS the goal is to improve these predictions of segments. By interacting via a notebook, we indicate for some segments whether they belong to the same cluster or not. We store the resulting labels of this clustering.
  * Next, we execute the pipeline for another set of features (2017). This gives predictions for segments. We do not want to cluster interactively every time, so we re-use information from the previous time. We fit a K Nearest Neighbor classifier using the labels of the old set (2016). Use this classifier to predict the cluster for each segment of 2017.
  
  * We do not want to answer a lot of questions. Therefore we use the labels from the original prediction as a 'smart' oracle to say whether 2 segments should belong to the same cluster or not. There are 2 uses for this. 
  * First, we want to reduce the number of FP. Maybe it was a TP but there was no event in the logbook.
  * Second, we want to reduce th enumber of FN. An anomaly was logged in the book but our anomaly detector did not find this. Answering questions with FN can help to do reduce this amount. 
  
  * Also important is the initial clustering of COBRAS. We expect this to be clustered using the initial labels, but it might not be the case. To be investigated. 
  
* Deployment
  * Currently the deployed version of Dash on the web is not the best / most recent version. This requires some manual build on openshift. 
  * Maybe later, it is no priority. 
  
* TODO
  * Rewrite README.md: currently not 1 text
  * Copyright into every file: need or not? Elia will ask Wannes.
  
## MongoDB on OpenShift

To manage pods via the command line, the ["OKD CLI"](https://docs.okd.io/latest/cli_reference/get_started_cli.html) must be installed. 
Login and use `rsync` to copy files to the pod containing the Mongo instance:

```sh
oc login openshift.cern.ch # anomaly-detection-mki project
oc get pods # look for a pod starting with mongodb
oc rsync [local_dir]  [podname]:/data-cern
```

Perform a restore inside the container:

```sh
oc rsh [podname]
# now you are inside the pod
mongorestore -d cern_ldb --archive=/data-cern/cern_ldb.mongodb-2017_09_24-2018_01_02.gz --gzip --noIndexRestore --username=[username] --password=[password]
```

## 09/04/2019

* Updates
  * COBRAS clustering with notebook is easier + added plots of results
  * Last 3 months of 2017 data added by Pieter
  * Build features refactored: logically split functionality over files
    * Parallel version did not work in CI due to multiple connections to Mongo
  * Custom build for deploy: OK
    * Quite some failed builds due to expired token
    * Created long-living token of service account
  * Clean-up of notebooks

* Cross-validation
  * First results: see below
  * Some magnet of beam 1 was replaced in 2017. Therefore cross validation Q1/2 <-> Q3/4 2017 might not work well. => Also test on beam 2
  * TODO: cross-validate using subset of best features 
  * TODO: add beam 2
  * TODO: cross-validate using shorter periods of time: train Q1, test Q2; etc... 

```
* Training model on Q1/2 2017, predicting anomalies for Q3/4 2017 *
Initialization 0
  Iteration 10
  Iteration 20
Initialization converged: True
|| Trained detector: 8.5 seconds
|| Scored features: 2.7 seconds
|| Area under PR curve = 0.012145936842979306

* Training model on 2017, predicting anomalies for 2016 *
Initialization 0
  Iteration 10
Initialization converged: True
|| Trained detector: 99.2 seconds
|| Scored features: 4.0 seconds
|| Area under PR curve = 0.14398666068398938

* Training model on 2016, predicting anomalies for 2017 *
Initialization 0
  Iteration 10
Initialization converged: True
|| Trained detector: 123.3 seconds
|| Scored features: 3.7 seconds
|| Area under PR curve = 0.2374740468459146 
```

* Live simulation
  * The idea is to see how good our detector works for 'real world' application at CERN. To predict week X+1, we use train the model on week 1 until week X and then test on week X+1.  This process is repeated on a weekly basis. 
  * For the beginning of 2017 the idea is to re-use the model of 2016. Keep doing this until the model of 2016 performs better. 
  
* Anomalies without a segment
  * March 28, 2017
  * February 22, 2017
  * I observed that there are situations where an anomaly exists but there is no segment created. This leads to the fact of ignoring these events in the logbook. This might be the case because the STATE:CONTROL is local (these values are ignored), or because STATE:MODE has no occurrence of on - <anything>. 
  * Since both cases occur during a local state, we ignore these anomalies. They are not taken into account.
  
## 16/04/2019
* Cross validation: 
  * MKI2D was replaced in Nov. 2016 (i.e. not 2017). - M. Barnes 
  * Added cross validation for beam 2: bad results -> see GitLab
* Live simulation 
  * Sometimes fails due to small time periods -> no values for certain features
  * Idea: build features once for entire time span, only use subset at train time
* Ranking metric
  * Alternative for AUC
  * Ev integratie met thresholds
  
  Ranking, AUC, threshold
  TP FP
  FN TN 
  
  45, .304, .99
  4 5 
  4 566
  
  32 .443, .975
  6 9 
  2 562
  
  67 .509, .99
  4 2 
  4 569
  
  54 .503, .985
  5 4 
  3 567
  
  Conclusion: higher AUC does not always mean better predictions!

* Added fade gray for updating in web app
* Used parser everywhere for parsing arguments in scripts -> cleaner

* Text:
  * Introductie hoofdstukken afwerken -> doorsturen naar Elia
  * Evaluatie: offline <-> online mode
  * Feature selectie
  
* Negative frequencies in COBRAS
  * Strange? Well, since all features are scaled this is also the case for the frequencies. Since there is mean subtraction and scaling, it will also have negative values. 
  
# 07/05/2019 

## Live evaluation
* Current grid search approach also takes into accounts knowledge of test data => not good
* Idea: use grid search for the training data, extract optimal hyperparameters and use these for a pipeline on the test data

* Differentiate between all features and subset of features 

## Web app 
* Change data to 1 plot, easier for sharing the 'zooming'

## COBRAS
* Has been tested but output looks a bit strange: only 2 clusters are maintained 
* Visualise using confusion matrix instead of plotting
* Look into this to catch the bug

# 14/05/2019

## CI: runners often fail, created a ticket

1) Failing because of insufficient space.

Your repository is 3.3 GB (https://gitlab.cern.ch/te-abt-ec/anomaly-detection-mki-2019 under files). Plus the cache, and the artifacts. And you run several (~20) parallel jobs in the same stage. A given runner has ~20 GB of free disk space and we have ~10 slots.

You must reduce the total size of the repository by a factor of 10, and/or reduce the number of parallel executions.

2) Failing because of exceeding the maximum execution time.

Your job was killed after 3 hours. We have been checking your jobs and we think that maybe they are more suited for a batch job in Condor, not a CI pipeline. Other option is to setup your own runner, the shared public runners are not prepared for so big size repositories or execution time.

## COBRAS

### Questions/remarks Pieter:
* using two tabs to http://anomaly-detection-mki.web.cern.ch/ to check the data for the below 'instances'

* ideally a link to the app, with the selected timestamps preselected, should be generated. Also all magnet pressure datasets should be preselected.
=> fixed

* cern_segments.csv is for which beam (seems to be B1/UA23 but how come?)
* unclear which scoring method has been used
=> fixed

* idea for app: instead of showing one graph/dataset, why not simply add the chosen dataset to a single fixed graph? This would allow 1/ fixing the auto-scale problem and 2/ ensure that for instance pressure units (bars) share the same scale
* difficult now when we see all 4 magnet pressures, to have the same scale and thus understand the variance wrt to the others. Hence difficult to see spark signatures
=> fixed with the 2 data viewers

* annoying online app data format: mm/dd/yyyy ! 
=> fixed

* online app 'STATE:MODE segments' TN, TP, FP, FN legend is on top of the x-axis, hard to read 
=> fixed

### Other

* Added confusion matrix

## Live evaluation
Grid search was used (only on the training data)

## Text
* Processed feedback

### Results

* INTERACTIVE IN 2017, NO INTERACTION IN 2017:

TP: 6 | FP: 9
FN: 2 | TN: 562

=>

TP: 7 | FP: 1
FN: 1 | TN: 570

* INTERACTIVE IN 2017, NO INTERACTION IN 2018:

TP: 4 | FP: 33
FN: 3 | TN: 1406

=> 

TP: 6 | FP: 2
FN: 1 | TN: 1437