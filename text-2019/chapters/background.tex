\chapter{Background}
\label{ch:background}

In this chapter the necessary background information is introduced.
We focus on the CERN LHC, anomaly detection, feature selection and an interactive clustering method called COBRAS. 
Finally, a summary of the previous work on this research study is given.

\section{The Large Hadron Collider}\label{sec:lhc}
The LHC is the largest and most powerful particle accelerator in the world \cite{cernLHCSite}.
It was built by CERN, the European Organization for Nuclear Research which is situated in Geneva, near the border of France and Switzerland.
Physicists at CERN use the LHC to study advanced properties of particles.
One of their missions is to simulate the first moments after the Big Bang and to learn more about the beginning of the universe.

\begin{figure}
	\centering
	\includegraphics[width=.4\textwidth]{img/background/lhc_tunnel}
	\caption{The LHC tunnel (source: \cite{Pantelia:1645024})}
\end{figure}

The LHC is the largest machine on the planet \cite{cernLHCSite}.
It is a ring with a circumference of 27 kilometres, lying under the ground at a depth of \SI{100}{\metre} on average.
Inside the ring two beams of particles travel in opposite directions.
Using specialised superconducting magnets, the particles are accelerated until they travel at nearly the speed of light.
Then they are made to collide.
These interactions are studied by physicists. 

\subsection{The accelerator complex}
Particle accelerators in general have a limited dynamic range to increase the energy of the particles.
To achieve the levels needed to reach a speed close to the speed of light, a chain of accelerators is needed.
At the beginning, particles are injected into a first accelerator which increases the energy up to the maximum level it can achieve.
Then the particles are extracted from the current accelerator and injected into the next one.
By iterating this process, the beam of particles travels through a sequence of machines.
It continues until the particles are injected from the Super Proton Synchrotron (SPS) into the LHC.
Here, the energy level is increased for a last time up to the maximum energy level of  6.5 \si{TeV}.
Then the beams are made to collide.
An overview of the CERN accelerator complex is shown in Figure~\ref{fig:accelerator_complex}. 

\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{img/background/lhc_accelerator_complex}
	\caption{The CERN accelerator complex (source: \cite{Mobs:2636343})}
	\label{fig:accelerator_complex}
\end{figure}

\begin{comment}
The beams of particles start their journey in the first accelerator, Linac 2. The energy of the protons is increased to 50 \si{MeV}. 
Then they are injected into the Booster. 
Here the beams are divided into four packages, which will maximize the intensity of the beam. Electric fields are used to increase the energy of the protons, while magnets are used to bend the particles around the circular form of the Booster. Now the protons have an energy of 1.4 \si{GeV}. 
Next, the protons are transferred to the Proton Synchrotron (PS) where they are accelerated to 25 \si{GeV} and thereafter to the Super Proton Synchrotron (SPS). Now the particles have a charge of 450 \si{GeV}.
The last step is to inject them into the LHC, the final accelerator. The particles arrive in so-called \emph{bunches}, small groups of particles instead of the entire beam. A process of 20 minutes accelerates the particles to 6.5 \si{TeV}, after which the particles remain usually for hours in the accelerator during experiments.
\end{comment}

\subsection{Injection Kicker Magnets}
Within the accelerator complex, particle beams are extracted from one accelerator and injected into another one.
The hardware responsible for this action is called \emph{beam transfer equipment}.
Within CERN, it is the responsibility of the Accelerator Beam Transfer (ABT) group to design and maintain this equipment.
Critical for LHC operation are the Injection Kicker Magnets (MKI) \cite{BarnesM.J.2011Iaem}.

A MKI installation consists of four magnets, which are named A, B, C and D.
Each pair of magnets (A and B, C and D) is powered by one generator, which has one Resonant Charging Power Supply (RCPS) and two Pulse Forming Networks (PFNs); thus one PFN per magnet. 

The LHC requires two injection installations (named MKI2 and MKI8), one for each counter-rotating beam.
This brings the total of magnets to 8.
Each system of MKI magnets has a certain location and installation name, see Table~\ref{tbl:location_installation_beam}.
They will be referred to in the variable names of the dataset.

\begin{table}[]
	\centering
	\caption{MKI magnet installation names}
	\label{tbl:location_installation_beam}
	\begin{tabular}{|l|l|l|}
		\hline
		Location & Installation name & Beam \\ \hline
		UA23     & MKI2              & B1   \\ \hline
		UA87     & MKI8              & B2   \\ \hline
	\end{tabular}
\end{table}

\subsection{Kicker injection soft start conditioning}
As described above, the MKI magnets' function is to inject particles into the LHC.
This process takes a relative short time, whereas the experiment inside the LHC goes on for several hours.

During a physics run the magnets cannot be pulsed, since this would influence the ongoing operation.
However since the magnets require a high voltage level, de-conditioning might occur during that time.
When a magnet at a low pressure level (vacuum) is pulsed in this condition with high voltage, it might cause a vacuum breakdown (i.e. sparking inside the magnet).

To avoid this problem, a Kicker Injection Soft Start (KISS) phase \cite{barlow2010kiss} has been added.
It slowly increments the voltage level in a safe way until the nominal voltage is reached.
The KISS conditioning is executed in parallel with a start-up phase for setting machine parameters before each beam injection. It is relevant for this work because it will generate a very specific pattern in the data which is introduced in Chapter~\ref{ch:data}.

\subsection{LHC schedule} \label{sec:lhc_schedule}
The usage of the LHC is scheduled beforehand for a duration of one year.
Periods of normal usage are interleaved with machine development and planned technical stops.
Every year there is a Year End Technical Stop (YETS) starting in December and continuing until the beginning of March.
When modelling normal behaviour of the machine, data generated during maintenance or test periods should be excluded.

\subsection{CERN accelerator logging service}
During LHC runs a vast amount of operational machine data is generated.
To store all data from these devices, the CERN Accelerator Logging Service (CALS) \cite{Billen:1000757} is used.
It aims to centralise all information and to avoid duplicating the data logging effort.

CALS was originally developed in 2006 with an infrastructure of two Oracle databases. 
It was designed for a data volume of 5 TB per year \cite{Billen:1000757}. 
However in 2017 the throughput was 1.2 TB per day. 
This dramatic increase of data load has led to performance issues for user queries. 
J. Wozniak (CERN) mentioned at the Spark Summit in 2017 that it took 12 hours to retrieve 24 hours of data. 
This situation is clearly not desirable. % https://www.youtube.com/watch?v=80-csZJK7kc

Therefore a next generation logging service (NXCALS) \cite{Jurasz:2290902} is currently developed.
It makes use of Hadoop and Spark clusters and enables more efficient analysis of bigger data sets over a longer period of time. 

\begin{comment}
\subsection{Service for web based analysis} \label{sec:swan}
Service for Web based ANalysis (SWAN) \cite{PIPARO20181071} is a platform that was developed by CERN for interactive data analysis in the cloud. A user gets (without installing any software) access to a Jupyter notebook and a shell via the browser.  

Using SWAN has several advantages.  First, the platform has direct access to NXCALS. This means that there is no longer need for a local database. Second, it offers access to PySpark, a Spark interface for Python. This makes it possible to perform intense data operations on the CERN cluster. The system provides automatic parallelisation which is beneficial for ...?

\begin{figure}[]
\begin{center}
\includegraphics[width=.3\textwidth]{img/logo_swan}
\caption{SWAN: A service for interactive analysis in the cloud}
\label{default}
\end{center}
\end{figure}
\end{comment}

\section{Anomaly detection}\label{sec:anomaly_det}
\emph{Anomaly detection} (or \emph{outlier detection}) is the task of detecting abnormal patterns in data which are called \emph{anomalies} (or \emph{outliers}).
It can be interpreted as a binary classification task: classify an example as anomalous or not.
There is a wide range of applications in anomaly detection: card fraud detection, malignant tumor detection, etc.
We formally define anomaly detection as a mapping from input to output:
\begin{itemize}
	\item \textbf{Input:} a set of input vectors with values for certain features
	\item \textbf{Output:} the set of input vectors with an additional value per feature: an anomaly score $\in [0,1]$
\end{itemize}
By convention a high score indicates a high probability of being anomalous and vice versa.
Anomalous examples will be referred to as the \emph{positive} examples, whereas normal examples are referred to as \emph{negative}. 

Using the output of an anomaly detector, a \emph{binary prediction} (anomalous or not) can be made for each input vector.
Choose a threshold $t$ and predict the vector as anomalous if its score is higher than $t$, otherwise predict the vector as not anomalous.

\subsection{Unsupervised learning}
Anomaly detection is usually applied in the setting of unsupervised learning.
This means that training examples are not labelled, but assumed to be normal (negative).
When the number of anomalies compared to the total number of examples is rather small, it is reasonable to make this assumption.
This approach is also used in this work.

\subsection{Interpretability}
Some anomaly detectors cannot only detect anomalies but also offer possibilities to track down the cause of a predicted anomaly.
We refer to this property as \emph{interpretability}. 

Interpretability is especially useful for the CERN application. Since the dataset consists of 120 variables, it would be an important benefit to know which variable or even subset of variables caused this anomaly.

\subsection{Techniques} \label{sec:gmm_iforest}
Different techniques have been applied to the problem of anomaly detection. 
A structured overview of all techniques is given in the survey of V. Chandola \cite{ChandolaV.2009AdAs}.
This work focusses on two techniques called Gaussian Mixture Model and Isolation Forest. 

\subsubsection{Gaussian Mixture Model} \label{sec:gmm}
A Gaussian Mixture Model (\gmm) assumes that each of the given data points belong to one of $k$ Gaussian clusters.
These clusters are determined using an Expectation-Maximization algorithm.
It is a natural choice to assign a data point a high anomaly score, if it has a low probability to belong to any of the $k$ clusters, and vice versa.

The performance of a \gmm~is influenced by the number of components.
When the number of components is too small, useful relations within the data might not be learnt. 
On the other hand, when there are too many components overfitting will occur.
A theoretical method to determine this parameter is the Bayesian Information Criterion (BIC) \cite{schwarz1978estimating}. 
The model with the lowest BIC score is usually optimal. 
However, W\'ery has followed this approach \cite{WeryNiels2017Advd} but concluded that for two or more components the BIC score was constant.

A disadvantage of GMM is the lack of interpretation.
There is no clear connection between certain variable values and a predicted anomaly.

\subsubsection{Isolation Forest} \label{sec:iforest}
In contrast to the previous method Isolation Forest (\iforest) does not model normal behaviour but explicitly isolates anomalies.
This idea was proposed by Liu, Ting and Zhou \cite{liu2008isolation} in 2008 and is implemented with an ensemble of decision trees.
At every node of the decision tree, a set of instances is partitioned in a random fashion.
The process is continued until all instances are isolated. 

Isolation Forest assumes that anomalies are easier to isolate and hence will lead to a shorter path length.
The more the attributes of the data distinguish between normal instances and anomalies, the easier this process is.
A visualisation is shown in Figure~\ref{fig:iforest}.
Isolating the normal point $x_i$ requires a higher number of splits, whereas isolating the anomalous point $x_o$ requires a lower number of splits.

\begin{figure}
	\centering
	\includegraphics[width=.8\textwidth]{img/background/iforest}
	\caption{Isolating $x_i$ and $x_o$ (source: \cite{liu2008isolation})}
	\label{fig:iforest}
\end{figure}

A main advantage of Isolation Forest is the support for interpretability. 
One can easily observe a single decision tree of the ensemble and inspect the nodes.
It is exactly known why an instance was predicted as anomalous.
By combining this information of all trees in the ensemble, one can get a certain intuition about the cause of a predicted anomaly.

% TODO disadvantage: computation time

\subsection{Evaluation metrics} \label{sec:confusion_matrix}
Formal evaluation metrics are necessary to evaluate an anomaly detection application.
They can also be used to compare the performance of different detectors.

For an evaluation metric, it is assumed that the ground truth of each example is known.
By choosing a threshold $t$ and assigning labels as described above, we can now compare the prediction to the ground truth.
The result is summarized in a so-called confusion matrix.
It contains four classes: true positive (TP), false positive (FP), false negative (FN) and true negative (TN).
They are defined in Table \ref{tbl:confusion_matrix}.

\begin{table}
	\centering
	\caption{Confusion matrix}
	\label{tbl:confusion_matrix}
	\begin{tabular}{|l|l|l|}
		\hline
		& actually positive & actually negative \\ \hline
		predicted as positive & TP & FP                \\
		predicted as negative & FN & TN             \\ \hline 
	\end{tabular}
\end{table}

\subsubsection{Precision and recall}
The quality of the output is often expressed in terms of precision and recall.
Precision indicates how many of the examples that were predicted anomalous are actually anomalous.
Recall indicates how many of the actual anomalies were predicted as anomalous. 

\begin{align}
\text{precision} &= \frac{TP}{TP+FP}\\
\text{recall} &= \frac{TP}{TP+FN}
\end{align}

In a more intuitive way: high precision means that predictions of anomalies are most of the time an actual anomaly, while high recall means that most of the anomalies were actually found with the detector.

\subsubsection{Area under the Precision-Recall Curve}
Every threshold yields a value for both precision and recall.
The combinations form a curve which is called the Precision-Recall Curve (PR Curve).
The area under the PR curve (\auc) is a good indication for the quality of the detector.
An ideal detector has an \auc~of 1.

\subsubsection{Average anomaly rank} \label{sec:rank}
Another metric is the average anomaly rank (\rnk).
Let $l$ be the list of predictions, sorted by decreasing anomaly score.
List $l$ contains elements $l_0,...,l_n$ where $score(l_{i})>score(l_{i+1})$ for $i\in\{0,...,n-1\}$.
We define the average anomaly rank as:
\begin{align}
\rnk = \frac{\sum_{l_i \text{is actual anomaly}} i }{TP + FN}
\end{align}
where the denominator denotes the number of anomalies.

Suppose there are four instances to predict, of which three are anomalous (A) and one is normal (N).
A first perfect detector would assign the highest anomaly scores to the anomalous examples. 
A second detector however might incorrectly assign a higher anomaly score to a normal example. 
These orderings together with the resulting \rnk~are shown in Table~\ref{tbl:rank}.
It is clear that a \rnk~of $1$ represents a perfect detector. 
The higher the \rnk, the worse the anomaly detector.

\begin{table}[b]
	\centering
	\caption{Computing \rnk}
	\label{tbl:rank}
	\begin{tabular}{l|ll}
		Element & Detector 1 & Detector 2\\ \hline
		$l_0$ & A & A \\
		$l_1$ & A & N\\
		$l_2$  & A & A\\
		$l_3$  & N & A\\\hline
		&&\\
		\rnk & $\dfrac{0+1+2}{3} = 1$ & $\dfrac{0 + 2 + 3}{3} = 1.67$\\
	\end{tabular}
\end{table}

\section{Feature selection}
\emph{Feature selection} is the process of selecting a subset of features which are then used to perform a task.
In our case this is the training and evaluation of an anomaly detector.
Selecting an optimal subset is a preprocessing step which reduces the data dimensionality.
It should lead to a better result, be computationally less intense and yield a higher interpretability \cite{liu1998feature}.  

 Popular feature selection algorithms have been analysed by K. Sutha \cite{guyon2003introduction}.
 This work has led to a classification of the algorithms into three categories: Filter, Wrapper and Hybrid Method.
 
 The \emph{Filter Method} selects the feature subset based on the data itself.
 It is independent of the task for which the data is used. As a consequence it is very general.
 The \emph{Wrapper Method} selects the feature subset based on its predicted performance on the task.
 This is usually a computationally more intense method for large datasets, yielding better results.
 The \emph{Hybrid Method} combines the previous approaches.
 It will first perform a Filter Method and then a Wrapper Method.
 It is a trade-off between the low computational intensity of the Filter and the higher quality result of the Wrapper Method.
 
\section{COBRAS: interactive clustering with super-instances} \label{sec:cobras}
An anomaly detection application assigns anomaly scores to instances, leading to a binary prediction.
Usually these results are reported to a team of experts.
However, some types of anomalies might not be of interest to the experts.
It might be a normal situation where equipment is replaced causing an anomaly.
This is a false alarm which should not be examined.
Therefore it is beneficial to incorporate expert feedback.
The feedback could be: ignore a certain type of ``intervention'' anomalies.
In this work a method to achieve this is proposed using the interactive clustering algorithm COBRAS \cite{DBLP:journals/corr/abs-1803-11060}.

Clustering is the task of classifying a set of input data into multiple clusters.
The motivation to use COBRAS is that clustering is an inherently subjective task.
The desired clustering does not only depend on the given dataset, but also on the goal for which it is used.
Therefore user interaction is required to obtain the target clustering.

In the case of COBRAS, user interaction is provided in the format of pairwise queries.
Given two instances, the system queries the user with the question: \textit{``Should these 2 instances belong to the same cluster?''}.
The answer (yes or no) results into a \textit{must-link} or \textit{cannot-link} constraint.
A \textit{must-link} constraint means that the instances must belong to the same cluster, whereas a \textit{cannot-link} constraint expresses that they cannot belong to the same cluster.

Central in the work of COBRAS is the concept of a \textit{super-instance}.
Van Craendonck defines it as \textit{``a local region in the data in which all instances are assumed to belong to the same cluster''} \cite{DBLP:journals/corr/abs-1803-11060}.
It gives rise to a hierarchy of clusters.
A clustering is a set of clusters, and a cluster consists of set of super-instances.
A super-instance consists of a set of instances.

Given the concept of constraints and super-instances, COBRAS builds a clustering in an iterative process.
Each iteration consists of two steps: refine and merge.
The first step refines the super-instance into smaller super-instances based on K-means.
The second step merges the super-instances using the input of the user in the form of constraints.
An example is shown in Figure~\ref{fig:cobras_clustering}.
This iterative process ends with a clustering when the so-called \emph{query budget} has been spent.  
It limits the number of questions to be answered by the expert.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{img/background/cobras_clustering}
	\caption{Constraint-based clustering with COBRAS (source: \cite{van2018cobras})}
	\label{fig:cobras_clustering}
\end{figure}

\section{Previous work} \label{sec:previous_work}
Niels W\'ery \cite{WeryNiels2017Advd} and Armin Halilovic \cite{HalilovicArmin2018ADft} have previously worked on an anomaly detection application for the MKI magnets at CERN.
This section discusses the most important contributions of their work.

W\'ery was the first one to work on this topic. 
He designed a Mongo database abstraction for querying the data provided by CERN.
It is still being used.
W\'ery trained a model using GMM on a feature dataset consisting of continuous data, IPOC data and sliding window features. 
The results were evaluated using the concept of anomalous segments.
Such a segment was quite arbitrary since it was determined by a constant parameter and not linked to the usage of the machine.
W\'ery suggested to use other anomaly detection methods, to improve the interpretability of the model. 

Halilovic continued the work of W\'ery. 
His main contribution is an anomaly detection pipeline consisting of four steps: preprocessing, anomaly detection, postprocessing and evaluation. 
The preprocessing step transforms the data into features that can be used for training a model.
There are three transformations. First, it filters out wrong sensor measurements, as provided by CERN.
Second, it computes sliding window features of continuous data.
Third, features are scaled before they are passed to the training phase. 
The anomaly detection step trains and scores the given features.
Halilovic proposed using Isolation Forest as an anomaly detector and compared the results with GMM and a few other dummy detectors.
The postprocessing step then segmented the examples.
This segmentation is based on a fixed parameter.
Each segment is given a ground truth and predicted score.
The evaluation step is the last step of the pipeline.
Its input consists of segments with a ground truth and a predicted anomaly score.
Using a threshold, the predicted scores were converted to predictions.
Standard evaluation metrics were used to evaluate the results.

Halilovic concluded that GMM performed better than Isolation Forest on the data of 2015-2016.
Surprisingly the Isolation Forest  on the entire feature set performed worse than a dummy detector.
Then, he used a feature subset consisting of the IPOC data for training and testing Isolation Forest.
This experiment resulted in a better prediction than the dummy detectors but was still worse than GMM.
It gave the intuition that using a feature subset could help Isolation Forest to model anomalies, with the benefit of interpretability.
