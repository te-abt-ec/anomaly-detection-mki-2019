\chapter{Introduction}
\label{ch:introduction}

\begin{comment}
Avoid giving too many details in the introduction. Most of these things will be discussed in more detail later on. For instance, "what others have done" is typically included in a separate chapter "related work " or in the literature study. The introduction provides context of the work but also gives a preview of what will come. It briefly discusses the structure of the remainder of the text, and clarifies what is explained where (for instance, where the main contributions of the thesis are), so that a reader who, for instance, already has a lot of background and wants to skip straight to the description of the main novel elements knows where to find them.
\end{comment}

The Large Hadron Collider (LHC) is the world's largest and most powerful particle accelerator \cite{cernLHCSite}.
Maintaining a machine of this size in an efficient and sustainable manner requires some effort.
Nowadays, problematic situations (anomalies) during operation are too often detected late in the process by experts who analyse the data.
Tracking down what actually caused the anomaly is done manually.
It is clear that valuable time is lost during these periods of equipment failure.
Furthermore discovering and analysing anomalies is a tedious process.
It requires a strong knowledge of the complex relations between the components of the LHC and does not scale for future -- larger -- accelerators.

This thesis proposes an anomaly detection application which will automatically detect anomalies based on a model of the injection kicker magnets behaviour.
Such an application can potentially save a lot of time for CERN engineers.
Results of root cause analysis can further guide the engineers to improve the LHC for the future.

Chapter \ref{ch:background} of this work provides all necessary background information.
First, an introduction to the operation of the LHC and the injection kicker magnets is given.
Next, the anomaly detection problem is formally defined.
Common anomaly detectors and evaluation metrics are given.
The main principles of feature selection and the clustering algorithm COBRAS are introduced.
Finally, the main contributions of previous work on this application by N. W\'ery and A. Halilovic are summarized.

Chapter \ref{ch:data} introduces the different types of data which are used for the anomaly detection application.
The asynchronous sources make it challenging to combine all kinds of data into a feature vector.
A second challenge is to incorporate noisy labels as a ground truth for evaluation.

Chapter~\ref{ch:problem_statement} defines the problem and motivates the development of an anomaly detection application. 
Although a substantial amount of work has been invested in this application, there are remaining open problems.
First of all, the current application does not detect certain anomalies.
This is problematic since CERN wants to be notified when problems occur.
It is not clear why the model cannot detect these.
Second, it is hard to interpret the output of the anomaly detection application.
In previous work, predictions were made for certain time intervals which were not related to the usage of the injection kicker magnets.
Third, the visualisation of the results was quite primitive.

Chapter~\ref{ch:anomaly_detection} presents the core of this work. 
It explains how the open problems stated in the previous chapter are addressed by our application. 
First, previously undetected anomalies are now detected thanks to feature engineering and fine-tuned raw data filters. 
Second, new state variables are used to generate predictions that make sense. 
Predictions are directly linked to the single usage of the machine.
Third, a web application is presented which can be used in an interactive way for interpreting and improving the anomaly detector. 
These three main contributions are combined in a pipeline. 
It includes feature engineering, selection and a new segmentation method. 

Chapter~\ref{ch:results} gives the the results of an extensive series of experiments of the anomaly detection application.
A comparison is made on different levels. 
The first level varies the set of features which is used. 
It demonstrates the impact of selecting an appropriate subset of features. 
The second level varies the mode of the detector: offline or online. 
It is the first time that an online experiment is performed.
