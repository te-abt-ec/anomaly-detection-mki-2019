\chapter{Conclusion}
\label{ch:conclusion}

\begin{comment}
CONCLUSIONS

Briefly recall what the goal of the work was. Summarize what you have done, summarize the results, and present conclusions. Conclusions include a critical assessment: were the original goals reached? Discuss the limitations of your work. Describe how the work could possibly be extended in the future, mitigating limitations or solving remaining problems.
\end{comment}

The goal of this work was the development of an anomaly detection application for the injection kicker magnets of the LHC, which supports both an offline and online mode. 
In this last chapter we summarize our main contributions and propose future research directions.

\section{Main contributions}

In Chapter~\ref{ch:anomaly_detection} an anomaly detection application has been proposed.
It is structured as a pipeline which includes preprocessing, detection, postprocessing and evaluation. 
A final visualisation step shows the input data and resulting predictions in an interactive explorer.
New state and controller variables were added as features. 
The data used is more recent and has a larger timespan of 3 years compared to 1.5 year before.
Additionally, the feature engineering step was extended with Fourier features (frequency and intensity).
Together with improved filters, these additions have led to a better model for detecting anomalies.
In the postprocessing step, we proposed an exact segmentation method based on the state data. 
It replaces a previous method with a fixed \texttt{segmentation\_distance} parameter.
Our new method resulted in more meaningful predictions.

In Chapter~\ref{ch:results} the performance of the application has been tested in several experiments.
It is the first time that the application was tested extensively, with the new data.

First, a grid search has led to an optimal set of parameters per year together with good predictions.
Based on these results, we proposed a set of general parameters. 

Second, feature selection has led to an optimal subset of features per year for both anomaly detectors.
It consists of at most 16 features for \gmm~and 26 features for \iforest. 
Using a model trained on these subsets of features with the general parameters, the application detects the same amount of anomalies as with a model using all features with optimal parameters from a grid search.
The proposed feature subsets are beneficial, because they make the training process faster and allow for more generalisation.

Third, we moved from an offline to online setting.
Using the initially chosen thresholds, the results were worse, but when decreasing the threshold appropriately the model was still able to detect the same anomalies as in the offline mode (at the cost of a lower precision).
However, it remains to be examined how to adjust the threshold and how to cope with the resulting FPs.

Fourth, we have proposed a method for incorporating expert feedback. 
In an interactive way, experts can correct predictions by clustering the predicted segments of historical data. 
This knowledge is then transferred to new predictions.
It has led to promising results with a lower number of FPs.
Surprisingly the technique also helped to increase both precision and recall of \iforest~which did not perform well without the additional expert knowledge.

Based on the results of the experiments, we have concluded that \gmm~outperforms \iforest.
The latter one also detects anomalies correctly but its \auc~and \rnk~metric are worse than for \gmm.

\section{Future work}
Although the current application performs reasonably well, there are different open problems giving rise to new research directions. 

First, the current system does not provide \emph{interpretability}. 
When an anomaly is detected, it would be beneficial for CERN to indicate what exactly caused the anomaly.
Exploring other anomaly detectors could be an approach to solve this problem. 
In addition to the Isolation Forest interpretability, the MERCS \cite{van2018mercs} model, developed by Van Wolputte et al., could be explored.

Second, the more data to be used for training and prediction, the more \emph{computational power} will be needed. 
With the current amount of data it was for example unfeasible to exhaustively execute all configurations for a grid search.
For future particle accelerators, the data generated during experiments will be even larger, leading to a longer execution time.
However, CERN is working on a new logging system with a Spark back-end.  
This system allows automatic parallelisation and is developed for large-scale data operations.

Third, the interactive clustering COBRAS has shown to be useful in reducing the number of FPs.
However, at this point COBRAS has only access to a limited amount of attributes. 
Possibly the system could work even better, by providing more data of other types. 
This could be tested in future work.

Finally, it is possible that the FPs are in fact no false alarms but indeed anomalies with reference to the trained model. 
These FPs might not have led to equipment failure but could be telltale signs of failure. 
The correlation of the FPs with the found anomalies should be investigated.
