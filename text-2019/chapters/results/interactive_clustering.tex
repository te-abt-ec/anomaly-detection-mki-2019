\section{Incorporating expert feedback}

Most results of previous experiments have a high recall. 
This means that most anomalies were actually detected. 
However, sometimes the precision might be low which means that there are quite some false alarms. 

Although it is more important for CERN not to miss anomalies (FN) than to give a false alarm (FP), a low precision is not desirable.
FPs are situations where an alarm is caused, while there is no anomaly.
It is clear that the model somehow fails. 
It cannot classify a normal situation as not anomalous.

To solve this problem, we propose an additional step that interactively clusters the output of the anomaly detector using COBRAS (see Section~\ref{sec:cobras}). 
We aim to correct mistakes of the model by re-assigning it to the correct cluster of anomalous or normal segments.
This knowledge is stored and re-used for later predictions.
We have two reasons to introduce this step instead of adjusting the existing model.

First, it is hard for humans without expert knowledge to understand why certain segments are anomalous and others are not. 
There is a need for expert knowledge to actually classify these anomalies as normal situations. 
It concerns segments where we expect a good model to actually predict these cases as anomalies.

Second, we want to incorporate expert knowledge independent of the detector itself.
We do not want to give up the flexibility of using any anomaly detector.

So, in this fourth experiment we explain how the proposed clustering method works and discuss the results. 
It is useful because including expert feedback appears to be necessary. 
Our approach to include expert feedback is new.

\subsection{Interactive clustering}

Initially the anomaly detection application is used to predict segments as anomalous or not.
It is likely that there will be some FPs after comparing the predictions with the e-logbook. 
We aim to cluster the predicted segments using COBRAS.

\paragraph{COBRAS segments}
The available COBRAS implementation\footnote{\url{https://bitbucket.org/toon_vc/cobras_ts}} requires a fixed length representation per segment which is assumed to be useful for clustering.
Since the segments of the predictions, consist of data for a varying number of instances some choice must be made. 
We decided to create so-called COBRAS segments based on the prediction segments. 
It is important to carefully select their attributes.
We observed that for example the frequency of the pressure is an important indicator of an anomaly. 
A large difference between the minimum and maximum value often predicts an anomaly (see for example Figure~\ref{fig:cobras_segment949}).
Therefore we included this type of aggregated attributes.
All attributes are shown in Table~\ref{tbl:cobras_segments}.

\begin{table}
	\centering
	\caption{Attributes of a segment in COBRAS.}
	\label{tbl:cobras_segments}
	\begin{tabular}{|l|l|}
		\hline
		Attribute & Explanation \\ \hline
		nb        &  The number of instances in the segment\\
		y\_pred   &  The anomaly score of the segment\\
		freq\_temp & The  maximal frequency of the temperature\\
		min\_freq\_pressure & The minimal frequency of the pressure\\
		max\_freq\_pressure & The maximal frequency of the pressure\\
		timestamp\_min & The start of the segment\\
		timestamp\_max & The end of the segment\\ \hline
	\end{tabular}
\end{table}

\paragraph{Labels}
Besides the data attributes, each COBRAS segments has a label.
The label is the predicted class for a fixed threshold. 
It can be TP, FP, FN or TN.

\paragraph{Questions}

During the clustering, COBRAS asks questions of the format: ``\textit{Should segment X and segment Y be in the same cluster?}'' (see Figure~\ref{fig:cobras_query}).
A hybrid querier is added around COBRAS to assume that only FPs might possibly need to be re-assigned to another cluster.
It is called hybrid because it answers questions concerning non-FP segments automatically with \textit{no}, but requires interaction for questions concerning at least one FP segment.
The user should answer yes to a query if the given FP is actually a false alarm and is proposed to be clustered with a FN.
To decide this, the data of the segment must be inspected by an expert.
An example is shown in Figure~\ref{fig:data_explorer3} where an actual FP is compared to a FN. 
The expert should cluster these segments together, to reduce the number of FPs.

\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{img/results/cobras/query}
	\caption{The system queries the user: ``\textit{Should segment X and segment Y be in the same cluster?}''. Automatically generated links guide the expert to the segments in the explorer, where pressure and temperature datasets are pre-selected.}
	\label{fig:cobras_query}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}{\linewidth}
		\includegraphics[width=\textwidth]{img/results/cobras/inspect3_1}
		\caption{Segment 916 (FP)}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\linewidth}
		\includegraphics[width=\textwidth]{img/results/cobras/inspect3_2}
		\caption{Segment 905 (TN)}
	\end{subfigure}
	\caption{The FP does not show any spikes in the data. The data pattern is similar to the given TN. The expert should answer the query with \textit{yes}, with the aim to cluster the FP as a TN.}
	\label{fig:data_explorer3}
\end{figure}

Although in the nominal case we expect the comparison of a true FP with a FN, there can be queries about any combination with a FP.
We discuss two special cases.

\subparagraph{Special case 1}
In Figure~\ref{fig:data_explorer2} a FP (with clear spikes in the pressure data) is compared to a TP.
Although there was no event in the e-logbook, CERN experts have indicated that these events are also anomalous and therefore of interest.
The expert should cluster these segments together with TPs, to ensure that these events trigger an alarm.

\subparagraph{Special case 2}
In Figure~\ref{fig:data_explorer1} a FP is compared to a TN. 
In the normal case, we would like to assign the FP to the cluster of the TNs.
However, in this case the FP contains a spike and is anomalous, although there is no log.
This case must trigger an alarm and therefore the expert should answer with no.

\begin{figure}
	\centering
	\begin{subfigure}{\linewidth}
		\includegraphics[width=\textwidth]{img/results/cobras/inspect2_1}
		\caption{Segment 103 (FP)}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\linewidth}
		\includegraphics[width=\textwidth]{img/results/cobras/inspect2_2}
		\caption{Segment 1290 (TP)}
	\end{subfigure}
	\caption{Special case 1: both the FP and TP contain abnormal patterns in the pressure. The expert should answer the query with \textit{yes}, with the aim to cluster the FP as a TP.}
	\label{fig:data_explorer2}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}{\linewidth}
		\includegraphics[width=\textwidth]{img/results/cobras/inspect1_1}
		\caption{Segment 629 (FP)}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\linewidth}
		\includegraphics[width=\textwidth]{img/results/cobras/inspect1_2}
		\caption{Segment 949 (TN)}
		\label{fig:cobras_segment949}
	\end{subfigure}
	\caption{Special case 2: the FP contains a clear spike in the pressure, whereas the TN shows normal behaviour. The expert should answer the query with \textit{no}.}
	\label{fig:data_explorer1}
\end{figure}

\paragraph{Store the information}
When the query budget of COBRAS is exhausted, it will present a clustering. 
This clustering, together with the set of input segments is stored for later use.

\subsection{Re-use of the clustering}
Now we assume to have predictions for segments of another time interval, obtained with the pipeline of Chapter~\ref{ch:anomaly_detection} (without clustering).
The goal is to improve these predictions with the additional knowledge incorporated in the COBRAS clustering. 
To do so, we first train a k-nearest neighbour classifier with the previous COBRAS clustering.
Then we assign every new segment to its nearest neighbour.
If this is a TP or FN, the predicted anomaly score is set to 1. 
If it is a FP or TN, the predicted anomaly score is set to 0. 
Now these predicted segments are the improved predictions.
They can be evaluated and compared to the previous predictions without COBRAS improvement. 
 
\subsection{Experiment}
\subsubsection{Set-up}
As an experiment we have predicted the 2017 segments using the pipeline.
Then, we interactively clustered this predicted segments using COBRAS with a query budget of 250.
It means that COBRAS obtained knowledge of the relation between 250 segments. 
Most of these queries were answered automatically, because they did not concern a FP.
The expert feedback was stored in the format of this clustering. 

Next, the 2018 segments were predicted with the usual pipeline. 
We used the same segmentation score and thresholds as before. 

Then, we improved these predictions using the expert feedback of the 2017 COBRAS clustering.
As such the knowledge of the previous year is transferred to the current year.
This result is then the improved prediction for 2018.

Note that this improved prediction is based both on the scores of the anomaly detector of 2018 and the expert knowledge on the predictions of 2017.

\subsubsection{Results}

\paragraph{\gmm}
The confusion matrices for 2017 and 2018 (without and with improvement) are shown in Table~\ref{tbl:cobras_gmm}.
One can see that this method is effective. 
The number of FPs was reduced and two more anomalies were detected. 
This is the overall best result we have obtained for predicting 2018.

\begin{table}
	\caption{Confusion matrix and metrics using the pipeline with parameters of Table~\ref{tbl:gmm_general}, \anomdetect=\gmm, \thresh=99\%.}
	\label{tbl:cobras_gmm}
	\begin{subtable}{\textwidth}
		\caption{2017}
		\label{tbl:cobras_2017_gmm}
		\begin{subtable}{\textwidth}
			\centering
			\confmatrixpr{5}{8}{3}{1281}{0.38}{0.62}
		\end{subtable}
		\par \bigskip
		\caption{2018: without and with COBRAS improvement.}
		\label{tbl:cobras_2018_gmm}
		\begin{subtable}{.5\textwidth}
			\centering
			\confmatrixpr{4}{11}{3}{1428}{0.27}{0.57}
		\end{subtable}%
		\begin{subtable}{.5\textwidth}
			\centering
			\confmatrixpr{6}{2}{1}{1437}{0.75}{0.86}
		\end{subtable}
	\end{subtable}
\end{table}

\paragraph{\iforest}
The confusion matrices for 2017 and 2018 (without and with improvement) are shown in Table~\ref{tbl:cobras_iforest}.
The result is quite surprising.
Although \iforest~does not succeed in making good predictions with the anomaly detector itself, in combination with COBRAS it appears to give useful predictions.
Not only all FPs have disappeared, the number of FNs has decreased as well.

\begin{table}
	\caption{Confusion matrix and metrics using the pipeline with parameters of Table~\ref{tbl:iforest_general}, \anomdetect=\iforest, \thresh=95\%.}
	\label{tbl:cobras_iforest}
	\begin{subtable}{\textwidth}
		\caption{2017}
		\label{tbl:cobras_2017_iforest}
		\begin{subtable}{\textwidth}
			\centering
			\confmatrixpr{2}{63}{6}{1226}{0.03}{0.25}
		\end{subtable}
		\par \bigskip
		\caption{2018: without and with COBRAS improvement.}
		\label{tbl:cobras_2018_iforest}
		\begin{subtable}{.5\textwidth}
			\centering
			\confmatrixpr{1}{72}{6}{1367}{0.01}{0.14}
		\end{subtable}%
		\begin{subtable}{.5\textwidth}
			\centering
			\confmatrixpr{3}{0}{4}{1439}{1.00}{0.43}
		\end{subtable}
	\end{subtable}
\end{table}

\subsection{Conclusion}
We have shown that by incorporating expert feedback, the number of FPs can be decreased. 
In one setting, there was even no single FP remaining, giving a precision of 1.00.

Furthermore, experimental results suggest that COBRAS clustering is also helpful to transform FNs into TPs. 
Therefore a little amount of interactive feedback by experts can positively influence the performance of the detector.
