\section{Feature selection} \label{sec:feature_selection}
In a second experiment, we apply feature selection to reduce the number of features used for training and predicting.
This is useful because it might yield a subset of features resulting in better performance and a lower computational intensity.

\subsection{Set-up}
Feature selection \cite{guyon2003introduction} is the process of selecting a subset of features to use for the training of a machine learning model.
A first idea would be to try all subsets in a brute-force search.
However already without the generated features, our dataset consists of 120 variables.
Since a set of $n$ elements has $2^n$ subsets, this is not feasible.
Therefore we propose a greedy local search algorithm.

\subsubsection{Greedy local search}
The feature selection starts with an initial solution.
This might be an empty set of features or a fixed set of features.
Using this solution a set of neighbours is determined.
We define two types of neighbours:
\begin{itemize}
	\item Type 1: remove a feature from the current solution.
	\item Type 2: add a feature to the current solution (which is not in the current solution).
\end{itemize}

For all neighbours a cost will be computed.
The cost function can be configured as the \rnk~or inverse \auc, resulting from a \gmm~or \iforest~pipeline.

The neighbour with the lowest cost (lowest \rnk~or highest \auc) will be selected as the next current solution.
This process iterates until a maximum number of iterations has been executed.
Finally the best solution of the last iteration will be returned.

\begin{lstlisting}[caption={Feature selection}]
for i in range(0, max_iterations):
  neighbors = get_neighboring_solutions(solution, use_features)
  solution, df, score = select_neighbor(neighbors, compute_score)
\end{lstlisting}

\subsubsection{Initial solution}
As initial solution, we selected a set of 10 variables.
We decided to choose these variables, since they have shown to be a good decision in the first runs of the algorithm (starting from no variables).
Thanks to this initial solution, the algorithm will quicker find a solution.
However, one should be cautious that we might have led the algorithm into a local optimum.
One could decrease the chance of this problem by introducing other neighbour types which differ in for example more than one feature. 
We decided not to do this because of computational restrictions.

The initial solution is:
\begin{lstlisting}
'MKI.C5L2.B1:TEMP_MAGNET_UP_fft_freq_max', 
'MKI.A5L2.B1:TEMP_TUBE_DOWN_fft_freq_max',
'MKI.UA23.IPOC.CB1:T_DELAY', 
'LHC.BCTFR.A6R4.B1:BEAM_INTENSITY', 
'MKI.UA23.STATE:SOFTSTARTSTATE',
'MKI.UA23.F3.CONTROLLER:KICK_LENGTH_TOPLAY', 
'MKI.UA23.STATE:CONTROL',
'MKI.B5L2.B1:PRESSURE:SW_MEAN_DIFF_600_s', 
'MKI.B5L2.B1:TEMP_TUBE_UP',
'MKI.B5L2.B1:TEMP_TUBE_UP:SW_MEAN_DIFF_600_s'
\end{lstlisting}

\subsubsection{Parameters}
Since some optimal configurations require a substantial amount of computation time (e.g. \texttt{n\_components}=8), the parameters are configured according to the general set of parameters. For \gmm~and~\iforest~it is shown in respectively Table~\ref{tbl:gmm_general}~and~\ref{tbl:iforest_general}.

\subsubsection{Stopping criterion}
The stopping criterion is the number of iterations. 
Since each step of the feature selection evaluates many neighbours, an iteration is expensive (in execution time).
The number of iterations is fixed at 20 which is a priori an arbitrary decision.
A posteriori the results will suggest that this amount is sufficient.

\subsection{Results}
The resulting cardinalities of the feature subsets with their corresponding \rnk~are listed in Table~\ref{tbl:fs_res}.
The optimal feature subsets themselves are given in Appendix~\ref{app:feature_sets}.
The resulting predictions can be compared to the predictions with optimal hyper-parameters per year on all features (Figure~\ref{fig:gs_gmm} and~\ref{fig:gs_iforest}).

We see that by using at most 16 features for \gmm~and 26 features for \iforest~the \rnk~has decreased.
For example for \gmm~in 2017 the rank has decreased from 70.00 to 9.25. 
This is 13.21\% of the original rank. 
We have added these reductions for all results as the relative \rnk.
In all set-ups, we observe a significant reduction.
It means that using a subset of features, helps the detector to assign higher anomaly scores to the more anomalous instances.

\gmm~which already performed better after the grid search, also gives the best absolute and relative \rnk~values.
When inspecting the predictions, we now see that \gmm~detects 88\% and 71\% of the anomalies in 2017 and 2018.

Additionally we see that for both detectors over all years, the recall values are identical or better (i.e. at least the same amount of anomalies could be discovered as with all features).

\begin{table}
	\caption{Results of the feature selection experiment. The progress of the algorithm is shown in Figure~\ref{fig:fs_progress}.}
	\label{tbl:fs_res}
	\begin{subtable}{\textwidth}
		\centering
		\caption{\gmm}
		\label{tbl:fs_gmm_res}
		\begin{tabular}{|l|lll|}
			\hline
			& 2016 & 2017 & 2018\\ \hline
			Number of features & 12 & 12 &  16 \\
			\rnk  & 51.00 & 9.25 & 13.57 \\ \hline
			Relative \rnk~to Fig.~\ref{fig:gs_gmm} & 15.46\%  & 13.21\% & 9.98\% \\ \hline
			PR curves and predictions  & Fig.~\ref{fig:fs_gmm_2016} &Fig.~\ref{fig:fs_gmm_2017} & Fig.~\ref{fig:fs_gmm_2018}\\ \hline
		\end{tabular}
	\end{subtable}
	\par \bigskip
	\begin{subtable}{\textwidth}
		\centering
		\caption{\iforest}
		\label{tbl:fs_iforest_res}
		\begin{tabular}{|l|lll|}
			\hline
			& 2016 & 2017 & 2018\\ \hline
			Number of features & 16 & 26 & 24  \\
			\rnk  & 81.06 & 43.00 & 81.86 \\ \hline 
			Relative \rnk~to Fig.~\ref{fig:gs_iforest} & 24.70\% & 16.99\% & 23.03\% \\ \hline
			PR curves and predictions  &Fig.~\ref{fig:fs_iforest_2016} &Fig.~\ref{fig:fs_iforest_2017} & Fig.~\ref{fig:fs_iforest_2018}\\ \hline
		\end{tabular}
	\end{subtable}
\end{table}

\begin{figure}
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/fs/gmm/2016/pr_curve}{4}{8}{14}{1160}{0.33}{0.22}{0.27}{51.00}
		\caption{2016}
		\label{fig:fs_gmm_2016}
	\end{subfigure}
	\par\bigskip 
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/fs/gmm/2017/pr_curve}{7}{6}{1}{1283}{0.54}{0.88}{0.53}{9.25}
		\caption{2017}
		\label{fig:fs_gmm_2017}
	\end{subfigure}
	\par\bigskip 
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/fs/gmm/2018/pr_curve}{5}{10}{2}{1429}{0.33}{0.71}{0.59}{13.57}
		\caption{2018}
		\label{fig:fs_gmm_2018}
	\end{subfigure}
	\caption{PR curves and predictions of the best results after feature selection with \anomdetect=\gmm, \beam=1, \segscore=\texttt{max}, \thresh=99\%.}
	\label{fig:fs_gmm_res}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}{\textwidth}
		\includegraphics[width=\textwidth]{img/results/fs/gmm/progress}
		\caption{\gmm}
		\label{fig:fs_gmm_progress}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\includegraphics[width=\textwidth]{img/results/fs/iforest/progress}
		\caption{\iforest}
		\label{fig:fs_iforest_progress}
	\end{subfigure}
	\caption{The progress of feature selection for both anomaly detectors on the data of beam 1.}
	\label{fig:fs_progress}
\end{figure}

\begin{figure}
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/fs/iforest/2016/pr_curve}{7}{53}{11}{1115}{0.12}{0.39}{0.11}{81.06}
		\caption{2016}
		\label{fig:fs_iforest_2016}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/fs/iforest/2017/pr_curve}{6}{59}{2}{1230}{0.09}{0.75}{0.11}{43.00}
		\caption{2017}
		\label{fig:fs_iforest_2017}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/fs/iforest/2018/pr_curve}{3}{70}{4}{1369}{0.04}{0.43}{0.06}{81.86}
		\caption{2018}
		\label{fig:fs_iforest_2018}
	\end{subfigure}
	\caption{PR curves and predictions of the best results after feature selection with \texttt{anomaly\_detector}=\iforest, \texttt{beam}=1, \segscore=\texttt{max}, \thresh=95\%.}
	\label{fig:fs_iforest_res}
\end{figure}

\subsection{Conclusion}
For both anomaly detectors, feature selection helps to increase the quality of predictions. 
It suggests that a subset of features is sufficient. 
This is beneficial for the computational power needed to train the model.
As for the grid search, the \gmm~model is still preferred.
