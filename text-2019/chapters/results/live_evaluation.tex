\section{Live evaluation}
In a third experiment, we aim to make the circumstances for the anomaly detection application as realistic as possible.
We move from predictions on historical data to an online setting. 
Therefore this experiment is called live evaluation.
The knowledge for training a model for future predictions is gradually increased as time goes on. 

This experiment is useful since it is closer to the actual usage of the anomaly detection application in the control room at CERN. 
It is the first time that an experiment reflecting the real-world scenario has been performed.

\subsection{Set-up}
Concretely, we have discretized the live evaluation into weeks.
The experiment starts in week 1, when CERN is given a model that was trained based on historical data of week 0.
It is used to predict anomalies during this week.
At the end of week 1, CERN trains a new model incorporating the additional data.
This model is then used for prediction during the next week.
This process is iterated every week.
A graphical representation is shown in Figure~\ref{fig:live_setup}.

\begin{figure}
	\centering
	\input{chapters/results/live_evaluation_setup}
	\caption{Live evaluation: set-up. For predictions during week $t$ a model trained on historical data from week 0 until the last day of week $t-1$ is used. At the beginning of week $t+1$ the model is retrained incorporating the additional data of week $t$.}
	\label{fig:live_setup}
\end{figure}

Notice the difference with the offline setting where a model is trained on historical data and then used to make predictions on this data.
In the live evaluation setting, it is harder to train a model since it only accesses a local part of the data in the beginning.
The more time progresses, the better we assume the model will perform since it is trained on a larger set of data.

\subsubsection{Anomaly detector}
Since performing live predictions is a more challenging task, we decided to do this experiment for the best model only.
Based on the previous two experiments, we continue with \gmm.

\subsubsection{Features}
The experiment was performed for two feature sets. 
The first one contains all features, while the second one contains only the optimal feature subset (derived in Section~\ref{sec:feature_selection}).

\subsubsection{Hyper-parameters}
To capture precisely the effect of moving from an offline to an online setting, it is necessary to compare the online results with a similar offline setting. 
In previous (offline) experiments, we have obtained results with both a grid search and feature selection. 
Since these experiments used different sets of parameters, we apply the same distinction to this experiment:
\begin{itemize}
	\item For all features, we use the parameters derived in the grid search experiment (Table~\ref{tbl:gs_gmm}). This allows to directly compare the results to Figure~\ref{fig:gs_gmm}.
	\item For the optimal feature subset, we use the general parameters (Table~\ref{tbl:gmm_general}). This allows to directly compare the results to Figure~\ref{fig:fs_gmm_res}.
\end{itemize}

\subsubsection{Weeks}
In the postprocessing step segments are created based on the \texttt{STATE:MODE} data. 
Table~\ref{tbl:live_evaluation_setup} shows the date of the first and last segment for both 2017 and 2018.
Week 0 begins at the day of the first segment.
As explained above, it is used to train the first model which is used for predictions during the consecutive week.
 
\begin{table}
	\centering
	\caption{Live evaluation: set-up for 2017 and 2018.}
	\label{tbl:live_evaluation_setup}
	\begin{tabular}{l|ll|l}
		& First segment & Last segment & Number of prediction weeks\\ \hline
		2017& April 12 & December 4 & 33 \\
		2018& March 23 & December 3 & 36 \\
	\end{tabular}
\end{table}

For later interpretation of the results it is important to know that there are no anomalies during week 0 for both 2017 and 2018. 
This implies that all anomalies can still be predicted. 

Notice that the number of segments considered and predicted will be slightly different from the historical data. 
First, the segments of week 0 will not be predicted and are therefore left away. 
We did this because in week 0 there is no data yet, and thus no model.
Alternatively one could use a model based on the historical data of the previous year. 
We decided not to do this because the data of different years is quite different due to interventions during e.g. the YETS (see Section~\ref{sec:lhc_schedule}).
Second, segments accidentally overlapping between two weeks will influence the total number of segments.

\subsection{Results}
\subsubsection{All features}
The aggregated results of the live evaluation using \gmm~with all features are shown in Table~\ref{tbl:results_live_evaluation_all}.
We compare these results with the offline setting of Figure~\ref{fig:gs_gmm}.

For 2017, the online application does not succeed to detect as many anomalies as offline.
Two instead of five anomalies are detected, leading to a decrease of recall from 0.63 to 0.25.
As a sanity check, we have decreased the \thresh~for~\gmm~to 95\% to see how this would impact the result. 
As hoped, we see in Table~\ref{tbl:results_live_evaluation_all_95} that the recall increases again to 0.63 and that the precision remains the same. 
It is an arbitrary choice to change to \thresh~but possibly 99\% is too strict for a live setting.

For 2018, the live evaluation detects the same amount of anomalies.
The recall remains 0.57 but precision decreases from 0.27 to 0.11. 

\begin{table}
	\caption{Confusion matrices and metrics of the results after live evaluation on \emph{all features} with \anomdetect=\gmm, \beam=1, \segscore=\texttt{max}, \thresh=\textbf{99}\%. Hyperparameters are set per year to the best results of the grid search (see Table~\ref{tbl:gs_gmm}).}
	\label{tbl:results_live_evaluation_all}
	\begin{subtable}{.5\textwidth}
		\centering
		\caption{2017}
		\confmatrixpr{2}{31}{6}{1253}{0.06}{0.25}
	\end{subtable}%
	\begin{subtable}{.5\textwidth}
		\centering
		\caption{2018}
		\confmatrixpr{4}{32}{3}{1376}{0.11}{0.57}
	\end{subtable}
\end{table}

\begin{table}
	\caption{Confusion matrices and metrics of the results after live evaluation on \emph{all features} with \anomdetect=\gmm, \beam=1, \segscore=\texttt{max}, \thresh=\textbf{95}\%. Hyperparameters are set per year to the best results of the grid search (see Table~\ref{tbl:gs_gmm}).}
	\label{tbl:results_live_evaluation_all_95}
	\begin{subtable}{.5\textwidth}
		\centering
		\caption{2017}
		\confmatrixpr{5}{74}{3}{1210}{0.06}{0.63}
	\end{subtable}%
	\begin{subtable}{.5\textwidth}
		\centering
		\caption{2018}
		\confmatrixpr{4}{85}{3}{1323}{0.04}{0.57}
	\end{subtable}
\end{table}

\subsubsection{Subset of features}
The aggregated results of the live evaluation using \gmm~with the optimal subset of features are shown in Table~\ref{tbl:results_live_evaluation_optimal}.
We compare these results with the offline setting of Figure~\ref{fig:fs_gmm_res}.

For 2017, the online application detects only 1 instead of 7 anomalies. 
This is a decrease of the recall of 0.88 to 0.13. 
Again, we performed the experiment with a lower threshold of 95\% (see Table~\ref{tbl:results_live_evaluation_optimal_95}).
It detects 3 additional anomalies, but still fails to detect 3 anomalies which were found in the offline mode.

For 2018, the online application detects 3 anomalies, compared to the 5 anomalies in offline mode. 
However with the decreased threshold, 1 more anomaly is detected. 

\begin{table}
	\caption{Confusion matrices and metrics of the results after live evaluation on the optimal \emph{feature subset} (derived in Section~\ref{sec:feature_selection}) with \anomdetect=\gmm, \beam=1, \segscore=\texttt{max}, \thresh=\textbf{99}\%.}
	\label{tbl:results_live_evaluation_optimal}
	\begin{subtable}{.5\textwidth}
		\centering
		\caption{2017}
		\confmatrixpr{1}{32}{7}{1252}{0.03}{0.13}
	\end{subtable}%
	\begin{subtable}{.5\textwidth}
		\centering
		\caption{2018}
		\confmatrixpr{3}{33}{4}{1375}{0.08}{0.43}
	\end{subtable}
\end{table}

\begin{table}
	\caption{Confusion matrices and metrics of the results after live evaluation on the optimal \emph{feature subset} (derived in Section~\ref{sec:feature_selection}) with \anomdetect=\gmm, \beam=1, \segscore=\texttt{max}, \thresh=\textbf{95}\%.}
	\label{tbl:results_live_evaluation_optimal_95}
	\begin{subtable}{.5\textwidth}
		\centering
		\caption{2017}
		\confmatrixpr{4}{75}{4}{1209}{0.05}{0.50}
	\end{subtable}%
	\begin{subtable}{.5\textwidth}
		\centering
		\caption{2018}
		\confmatrixpr{4}{85}{3}{1323}{0.04}{0.57}
	\end{subtable}
\end{table}

\subsection{Conclusion}
We have subjected the anomaly detection application to an online prediction setting. 
As expected, results are slightly worse than for an offline setting but by decreasing the original threshold it is still able to detect most of the anomalies as detected offline. 
However, one must carefully select the threshold and cope with the resulting FPs. 

