\section{Grid search} \label{sec:grid_search}
The performance of a machine learning model is dependent on the choice of its hyper-parameters (see Section~\ref{sec:hyperparameters}).
To automate the process of making the right decision, a grid search running through several combinations was developed.
In this experiment, we derive the most performant hyper-parameter choice for each anomaly detector per year.
Next, we propose a general set of hyper-parameters for future use. 

An example call to the grid search method looks as follows:
\begin{lstlisting}[caption={Grid search}]
results = grid_search(
	feature_selection=feature_selection,
	beam=beam,
	scale_data=scale_data,
	anomaly_detector=anomaly_detector,
	detector_params=detector_params,
	labels=labels,
	segment_score=segment_score,
	x_train=df,
	state_mode=state_mode,
	scoring=scoring
)
\end{lstlisting}

We explain each of the arguments:
\begin{itemize}
	\item \texttt{feature\_selection}: the subset of columns of \texttt{x\_train} used for training and prediction
	\item \beam: the beam for which the grid search is executed
	\item \texttt{scale\_data}: the scaling method to use during preprocessing
	\item \texttt{detector\_params}: the configuration of all hyper-parameters to evaluate; formatted as a dictionary where values are of type list
	\item \texttt{labels}: the e-logbook labels to use during evaluation
	\item \segscore: the segment score method
	\item \texttt{x\_train}: the training instances
	\item \texttt{state\_mode}: the state mode data for the test instances
	\item \texttt{scoring}: the metric to use for the evaluation; either \auc~or \rnk
\end{itemize}

\subsection{Configuration}
A set of hyper-parameters (\texttt{detector\_params}) is tested while keeping the other parameters constant.
All features of the instances are used for training and prediction, so there is no feature selection involved (this will be done in Section~\ref{sec:feature_selection}).
The \auc~metric is used because it is standard in the field of anomaly detection.

\subsubsection{\gmm}
For \gmm~the following parameters are tested:
\begin{lstlisting}
n_components = [1, 2, 4, 6, 8]
covariance_type = ['full', 'tied', 'diag']
n_init = [1, 5]
init_params = ['kmeans']
scale_data = ['standard']
\end{lstlisting}
Combining these parameters results in 30 configurations.

We have limited the number of \texttt{n\_components} to limit the training time and to avoid overfitting. 
The larger the number of \texttt{n\_components}, the more specific relations in the data will be learnt but the less it will generalize.
We assume that 8 clusters are sufficient.

For \texttt{covariance\_type} we excluded \texttt{spherical} because it did not perform well. This has also been suggested in previous work \cite{HalilovicArmin2018ADft}.

The number of initialisations is limited to reduce the execution time of the grid search. An option \texttt{n\_init}=5 is added for verification.

For \texttt{init\_params}, we have excluded \texttt{random}.
Although \texttt{random} sometimes led to a good \auc, we were not able to reproduce good results with the same configuration.

For \texttt{scale\_data}, we have excluded \texttt{none} because \gmm~depends on scaling. %TODO reference
In small set-ups we observed that in fact not using any scaling yielded worse results. 

\subsubsection{\iforest}
For \iforest~the following parameters are tested:
\begin{lstlisting}
n_estimators = [50, 100, 200]
max_samples = [256, 256*10, 256*20]
contamination = [0.005, 0.01]
max_features = [1.0]
scale_data = ['standard', 'none']
\end{lstlisting}
Combining these parameters results in 36 configurations.

For \texttt{n\_estimators} the default value is 100. 
We have halved and doubled this value.

For \texttt{max\_samples} the default value is 256. 
Halilovic has suggested \cite{HalilovicArmin2018ADft} that larger values perform better. 
Although the performance of \iforest~was not satisfying, we have also included larger values. 

For \texttt{contamination} the default value is 0.01. 
However for our problem, the contamination is lower. 
On the level of segments for 2017, there were 8 anomalous segments and 1297 normal segments. 
This is a contamination of 0.6\%. 
On the level of individual instances this will be even lower, because not always all instances of a segment render it anomalous. 
Therefore we included a lower value for the contamination. 

For \texttt{max\_features} we did not allow a subset of features, since we will consider this effect later during feature selection (see Section~\ref{sec:feature_selection}).

For \texttt{scale\_data} we allow both scaling and no scaling.
This is because \iforest~in general does not depend on scaling.

\subsection{Results}

\subsubsection{\gmm}

\paragraph{Per year}
For every year, we executed a grid search.
This grid search executes the pipeline for all combinations of parameters.
Each of these executions is evaluated according to its \auc.
The best result of the grid search per year (i.e. the configuration with the best \auc) is shown in Table~\ref{tbl:gs_gmm}.
We will refer to these configurations as the best set of parameters per year.
The corresponding PR curves and predictions are shown in Figure~\ref{fig:gs_gmm}. 

For both 2017 and 2018 the model detects respectively 63\% and 57\% of the anomalies. 
The good values for \auc~and~\rnk~confirm that the model is actually able to detect the anomalies.

For 2016 however only 17\% of the anomalies are detected.
The \auc~and~\rnk~metric are also considerably worse than for 2017 and 2018.

\begin{table}
	\centering
	\caption{Optimal parameters for \gmm~per year as result of a grid search with \beam=1, \segscore=\texttt{max}.}
	\label{tbl:gs_gmm}
	\begin{tabular}{|l|lll|}
		\hline
		\textbf{Parameter}    & \textbf{2016} & \textbf{2017} & \textbf{2018} \\ \hline
		n\_components        & 4 &8 &  2 \\
		covariance\_type      & full & full & full \\
		n\_init                      & 5 & 5  & 5 \\
		init\_params             & kmeans & kmeans & kmeans \\ 
		scale\_data              & standard & standard &  standard\\ \hline
	\end{tabular}
\end{table}

\begin{figure}
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/gs/gmm/2016/pr_curve}{3}{9}{15}{1159}{0.25}{0.17}{0.21}{329.94}
		\caption{2016}
		\label{fig:gs_gmm_2016}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/gs/gmm/2017/pr_curve}{5}{8}{3}{1281}{0.38}{0.63}{0.56}{70.00}
		\caption{2017}
		\label{fig:gs_gmm_2017}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/gs/gmm/2018/pr_curve}{4}{11}{3}{1428}{0.27}{0.57}{0.48}{136.00}
		\caption{2018}
		\label{fig:gs_gmm_2018}
	\end{subfigure}
	\caption{PR curves and predictions of the best results after a grid search with \anomdetect=\gmm,  \beam=1, \segscore=\texttt{max}, \thresh=99\%.}
	\label{fig:gs_gmm}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}{.4\textwidth}
		\includegraphics[width=\linewidth]{img/results/gs/gmm/2017/n_components}  
		\par \bigskip
		\includegraphics[width=\linewidth]{img/results/gs/gmm/2017/covariance_type}  
		\par \bigskip
		\includegraphics[width=\linewidth]{img/results/gs/gmm/2017/n_init}  
		\caption{2017}
	\end{subfigure}
	\begin{subfigure}{.4\textwidth}
		\includegraphics[width=\linewidth]{img/results/gs/gmm/2018/n_components} 
		\par \bigskip 
		\includegraphics[width=\linewidth]{img/results/gs/gmm/2018/covariance_type}  
		\par \bigskip
		\includegraphics[width=\linewidth]{img/results/gs/gmm/2018/n_init}  
		\caption{2018}
	\end{subfigure}
	\caption{Grid search results for 2017 and 2018, with settings \anomdetect=\gmm, \beam=1, \segscore=\texttt{max}.}
	\label{fig:gs_gmm_boxplots}
\end{figure}

\paragraph{Generalisation}
Although we have found an optimal set of parameters per year, we require also a general set of parameters which will likely yield good results.
We suggest the configuration in Table~\ref{tbl:gmm_general} for this purpose.
We designed this configuration not to use many components. 
This should lead to learning behaviour and not to overfitting. 
It is also substantiated by a theoretical derivation of the optimal number of components by W\'ery \cite{WeryNiels2017Advd}.
Further, it will be faster for training and predicting which is useful for the next experiment. 
For the other settings, we inspected the results of the grid search.
The boxplots in Figure~\ref{fig:gs_gmm_boxplots} turned out to be useful.
Similar boxplots with \rnk~as metric (not shown) were also used.

\begin{table}
	\centering
	\caption{A general set of parameters for \gmm.}
	\label{tbl:gmm_general}
	\begin{tabular}{|l|l|}
		\hline 
		\textbf{Parameter} & \textbf{Value}\\ \hline
		n\_components & 2 \\
		covariance\_type & full \\
		n\_init & 1 \\
		init\_params & kmeans \\
		scale\_data & standard \\ \hline
	\end{tabular}
\end{table}

\newpage
\subsubsection{\iforest}

\paragraph{Per year}
The grid search with \iforest~resulted in the optimal parameters per year as shown in Table~\ref{tbl:gs_iforest}.
The corresponding confusion matrices and predictions are shown in Figure~\ref{fig:gs_iforest}.

\begin{table}
	\centering
	\caption{Optimal parameters for \iforest~per year as result of a grid search with  \beam=1, \segscore=\texttt{max}.}
	\label{tbl:gs_iforest}
	\begin{tabular}{|l|lll|}
		\hline
		\textbf{Parameter}    & \textbf{2016} &  \textbf{2017} & \textbf{2018} \\ \hline
		n\_estimators &50&100&200\\
		max\_samples&256&5120&256\\
		contamination&0.005&0.01&0.005\\
		scale\_data&standard&standard&standard\\ \hline
	\end{tabular}
\end{table}

For 2016, 2017 and 2018 respectively 28\%, 38\% and 29\% of the anomalies are detected. 
However, the corresponding precision is lower than for \gmm. 
The \auc~and~\rnk~are also worse than for \gmm.
Although \iforest~correctly detects some anomalies, it is outperformed by \gmm.

\paragraph{Generalisation}
The results obtained with \iforest~are not satisfactory.
For completeness, we show the spreading of the data for the executed grid search in Figure~\ref{fig:gs_iforest_boxplots}.
Although there are some slight differences, for the first three parameters we argue that these do not bring any value.
However the \texttt{scale\_data} plot suggests that using scaling is beneficial.
We conclude to use scaling, but it remains to be seen whether a set of hyperparameters can actually give us good performance.
We propose the general set of parameters in Table~\ref{tbl:iforest_general}.



\begin{table}
	\centering
	\caption{A general set of parameters for \iforest.}
	\label{tbl:iforest_general}
	\begin{tabular}{|l|l|}
		\hline
		\textbf{Parameter}    &  \textbf{Value} \\ \hline
		n\_estimators &100\\
		max\_samples&256\\
		contamination&0.01\\
		scale\_data&standard\\ \hline
	\end{tabular}
\end{table}

\begin{figure}
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/gs/iforest/2016/pr_curve}{5}{55}{13}{1113}{0.08}{0.28}{0.09}{328.22}
		\caption{2016}
		\label{fig:gs_iforest_2016}
	\end{subfigure}
	\par\bigskip 
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/gs/iforest/2017/pr_curve}{3}{62}{5}{1227}{0.05}{0.38}{0.03}{253.12}
		\caption{2017}
		\label{fig:gs_iforest_2017}
	\end{subfigure}
	\par\bigskip 
	\begin{subfigure}{\textwidth}
		\curvesconf{img/results/gs/iforest/2018/pr_curve}{2}{71}{5}{1368}{0.03}{0.29}{0.02}{355.43}
		\caption{2018}
		\label{fig:gs_iforest_2018}
	\end{subfigure}
	\caption{PR curves and predictions of the best results after a grid search with \texttt{anomaly\_detector}=\iforest, \texttt{beam}=1, \segscore=\texttt{max}, \thresh=95\%.}
	\label{fig:gs_iforest}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}{.4\textwidth}
		\includegraphics[width=\linewidth]{img/results/gs/iforest/2017/n_estimators}  
		\par\bigskip
		\includegraphics[width=\linewidth]{img/results/gs/iforest/2017/max_samples}  
		\par\bigskip
		\includegraphics[width=\linewidth]{img/results/gs/iforest/2017/contamination} 
		\par\bigskip
		\includegraphics[width=\linewidth]{img/results/gs/iforest/2017/scale_data} 
		\caption{2017}
	\end{subfigure}%
	\begin{subfigure}{.4\textwidth}
		\includegraphics[width=\linewidth]{img/results/gs/iforest/2018/n_estimators}  
		\par\bigskip
		\includegraphics[width=\linewidth]{img/results/gs/iforest/2018/max_samples}  
		\par\bigskip
		\includegraphics[width=\linewidth]{img/results/gs/iforest/2018/contamination}  
		\par\bigskip
		\includegraphics[width=\linewidth]{img/results/gs/iforest/2018/scale_data}  
		\caption{2018}
	\end{subfigure}
	\caption{Grid search results for 2017 and 2018, with settings \texttt{anomaly\_detector}=\iforest, \texttt{beam}=1, \segscore=\texttt{max}.}
	\label{fig:gs_iforest_boxplots}
\end{figure}

\subsection{Conclusion}
The grid search experiment has yielded an optimal set of parameters per year and a general set of parameters for both \gmm~and~\iforest.
By comparing the predictions with both detectors, we have concluded that \gmm~outperforms \iforest.
