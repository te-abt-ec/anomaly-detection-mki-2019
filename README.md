# Master's thesis: Anomaly detection for the CERN Large Hadron Collider injection magnets

Code, thesis text, study notes, presentations, poster, reports, and literature studied for an anomaly detection application for CERN's Large Hadron Collider. 

All of the code written for the application as well as Jupyter Notebooks lie under [src/](src/). The components of the code are clearly separated in different modules and directories. Executable code is grouped under [src/scripts/](src/scripts/), while Jupyter Notebooks are grouped under [src/notebooks](src/notebooks). They are named with their creation date, so that they are sorted properly. 

All notes created while working on the thesis are written in [notes-2019.md](notes-2019.md). Notes of the previous thesis can be found in [notes-2018.md](notes-2018.md). The final thesis text can be found at [text-2019/thesis.tex](text/thesis.tex). Studied literature is listed under [literature/](literature/). [Data-cern/](data-cern/) is used to keep extra information about the data, as well as files that keep built features (created by [build_features.py](src/scripts/build_features.py) in csv format.

## Getting started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. The instructions were tested on CentOS 7 and macOS 10.14.

### Prerequisites
Following packages are required in order for the setup to work:

    mongodb python3.7 python3.7-dev python3.7-gdbm python3-pip python3-tk libpng-dev libatlas-base-dev gfortran 

### Python dependencies
The Python dependencies are managed using `pipenv`. First, follow the installation instructions on <https://pipenv.readthedocs.io/en/latest/basics/>.
Then the dependencies can be installed and used in a shell.
```sh
pipenv install
pipenv shell
```

### Data

Run the below commands to start a mongoDB instance and load the data. 
```sh
./src/scripts/setup_db.sh
./src/scripts/load_data.sh localhost
```

#### General info
The MongoDB dump archives were created and compressed with the following command:
```sh
mongodump -d cern_ldb --archive=X.gz --gzip
```

To load a compressed archive file into a MongoDB instance.
```sh
mongorestore --drop -d cern_ldb --archive=file.gz --gzip
```
Only include the `--drop` parameter for the first file in case you are loading multiple files.

In the case of multiple files, a `createIndex error` might be returned by the `mongorestore` command. In that case, add the flag `--noIndexRestore`. In this case the indexes have to be regenerated before generating the features:
```sh
pipenv run src/scripts/reindex_mongodb.py
````

## Anomaly Detection Application usage examples

Note that the `src/util.py` file contains several global parameters, such as the timeframe for feature generation, data filters etc. Modifying these has an effect on the below scripts.

The anomaly detection application supports 2 modes.

### Local version
The local version requires the local database.

#### Train and predict
To use the anomaly detector, first build features and then use the grid search to obtain predictions. The results can be visually inspected the web app.
```sh
grep TRAINTIME src/util.py
### this will state the timespan used for all below commands
python src/scripts/build_features.py -b [beam]
### output files are written to data-cern/*.csv
python src/scripts/grid_search_gmm.py -b [beam] [--extended]
python src/scripts/grid_search_isolation_forest.py -b [beam] [--extended]
### result of the best run is written to src/*-best.csv 
python src/webapp/wsgi.py &; firefox http://127.0.0.1:8050/
```

#### Feature selection
Feature selection can be performed. A search algorithm is used to find a good set of features, by evaluating the pipeline in each step. Provide a beam (1 or 2), anomaly_dector ('gmm' or 'isolation_forest') and the number of iterations (integer).
```sh
python src/scripts/feature_selection.py -b [beam] -d [anomaly_detector] -i [iterations]
### output is written to feature_selection_<anomaly_detector>.log
```

#### Interactively improve predictions
Clustering using COBRAS is done via the command line or visually using a Jupyter notebook. The clustering depends on an input file called `cern_segments.csv`. This file is generated during a pipeline execution.
```sh
python src/scripts/cobras_cmd_interface
jupyter notebook src/notebooks/mongo/190325-COBRAS_notebook_segments.ipynb 
```

### Spark version
As an experiment we have ported the database client to use the Spark cluster of CERN. It has direct access to recent data of NXCALS.

### Miscellaneous
Several scripts are available in [src/scripts](src/scripts). They can simply be executed as follows:
```sh
python3 src/scripts/pipeline_iforest.py
```

## Built with
* [scikit-learn](http://scikit-learn.org) - Machine learning in Python
* [Jupyter Notebook](http://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/what_is_jupyter.html) - Human-readable, executable documents
* [MongoDB](http://mongodb.com) - NoSQL, Document-oriented database program
* [Robo 3T](https://robomongo.org/) - Robo 3T (formerly Robomongo) is the free lightweight GUI for MongoDB enthusiasts

## Authors
* **Armin Halilovic** – [arminnh](http://github.com/arminnh/)
* **Niels Wéry** – First version of python code
* **Pieter Van Trappen** – Code for accessing CERN data
* **Thiebout Dewitte**

## License
Distributed under the Apache license. See [LICENSE](LICENSE) for more information.
