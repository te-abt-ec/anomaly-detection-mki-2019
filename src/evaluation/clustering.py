import sys
import os
import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier

from util import get_html_url_for_timespan

sys.path.append(os.path.abspath(os.path.join('../cobras_ts')))

import util
from evaluation import evaluation


def cluster_segments(truth_and_pred_df, segment_score, beam, threshold, truth_and_pred_df_filename):
    pred_to_use = 'y_pred'
    if segment_score == "top_k":
        pred_to_use = "y_pred_top_k"
    if segment_score == "top_percentage":
        pred_to_use = "y_pred_top_percentage"

    # get TN, TP, FN, FP for 99% threshold
    TN, TP, FN, FP, segments = evaluation.cluster_predictions(truth_and_pred_df, threshold, pred_to_use)

    date_format = '%Y-%m-%d %H:%M:%S'
    segments.loc[:, 'url'] = segments.apply(
        lambda x: get_html_url_for_timespan(truth_and_pred_df_filename,
                                            x['timestamp_min'].strftime(date_format),
                                            x['timestamp_max'].strftime(date_format),
                                            beam),
        axis=1)

    columns = ['cluster_pred', 'nb', 'y_pred', 'freq_temp', 'min_freq_pressure', 'max_freq_pressure', 'timestamp_min',
               'timestamp_max', 'url', 'y_true']

    filename = 'segments_' + util.beam_to_str(beam) + '_' + segment_score + '.csv'
    segments.to_csv(filename, columns=columns)
    print('| Write segments clustering file to {}'.format(filename))
    return truth_and_pred_df


def recluster_with_interaction(budget, auto, filename):
    """
    Get a COBRAS clusterer object to improve the prediction of segments.
    :param budget: int, amount of questions that maybe answered by the COBRAS querier
    :param filename: the CSV file with the segments including initial predictions
    :return:
    """
    import cobras_ts.cobras_kmeans
    from evaluation.hybridnotebookquerier_ts import HybridNotebookQuerierTS
    from evaluation.autonotebookquerier_ts import AutoNotebookQuerierTS

    filename = os.path.join(util.CERN_DIR, filename)

    # Load input data
    data_cobras = np.loadtxt(filename, delimiter=',', skiprows=1, usecols=tuple(range(0, 7)))
    labels = data_cobras[:, 1]
    series = data_cobras[:, 2:]
    metadata = pd.read_csv(filename, index_col=0)

    if auto:
        querier = AutoNotebookQuerierTS(data_cobras, labels)
    else:
        querier = HybridNotebookQuerierTS(data_cobras, labels, metadata)

    clusterer = cobras_ts.cobras_kmeans.COBRAS_kmeans(series, querier, budget)
    return clusterer


def store_cobras_result(segments_filename, cobras_labeling):
    """
    Given a COBRAS clustering and the input file from which it was obtained,
    store the COBRAS clustering with the initial data.
    :param cobras_labeling:
    :param segments_filename:
    :return:
    """
    init_filename = os.path.join(util.CERN_DIR, segments_filename + '.csv')
    data_df = pd.read_csv(init_filename, index_col=0)
    data_df = data_df.assign(cluster_cobras=cobras_labeling)

    labeling_filename = segments_filename + '_labeling.csv'
    data_df.to_csv(os.path.join(util.CERN_DIR, labeling_filename))
    print('| Write COBRAS result to file {}'.format(labeling_filename))


def recluster_without_interaction(segments_to_cluster, labeling_filename):
    """
    Return a clustering of segments based on the knowledge of a previous COBRAS interactive clustering
    :param segments_to_cluster: the new segments to cluster
    :return: A list of
    """
    # Read training data
    cobras_file = os.path.join(util.CERN_DIR, labeling_filename + '.csv')
    print('| Read training data for KNN from {}'.format(cobras_file))
    train_data = pd.read_csv(cobras_file)

    # Cols on which to train
    cols = ['nb', 'y_pred', 'freq_temp', 'min_freq_pressure', 'max_freq_pressure']

    # Fit a KNN classifier on the existing segment
    neigh = KNeighborsClassifier(n_neighbors=3)
    # X is the data, y is the set of labels
    neigh.fit(train_data[cols], train_data['cluster_cobras'])

    # Get testing data
    df_predict = segments_to_cluster[cols]
    predictions = neigh.predict(df_predict)

    # Classify
    segments_to_cluster = segments_to_cluster.assign(cluster_cobras=predictions)

    # Select the most frequent occurring original cluster type as type of the cluster
    # A cluster consisting of 2 FN, 1 FP and 10 TN will be TN
    improved_pred = segments_to_cluster.groupby(['cluster_cobras'])['cluster_pred'].agg(pd.Series.mode).to_frame()
    improved_pred.columns = ['cluster_pred_improved']

    # Cluster updated will have the appropriate label for each segment, improved with COBRAS clustering
    final_clustering = segments_to_cluster.join(improved_pred, on='cluster_cobras')
    return final_clustering
