import numpy as np
import pandas as pd
import sklearn.metrics
import heapq

import plot.plot_evaluation
from postprocessing.segment import Segment


def segments_scored_to_truth_and_pred_df(segments_scored, segment_score):
    """
    Transforms a list of segments that contain ground truth (labels) and anomaly scores into a DataFrame of
    y_true (ground truth) and y_pred (anomaly scores) that can be evaluated.
    :param segments_scored: list
    :type segments_scored: list
    :param segment_score: string, the function to use for segment anomaly scores
    :return: DataFrame of y_true, y_pred, and label_id
    """
    # create DataFrame to hold (y_true, y_pred) pairs
    columns = ['y_true', 'y_pred', 'y_pred_top_k', 'y_pred_top_percentage', 'label_id', 'timestamp_min',
               'timestamp_max', 'nb', 'freq_temp', 'max_freq_pressure', 'min_freq_pressure']
    results = pd.DataFrame(index=range(len(segments_scored)), columns=columns)

    # fill in y_true and y_pred
    for i, segment in enumerate(segments_scored):
        results.iloc[i]['y_true'] = 1.0 if segment.is_labeled() else 0.0
        results.iloc[i]['y_pred'] = segment.anomaly_score(method=segment_score)
        results.iloc[i]['y_pred_top_k'] = segment.anomaly_score(method='top_k')
        results.iloc[i]['y_pred_top_percentage'] = segment.anomaly_score(method='top_percentage')
        results.iloc[i]['timestamp_min'] = segment.ts_min()
        results.iloc[i]['timestamp_max'] = segment.ts_max()
        results.iloc[i]['nb'] = len(segment)
        results.iloc[i]['freq_temp'] = segment.max_freq_temp()
        results.iloc[i]['min_freq_pressure'] = segment.min_freq_pressure()
        results.iloc[i]['max_freq_pressure'] = segment.max_freq_pressure()

        if segment.is_labeled():
            results.iloc[i]['label_id'] = segment.get_label().id

    # make each label (y_true) responsible for only 1 TP or FN by reducing the set of segments near a label to 1
    # segment with anomaly score (y_pred) equal to the worst anomaly score of the set of segments (= the largest one)
    label_ids = results[~results['label_id'].isnull()]['label_id'].unique()
    for label_id in label_ids:
        labels_mask = results['label_id'] == label_id

        y_true = 1.0
        y_pred = results[labels_mask]['y_pred'].max()
        y_pred_top_k = results[labels_mask]['y_pred_top_k'].max()
        y_pred_top_percentage = results[labels_mask]['y_pred_top_percentage'].max()
        ts_min = results[labels_mask]['timestamp_min'].min()
        ts_max = results[labels_mask]['timestamp_max'].max()
        nb = results[labels_mask]['nb'].sum()
        freq_temp = results[labels_mask]['freq_temp'].max()
        min_freq_pressure = results[labels_mask]['min_freq_pressure'].min()
        max_freq_pressure = results[labels_mask]['max_freq_pressure'].max()

        # remove entries for for this label
        results = results[~labels_mask]

        # add 1 entry to make label responsible for only 1 TP or FN
        results.loc[results.index.max() + 1] = [y_true, y_pred, y_pred_top_k, y_pred_top_percentage, label_id, ts_min,
                                                ts_max, nb, freq_temp, min_freq_pressure, max_freq_pressure]

    if results.empty:
        raise ValueError('Error: Empty truth_and_pred_df created. Probably tuples and segments do not correspond.')

    # Reset index to make sure there segment index is consistent after deleting / merging segments
    results = results.reset_index(drop=True)
    return results


def pr_curve(y_true, y_pred, fig_filename):
    """
    Calculates the PR-curve and the area under it for given y_true and y_pred.
    :param y_true: array, True targets of binary classification in range {-1, 1} or {0, 1}.
    :param y_pred: array, Anomaly scores for targets in range {-1, 1} or {0, 1}.
    :param fig_filename: string, the name of the file to store the figure to
    :return: float, The area under the PR-curve
    """
    l = list(y_true.unique())
    if len(l) < 2 and 1. not in l:  # only non-anomalies
        recall = np.nan
        precision = np.nan
        auc = np.nan
        return auc, precision, recall

    # Calculate precision and recall for different thresholds.
    precision, recall, _ = sklearn.metrics.precision_recall_curve(y_true, y_pred)

    # Approximate area under the curve. Need to sort by increasing recall to get precision points in correct order.
    sort_idx = np.argsort(recall)
    auc = np.trapz(y=precision[sort_idx], x=recall[sort_idx])

    if fig_filename is not None:
        plot.plot_evaluation.plot_precision_recall_curve(precision, recall, auc, show=False, filename=fig_filename)

    return auc, precision, recall


def make_predictions_for_threshold(truth_and_pred_df, threshold, prediction_col):
    """
    Converts predicted anomaly scores into True/False predictions for a given threshold.
    :param truth_and_pred_df: DataFrame of ground truth and predictions as given by `segments_scored_to_truth_and_pred_df`
    :param threshold: float, the threshold after which segments are predicted as anomalous.
    :param prediction_col: string, the name of the column with predictions
    :return: DataFrame
    """
    predictions_df = truth_and_pred_df.copy()
    predictions_df[prediction_col] = (predictions_df[prediction_col] >= threshold)

    y_true = predictions_df.y_true.astype('bool')
    y_pred = predictions_df[prediction_col].astype('bool')

    tn, fp, fn, tp = sklearn.metrics.confusion_matrix(y_true, y_pred).ravel()

    precision = tp / (tp + fp) if tp > 0 else 0
    recall = tp / (tp + fn)

    return predictions_df, precision, recall, tp, fp, fn, tn


def cluster_predictions(truth_and_pred_df, threshold, pred_to_use):
    threshold_score = truth_and_pred_df[pred_to_use].quantile(threshold)
    predictions_df = truth_and_pred_df.copy()
    predictions_df['y_pred_bool'] = (predictions_df[pred_to_use] >= threshold_score)

    TN = []
    TP = []
    FN = []
    FP = []

    clusters = []
    for index, pred in predictions_df.iterrows():
        new = prediction_to_segment(pred)
        if pred.y_true == pred.y_pred_bool:  # correct label assigned to segment
            if pred.y_true:
                TP.append(new)
                clusters.append(0.)
            else:
                TN.append(new)
                clusters.append(1.)
        else:  # wrong label assigned to segment
            if pred.y_pred_bool:
                FP.append(new)
                clusters.append(2.)
            else:
                FN.append(new)
                clusters.append(3.)

    predictions_df['cluster_pred'] = clusters
    return TN, TP, FN, FP, predictions_df


def prediction_to_segment(pred):
    segment = Segment(ts_min=pred.timestamp_min, ts_max=pred.timestamp_max, label=pred.label_id)
    return segment
