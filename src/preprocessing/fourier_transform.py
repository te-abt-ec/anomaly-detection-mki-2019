from heapq import nlargest
from operator import itemgetter

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from scipy import signal


def _intensity_max_var(f, t, Sxx):
    """Computes the intensity of the frequency with the largest variance."""
    variances = [(np.var(Sxx[i]), i) for i, _ in enumerate(f)]
    max_var, i = max(variances, key=itemgetter(0))
    return Sxx[i]


def _intensity_max_all(f, t, Sxx):
    """Computes the maximal intensity (at any frequency) for every point in time."""
    intensity = []
    for i, ts in enumerate(t):
        intensities_at_ts = [Sxx[k][i] for k in range(0, len(f))]
        intensity.append(max(intensities_at_ts))
    return intensity


def _intensity_median(f, t, Sxx):
    """Computes the avg intensity (over all frequencies) for every point in time."""
    intensity = []
    for i, ts in enumerate(t):
        median = np.median([Sxx[k][i] for k in range(0, len(f))])
        intensity.append(median)
    return intensity


def _frequency_max(f, t, Sxx):
    """Computes the frequency with maximal intensity for every point in time."""
    frequencies = []
    for i, ts in enumerate(t):
        intensity_freq_at_ts = [(Sxx[k][i], f[k]) for k in range(0, len(f))]
        _, freq_with_max_intensity = max(intensity_freq_at_ts, key=itemgetter(0))
        frequencies.append(freq_with_max_intensity)
    return frequencies


def _frequency_percentage_intensity5(f, t, Sxx):
    """Computes the highest frequency out of the frequencies with the 5% largest intensities."""
    return _frequency_percentage_intensity(f, t, Sxx, .05)


def _frequency_percentage_intensity10(f, t, Sxx):
    """Computes the highest frequency out of the frequencies with the 10% largest intensities."""
    return _frequency_percentage_intensity(f, t, Sxx, .10)


def _frequency_percentage_intensity20(f, t, Sxx):
    """Computes the highest frequency out of the frequencies with the 20% largest intensities."""
    return _frequency_percentage_intensity(f, t, Sxx, .20)


def _frequency_percentage_intensity(f, t, Sxx, percentage):
    frequencies = []
    for i, ts in enumerate(t):
        intensity_freq_at_ts = [(Sxx[k][i], f[k]) for k in range(0, len(f))]
        take_amount = max(1, round(percentage * len(intensity_freq_at_ts)))
        intensity_freq_at_ts_with_largest_intensities = nlargest(take_amount, intensity_freq_at_ts, key=itemgetter(0))
        _, freq = max(intensity_freq_at_ts_with_largest_intensities, key=itemgetter(1))
        frequencies.append(freq)
    return frequencies


def fft_ft(data, start, fs, col_name_suffix, func):
    df = None
    for col in data:
        f, t, Sxx = signal.spectrogram(data[col], fs)
        
        fft_data = func(f, t, Sxx)
        # Convert segments to time series that can be used with ffill
        time = np.append([0], t[:-1])
        time = pd.to_datetime(time, unit="s", origin=start)

        if df is None:
            df = pd.DataFrame(data=fft_data, index=time, columns=[col + col_name_suffix])
            df.index.name = 'timestamps'
        else:
            assert (df.index == time).all()
            df[col + col_name_suffix] = fft_data
    return df
