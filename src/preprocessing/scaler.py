import pandas as pd
import sklearn.preprocessing


def scale_array(arr, a=0, b=1):
    # scale scores from [min, max] to [a, b]: f(x) = (b-a) * (x-min)/(max-min) + a
    minn = arr.min()
    maxx = arr.max()
    return (b - a) * (arr - minn) / (maxx - minn) + a


def scale(df):
    """
    Scales the data by setting mean to 0 and variance to unit
    :param df: DataFrame as given by query
    :return: scaled df
    """
    df2 = pd.DataFrame(index=df.index, data=sklearn.preprocessing.scale(df))
    df2.columns = df.columns
    return df2


# Not used in the end
def scale_robust(df):
    """
    Scales the data in a robust manner in case of outliers
    :param df: DataFrame as given by query
    :return: scaled df
    """
    df2 = pd.DataFrame(index=df.index, data=sklearn.preprocessing.robust_scale(df))
    df2.columns = df.columns
    return df2
