def sliding_window_mean(data, base, length):
    """
    Calculates the mean of sliding windows over the data.
    Choose a window length larger than the sampling frequency!
    :param data: DataFrame as given by query
    :param base: string, 'D' for day, 'T' for minute and 'S' for seconds
    :param length: length of the window in number of times the base (ex base = 'D', length = 3 will have 3 day window)
    :return: dataframe with sliding mean. Column names are appended with ":SW_MEAN_{length}_{base}"
    """
    resampled = data.resample("1" + base).mean().rolling(window=length, min_periods=1).mean()
    resampled.rename(columns=lambda x: str(x) + ":SW_MEAN_" + str(length) + "_" + base, inplace=True)
    return resampled


def sliding_window_mean_diff(data, base, length):
    """
    Calculates the difference between the data and the mean of sliding windows over the data.
    Choose a window length larger than the sampling frequency!
    :param data: DataFrame as given by query
    :param base: string, 'D' for day, 'T' for minute and 'S' for seconds
    :param length: length of the window in number of times the base (ex base = 'D', length = 3 will have 3 day window)
    :return: dataframe with difference with sliding mean. Column names are appended with ":SW_MEAN_DIFF_{length}_{base}"
    """
    resampled = data.resample("1" + base).mean()
    resampled = resampled - resampled.rolling(window=length, min_periods=1).mean()
    resampled.rename(columns=lambda x: str(x) + ":SW_MEAN_DIFF_" + str(length) + "_" + base, inplace=True)
    return resampled


def sliding_window_sum(data, base, length):
    """
    Calculates the sum of sliding windows over the data.
    Choose a window length larger than the sampling frequency!
    :param data: DataFrame as given by query
    :param base: string, 'D' for day, 'T' for minute and 'S' for seconds
    :param length: length of the window in number of times the base (ex base = 'D', length = 3 will have 3 day window)
    :return: dataframe with sliding sum. Column names are appended with ":SW_MEAN_{length}_{length}"
    """
    resampled = data.resample("1" + base).mean().rolling(window=length, min_periods=1).sum()
    resampled.rename(columns=lambda x: str(x) + ":SW_SUM_" + str(length) + "_" + base, inplace=True)
    return resampled
