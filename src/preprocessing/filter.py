import re

import numpy as np
import pandas as pd


def filter_extremes(data):
    """
    Filter incorrect/impossible measurements out of a dataframe. Values provided by CERN.
    For continuous data, cells that exceed allowed values are set to NaN. Then, those cells are filled (ffill,
    then backfill). The same cannot be done for IPOC data, so the rows are removed as a whole.

    NOTE: If IPOC data is being filtered, it is important that I_STRENGTH and T_DELAY measurements are in the DataFrame,
    as other wrong IPOC data measurements will be filtered out by the filter for I_STRENGTH and T_DELAY. This happens
    because all IPOC data is sampled at the same timestamps within one beam, and if data is incorrect for some of the
    measurements, it will be incorrect for I_STRENGTH and T_DELAY as well.

    :param data: DataFrame as given by query
    :type data: pd.DataFrame
    :return: filtered dataframe
    :rtype: pd.DataFrame
    """
    # deep copy because changing data by indexing can change the original DataFrame
    df = data.copy(deep=True)  # type: pd.DataFrame

    for col in df:
        if re.search(r"MKI\..*:PRESSURE$", col):
            # Min 9x10^-12, max 5x10^-9 mbar
            ffilter = ~((df[col] >= 9 * 10 ** -12) & (df[col] <= 5 * 10 ** -9))
            df.loc[ffilter, col] = np.NaN

        elif re.search(r"MKI\..*:TEMP_TUBE_(UP|DOWN)$", col):
            # Min 18 °C, max 120 °C
            ffilter = ~((df[col] >= 18) & (df[col] <= 120))
            df.loc[ffilter, col] = np.NaN

        elif re.search(r"MKI\..*:TEMP_MAGNET_(UP|DOWN)$", col):
            # Min 18 °C, max 60 °C
            ffilter = ~((df[col] >= 18) & (df[col] <= 60))
            df.loc[ffilter, col] = np.NaN

        elif re.search(r"MKI\..*:I_STRENGTH$", col):
            # I_STRENGTH > 1 kA
            df = df[df[col] > 1]

        elif re.search(r"MKI\..*:T_DELAY$", col):
            # T_DELAY > 0.0 us
            df = df[df[col] > 0.0]

        elif re.search(r"MKI\..*:CONTROL$", col):
            # STATE:CONTROL <> 6 (i.e. LOCAL)
            df = df[df[col] != '6']

    return df.fillna(method='ffill').fillna(method='backfill')
