from matplotlib.pyplot import *

style.use("ggplot")

# rcParams["lines.linewidth"] = 3
# rcParams["figure.figsize"] = [6.4, 4.8]
# rcParams["figure.figsize"] = [8, 6]
rcParams["figure.figsize"] = [15, 8]  # 1080p, 15"
# rcParams["figure.figsize"] = [15, 11.25]
# rcParams["figure.figsize"] = [16, 12]
# rcParams["figure.figsize"] = [20, 11] # 1440, 27"
rcParams["axes.titlesize"] = 18
rcParams["axes.labelsize"] = 16
rcParams["xtick.labelsize"] = 14
rcParams["ytick.labelsize"] = 14
rcParams["legend.fontsize"] = "x-large"
rcParams["legend.framealpha"] = 1
rcParams["legend.labelspacing"] = 0.1
# rcParams["figure.titlesize"] = "x-large"


def axhlines(ys, **plot_kwargs):
    """
    Draw horizontal lines across plot
    Source: https://stackoverflow.com/questions/24988448/how-to-draw-vertical-lines-on-a-given-plot-in-matplotlib
    :param ys: A scalar, list, or 1D array of vertical offsets
    :param plot_kwargs: Keyword arguments to be passed to plot
    :return: The plot object corresponding to the lines.
    """
    ys = np.array((ys,) if np.isscalar(ys) else ys, copy=False)
    lims = gca().get_xlim()
    y_points = np.repeat(ys[:, None], repeats=3, axis=1).flatten()
    x_points = np.repeat(np.array(lims + (np.nan,))[None, :], repeats=len(ys), axis=0).flatten()
    return plot(x_points, y_points, scalex=False, **plot_kwargs)


def axvlines(xs, **plot_kwargs):
    """
    Draw vertical lines on plot
    Source: https://stackoverflow.com/questions/24988448/how-to-draw-vertical-lines-on-a-given-plot-in-matplotlib
    :param xs: A scalar, list, or 1D array of horizontal offsets
    :param plot_kwargs: Keyword arguments to be passed to plot
    :return: The plot object corresponding to the lines.
    """
    xs = np.array((xs,) if np.isscalar(xs) else xs, copy=False)
    lims = gca().get_ylim()
    x_points = np.repeat(xs[:, None], repeats=3, axis=1).flatten()
    y_points = np.repeat(np.array(lims + (np.nan,))[None, :], repeats=len(xs), axis=0).flatten()
    return plot(x_points, y_points, scaley=False, **plot_kwargs)

