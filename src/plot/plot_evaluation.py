import os
import numpy as np

import util
import evaluation.evaluation
import plot.pyplot as plt


def plot_precision_recall_curve(precision, recall, auc=None, show=True, filename=None):
    fig = plt.figure()
    plt.plot(precision, label="precision")
    plt.plot(recall, label="recall")

    plt.xlabel("threshold index")
    plt.ylim([0.0, 1.05])
    plt.xlim([0, len(precision)])
    plt.tight_layout(rect=(0, 0, 1, 0.98))
    plt.title('Precision and recall')
    plt.legend(loc="upper center")
    if filename is not None:
        base, fname = os.path.split(filename)
        plt.savefig(os.path.join(base, "pr-" + fname + util.FILE_EXTENSION))
    if show:
        plt.show()
    plt.close(fig)

    fig = plt.figure()
    plt.plot(recall, precision)
    # plt.fill_between(recall, precision, step='post', alpha=0.2, color='b')

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.00])
    plt.tight_layout(rect=(0, 0, 1, 0.98))
    if auc is not None:
        plt.title('2-class Precision-Recall curve, area = {0:0.3f}'.format(auc))
    else:
        plt.title('2-class Precision-Recall curve')
    if filename is not None:
        base, fname = os.path.split(filename)
        plt.savefig(os.path.join(base, "pr-curve-" + fname + util.FILE_EXTENSION))
    if show:
        plt.show()
    plt.close(fig)


def plot_pr_curves_named(pr_dict, show=True, filename=None):
    fig = plt.figure()

    for name in pr_dict:
        recall = pr_dict[name]["recall"]
        precision = pr_dict[name]["precision"]
        label = "{} (area = {:.3f})".format(name, pr_dict[name]["auc"])

        plt.plot(recall, precision, label=label)

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.00])
    plt.tight_layout(rect=(0, 0, 1, 0.98))
    plt.title('2-class Precision-Recall curves')
    plt.legend(loc="upper right")

    if filename is not None:
        base, fname = os.path.split(filename)
        plt.savefig(os.path.join(base, "pr-curve-" + fname + util.FILE_EXTENSION))
    if show:
        plt.show()

    plt.close(fig)


def labels_vs_predictions_for_different_thresholds(truth_and_pred_df, features, feature_regexes, show=True,
                                                   filename=None):
    """
    Takes a DataFrame of ground truth and anomaly score predictions and plots the anomalies that were predicted for
    different thresholds. The labels are also plotted so that the predictions can be checked.
    :param truth_and_pred_df: DataFrame of ground truth and predictions as given by `segments_scored_to_truth_and_pred_df`
    :param features: DataFrame of features
    :param feature_regexes: list of measurement regexes to plot the labels and anomalies over
    :param show: bool, whether or not to show the figure
    :param filename: string, name of the file the figure will be saved to
    """
    threshold_percentiles = [0.99, 0.98, 0.95]  # [0.99, 0.95, 0.85, 0.75]
    thresholds = truth_and_pred_df.y_pred.quantile(threshold_percentiles)

    labels = truth_and_pred_df[truth_and_pred_df.y_true > 0]
    ts_labels = labels.timestamp_max.tolist()

    for percentile, threshold in zip(threshold_percentiles, thresholds):
        fig, axes = plt.subplots(nrows=len(feature_regexes) + 1, ncols=1, sharex="col")
        ax0 = axes[0] if len(feature_regexes) > 0 else axes

        # make predictions for the current threshold
        predictions_df, p, r, _, _, _, _ = evaluation.evaluation.make_predictions_for_threshold(truth_and_pred_df,
                                                                                                threshold,
                                                                                                'y_pred')
        predictions = predictions_df[predictions_df.y_pred == True]
        ts_predictions = predictions.timestamp_max.tolist()

        # in the first subplot, just show dots for labels and dots for predictions
        # ax0.set_xlim(features.index.min(), features.index.max())
        ax0.scatter(x=ts_labels, y=labels.y_true, c="red", label="Anomalies", s=200, zorder=2, edgecolors="black")
        ax0.scatter(x=ts_predictions, y=predictions.y_pred, c="blue", label="Predictions", s=25, zorder=3)
        legend = ax0.legend(loc="upper right", bbox_to_anchor=(1.1, 1))
        legend.legendHandles[0]._sizes = [35]
        legend.legendHandles[1]._sizes = [35]

        # for each given column regex, plot the data of the columns and plot the labels and predictions over the data.
        if len(feature_regexes):
            for axis, regex in zip(axes[1:], feature_regexes):
                df = features.filter(regex=regex).sort_index(axis=1)

                axis.scatter(x=ts_labels, y=df.mean(axis=1)[ts_labels], c="red", label="Anomalies", s=200, zorder=2,
                             edgecolors="black")
                axis.scatter(x=ts_predictions, y=df.mean(axis=1)[ts_predictions], c="blue", label="Predictions", s=25,
                             zorder=3)

                for col in df:
                    axis.scatter(x=df.index, y=df[col], s=2)

                axis.set_xlim(df.index.min(), df.index.max())
                yrange_offset = 0.05 * (df.max().max() - df.min().min())
                axis.set_ylim(df.min().min() - yrange_offset, df.max().max() + yrange_offset)

                axis.set_title(regex)
                legend = axis.legend(loc="upper right", bbox_to_anchor=(1.15, 1))
                for i in range(len(df.columns)):
                    legend.legendHandles[i]._sizes = [35]

        plt.xlabel("Time")
        plt.suptitle(
            "Labels vs. predictions for threshold at {}-th percentile (= {:.2E}, precision = {:.2f}, recall = {:.2f})"
            .format(int(percentile*100), threshold, p, r), fontsize=15, fontweight="bold"
        )
        plt.tight_layout(rect=(0, 0, 1, 0.98))

        if filename is not None:
            plt.savefig(filename + "-threshold_" + str(percentile) + util.FILE_EXTENSION)
        if show:
            plt.show()

        plt.close(fig)
