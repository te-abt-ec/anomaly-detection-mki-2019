#!/usr/bin/env python3

import argparse
import os
import re
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from database.CERNMongoClient import CERNMongoClient


def test_ipoc_timestamps(db, beam):
    """ Check index equality between all IPOC collections for a certain beam. """

    # Get all IPOC collections
    collections = [c for c in db.get_all_collections() if re.search(".*IPOC\.[ABCD]{beam}.*".format(beam=beam), c)]
    print("{} collections will be scanned for beam {}".format(len(collections),beam))

    # Fetch every collection and store its DatetimeIndex (Immutable ndarray of datetime64 data)
    idx = []
    idx_len = []
    for c in collections:
        dti = db.query(c).index
        idx.append(dti)
        idx_len.append(len(dti.values))
    print("\nDebug output, last list DatetimeIndex object for beam {}:\n{}\n".format(beam, idx[-1]))

    # Check of IPOC missing data, for all collections should have the same idx length
    dtilenmax = max(idx_len)
    for i in range(0, len(idx_len)-1):
        try:
            assert (dtilenmax==idx_len[i])
        except AssertionError as e:
            print("** Missing IPOC data for collection {} with length {}, expected is {} **".format(collections[i],idx_len[i],dtilenmax))

    # Check that every index is the same as every other one
    for i in range(0, len(idx) - 1):
        try:
            assert (idx[i].equals(idx[i + 1]))
        except AssertionError as e:
            print('\n{}\t{}\t{}'.format(idx[i].equals(idx[i + 1]), collections[i], collections[i + 1]))
            print(idx[i+1].difference(idx[i]))
            print(idx[i].difference(idx[i+1]))

if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', dest='host', default='localhost', help='Host of MongoDB (default: localhost)')
    args = parser.parse_args()

    db = CERNMongoClient(host=args.host, logging=False)

    # Check IPOC timestamps for beam 1 and 2
    test_ipoc_timestamps(db, "B1")
    test_ipoc_timestamps(db, "B2")
