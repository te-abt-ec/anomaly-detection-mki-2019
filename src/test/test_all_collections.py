#!/usr/bin/env python3

import argparse
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from database.CERNMongoClient import CERNMongoClient

if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', dest='host', default='localhost', help='Host of MongoDB (default: localhost)')
    args = parser.parse_args()

    db = CERNMongoClient(host=args.host)

    all_collections = sorted(db.get_all_collections())
    print("Found: " + str(len(all_collections)) + " collections:")
    print("\t" + "\n\t".join(all_collections))
