import util


class Label:
    def __init__(self, id, date, tag, beam):
        self.id = id
        self.date = date
        self.tag = tag
        self.beam = beam

    def __str__(self):
        return "Label {} at {}: '{}' in beam {}".format(self.id, self.date, self.tag, self.beam)

    def __eq__(self, other):
        if not all(hasattr(other, attr) for attr in ["id", "date", "tag", "beam"]):
            return False

        return self.id == other.id and self.date == other.date and self.tag == other.tag and self.beam == other.beam

    @staticmethod
    def create_from_series_row(series_row):
        if series_row["TAG"] not in util.CERN_LABELS:
            raise Exception("Unexpected label '{}'".format(series_row["TAG"]))

        return Label(series_row["EVENT_ID"], series_row["EVENTDATE"], series_row["TAG"], series_row["BEAM"])
