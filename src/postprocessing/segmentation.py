import copy

import numpy as np
import pandas as pd

import util
from postprocessing.label import Label
from postprocessing.segment import Segment


def df_to_segments(df, states):
    """
    Takes a DataFrame and splits it into a list of Segments. Splitting is based on the state of the MKI magnets.
    A segment represents a time interval in which the MKI magnets change from on to standby.
    :param df: DataFrame
    :return list of Segment
    """
    segments = _create_empty_segments(states)
    segments = assign_tuples_to_segments(df, segments)
    segments = prune_empty_segments(segments)
    return segments


def _create_empty_segments(states):
    """
    Takes a DataFrame of states and returns a List of segments. 
    A segment is created for each "on" state, followed by the first "standby", "off" or "faulty" state.
    Each segment will contain a minimum and maximum timestamp, but no tuples are associated with it.
    :param df: DataFrame
    :type df: pd.DataFrame
    :return list of Segment
    """
    segments = []
    started_segment = False
    for index, row in states.itertuples():
        if not started_segment:
            if row == 1:  # on
                start = index
                started_segment = True
        else:
            if row == 3 or row == 2 or row == 4:  # standby or off or faulty
                segments.append(Segment(ts_min=start, ts_max=index))
                started_segment = False
    return segments


def assign_tuples_to_segments(tuples, segments):
    """
    Takes a DataFrame of IPOC tuples (sorted order) and a List of empty segments.
    IPOC tuples will be assigned to a segment, which corresponds to their timestamp.
    """
    for s in segments:
        tuples_for_seg = tuples.loc[s.ts_min(): s.ts_max()]
        s.add_tuples(tuples_for_seg)
    return segments


def prune_empty_segments(segments):
    return [s for s in segments if len(s) != 0]


def set_ground_truth(segments, labels):
    """
    Annotates segments with the ground truth using labels. If a segment lies near one of the given labels, it's
    is_anomalous flag is set to true.
    :param segments: list of Segments
    :param labels: pd.DataFrame of labels
    :return: list of annotated Segments
    """
    new_segments = copy.deepcopy(segments)

    for s in new_segments:
        ts_near_label = util.first((ts for ts in labels.index if s.is_near(ts, "start")), None)

        if ts_near_label:
            s.set_label(Label.create_from_series_row(labels.loc[ts_near_label]))
        else:
            s.set_label(None)

    return new_segments


def print_segment_stats(segments):
    lengths = [len(s) for s in segments]
    anomalous_segments = [s for s in segments if s.is_labeled()]
    time_lengths = [s.length_time() for s in segments]

    print("IPOC SEGMENT STATS:")
    print("Number of segments:     {}".format(len(segments)))
    print("Anomalous segments:     {}".format(len(anomalous_segments)))
    print("Min segment length:     {}, {} points".format(min(time_lengths), min(lengths)))
    print("Mean segment length:    {}, {} points".format(np.mean(time_lengths).round('s'), np.mean(lengths, dtype=int)))
    print("Max segment length:     {}, {} points".format(max(time_lengths), max(lengths)))
    print()