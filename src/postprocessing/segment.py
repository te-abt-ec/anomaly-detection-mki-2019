import pandas as pd
import math

import util


class Segment:
    """
    A Segment represents a period of time that machines were used for. It consists of the measurements that fall within
    such a period. It could span from a few seconds to a couple of hours.
    Segments are said to be anomalous if they lie near an anomaly label. They can be assigned the label they lie near.
    """

    def __init__(self, ts_min, ts_max, label=None):
        self._df = pd.DataFrame({})
        self._ts_max = ts_max
        self._ts_min = ts_min
        self._label = label

    def __str__(self):
        if self._df.empty:
            return "Segment from {} to {}".format(self.ts_min(), self.ts_max())
        return "Segment from {} to {}\n" \
               "\tLength: {}. Points: {}.\n" \
               "\tAnomalous: {}. Anomaly score: {}" \
            .format(self.ts_min(), self.ts_max(),
                    self.length_time(), len(self),
                    self.is_labeled(), round(self.anomaly_score(), 2))

    def __len__(self):
        return len(self._df.index)

    def __eq__(self, other):
        return self.get_df().equals(other.get_df()) and self.get_label() == other.get_label()

    def get_df(self):
        return self._df

    def add_tuples(self, df):
        """
        Adds an entry to the Segment
        :param df: row of pd.DataFrame
        """
        # Assert that all tuples which are added can belong to the segment
        for r in df.iterrows():
            assert self.can_have(r[0]), 'Cannot append tuple {} to segment {}-{}'.format(r[0], self.ts_min(),
                                                                                         self.ts_max())
        self._df = self.get_df().append(df)
        return self

    def max_freq_temp(self):
        df = self.get_df()

        freq_col = [f for f in list(df.columns) if 'freq_percentage_intensity20' in f and 'TEMP' in f]
        if not freq_col:
            #print('Warning: no maximal frequency of temperature. Returning 0.')
            freq = 0
        else:
            freq = (df[freq_col[0]]).max()  # maximal frequency in a fft feature of temperature
        return freq

    def max_freq_pressure(self):
        df = self.get_df()

        freq_col = [f for f in list(df.columns) if 'freq_percentage_intensity20' in f and 'PRESSURE' in f]
        if not freq_col:
            #print('Warning: No maximal frequency of pressure. Returning 0.')
            freq = 0
        else:
            freq = (df[freq_col[0]]).max()  # maximal frequency in a fft feature of temperature

        return freq

    def min_freq_pressure(self):
        df = self.get_df()
        freq_col = [f for f in list(df.columns) if 'freq_percentage_intensity20' in f and 'PRESSURE' in f]
        if not freq_col:
            # print('Warning: No maximal frequency of pressure. Returning 0.')
            freq = 0
        else:
            freq = (df[freq_col[0]]).min()  # maximal frequency in a fft feature of temperature
        return freq

    def is_labeled(self):
        return self._label is not None

    def get_label(self):
        return self._label

    def set_label(self, label):
        assert self._label is None, 'Trying to set label which was already set'
        self._label = label
        return self

    def ts_min(self):
        """
        :return: pd.Timestamp, the minimum timestamp in the index
        """
        return self._ts_min

    def ts_max(self):
        """
        :return: pd.Timestamp, the maximum timestamp in the index
        """
        return self._ts_max

    def ts_mean(self):
        """
        :return: pd.Timestamp, the mean timestamp in the index
        """
        # DatetimeIndex has no mean method in the pandas version used at the time of writing
        start = self.ts_min()
        t_deltas = [ts - start for ts in self._df.index]
        mean_delta = sum(t_deltas, pd.Timedelta(0, unit="h")) / len(t_deltas)
        return start + mean_delta

    def length_time(self):
        """
        :return: length of the Segment: max - min timestamp
        """
        return self.ts_max() - self.ts_min()

    def anomaly_score(self, method="top_percentage"):
        """
        :return: double, the anomaly score of the segment
        """
        if self._df.empty:
            # print("No score column in segment with start {} and end {}".format(self.ts_min(), self.ts_max()))
            return 0

        if method == "top_percentage":
            # returns the average of the 25% worst anomaly scores
            take_amount = max(1, round(0.25 * len(self._df.index)))
            return self._df.score.nlargest(take_amount).mean()

        elif method == "top_k":
            # returns the average of the 10 worst anomaly scores
            return self._df.score.nlargest(10).mean()

        elif method == "max":
            return self._df.score.max()

        else:
            raise Exception("Unsupported Segment anomaly_score method '{}'.".format(method))

    def top_scores(self):
        """
        Get the 10 largest anomaly scores of the segment
        """
        return self._df.score.nlargest(10).values

    def ground_truth(self):
        """
        :return: int, 0 if not anomaly, 1 if anomalous
        """
        if self.get_label() is not None and not math.isnan(self.get_label()):
            return True
        return False

    def distance_from_ts(self, timestamp, method):
        """
        Calculates the distance from the Segment to a timestamp using a certain method.
        :param timestamp: Timestamp
        :param method: "edge" (min distance to max or min timestamp in index),
                       "start" (distance to earliest timestamp in index),
                       "end" (distance to latest timestamp in index),
                       "mean" (distance to mean timestamp in index)
        :return: distance (Timedelta)
        """
        if method == "start":
            return timestamp - self.ts_min()
        elif method == "end":
            return timestamp - self.ts_max()
        elif method == "edge":
            return min(timestamp - self.ts_min(), timestamp - self.ts_max())
        elif method == "mean":
            return timestamp - self.ts_mean()
        else:
            raise ValueError("Method should be 'start', 'end', 'edge 'or 'mean'")

    def can_have(self, timestamp):
        return self.ts_min() <= timestamp <= self.ts_max()

    def is_near(self, timestamp, method="start"):
        """
        Returns true if the Segment occurs near a certain timestamp. The definition of near depends on the distance
        method to be used.
        """
        distance = self.distance_from_ts(timestamp, method)
        max_distance = pd.Timedelta(util.MAX_LOGGING_DELAY, unit="h")
        return pd.Timedelta(0, unit="h") <= abs(distance) <= max_distance
