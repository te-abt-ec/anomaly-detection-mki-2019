#!/usr/bin/env python3

import json
import os
import re
import sys
import traceback
from datetime import datetime as dt
from urllib.parse import urlparse, parse_qsl
from base64 import urlsafe_b64decode

import numpy as np
import pandas as pd

# Dash and Plotly
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

# Application specific imports
from database.CERNMongoClient import CERNMongoClient
from evaluation import evaluation
from preprocessing import builder
import util
import webapp.components as drc

# Some global constants
app = dash.Dash(__name__)
application = app.server
predictions = False
truth_and_pred_filename = ''
AR0 = "xaxis.range[0]"
AR1 = "xaxis.range[1]"
app.scripts.config.serve_locally = True  # needed for
app.config.suppress_callback_exceptions = True  # needed for url states
app.title = "CERN MKI Anomaly Detection Dash app"
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-layout')
])

try:
    # Check if there is a MongoDB container
    if os.environ.get('OPENSHIFT_MONGODB_DB_HOST') is not None:
        print('MONGODB DB HOST {}'.format(os.environ.get("OPENSHIFT_MONGODB_DB_HOST")))
        user_name = 'admin'
        user_pwd = os.environ.get('OPENSHIFT_ADMIN_PWD')
        host = os.environ.get("OPENSHIFT_MONGODB_DB_HOST")
        port = os.environ.get("OPENSHIFT_MONGODB_DB_PORT")
        uri = "mongodb://" + user_name + ":" + user_pwd + "@" + host + ":" + port
        db = CERNMongoClient(host=uri, hostAsUri=True)
    elif len(sys.argv) > 1:
        db = CERNMongoClient(host=sys.argv[1])
    else:
        db = CERNMongoClient()
    available_indicators = db.get_all_collections()
except Exception as e:
    print("Error occurred. Unable to create MongoClient")
    print(e)


def build_layout(params):
    layout = [
        html.Div(className='banner', children=[
            html.Div(className='container scalable', children=[
                html.H2('CERN Data Explorer'),
                html.Img(src="assets/cern_white.png")
            ]),

        ]),

        html.Div(id='body', className='container scalable', children=[
            html.Div(
                className='three columns',
                style={
                    'min-width': '10%',
                    'max-height': 'calc(100vh - 85px)',
                    'overflow-y': 'auto',
                    'overflow-x': 'hidden',
                },
                children=[
                    drc.Card([
                        apply_default_value(params)(drc.NamedDropdown)(
                            name='Show beam',
                            id='beam-dropdown',
                            options=[{'label': 'Beam {} ({})'.format(i, util.beam_to_num(i)), 'value': i} for i in
                                     util.BEAMS],
                            value=1,
                            multi=False,
                            clearable=False
                        ),
                        apply_default_value(params)(drc.NamedDropdown)(
                            name='Show datasets (view 1)',
                            id='dataset1-dropdown',
                            value=[],
                            multi=True
                        ),

                        apply_default_value(params)(drc.NamedDropdown)(
                            name='Show datasets (view 2)',
                            id='dataset2-dropdown',
                            value=[],
                            multi=True
                        ),

                        apply_default_value(params)(drc.NamedDropdown)(
                            name='Show labels',
                            id='label-checkboxes',
                            options=[{'label': i, 'value': i} for i in sorted(util.CERN_LABELS)],
                            value=[],
                            multi=True
                        ),

                        apply_default_value(params)(drc.NamedDatePicker)(
                            name='Show date range',
                            id='date-picker',
                            min_date_allowed=dt.strptime(util.DATETIME_EARLIEST, '%Y-%m-%d %H:%M:%S'),
                            max_date_allowed=dt.strptime(util.DATETIME_LATEST, '%Y-%m-%d %H:%M:%S'),
                            start_date=dt.strptime(util.TRAINTIME_EARLIEST, '%Y-%m-%d %H:%M:%S'),
                            end_date=dt.strptime(util.TRAINTIME_LATEST, '%Y-%m-%d %H:%M:%S'),
                            initial_visible_month=dt.strptime(util.TRAINTIME_EARLIEST, '%Y-%m-%d %H:%M:%S'),
                        )
                    ]),
                    drc.Card([
                        apply_default_value(params)(drc.NamedDropdown)(
                            name='Select CSV file with prediction',
                            id='predictions',
                            options=[{'label': os.path.basename(i), 'value': i} for i in util.list_predictions()],
                            value=util.list_predictions()[0] if len(util.list_predictions()) else '',
                            clearable=False
                        ),

                        apply_default_value(params)(drc.NamedDropdown)(
                            name='Set score method of segments',
                            id='segment-score-method',
                            options=[
                                {'label': 'maximum', 'value': 'max'},
                                {'label': 'top k', 'value': 'top_k'},
                                {'label': 'top %', 'value': 'top_percentage'}
                            ],
                            value='max',
                            clearable=False,
                            searchable=False,
                        ),

                        apply_default_value(params)(drc.NamedSlider)(
                            name='Set threshold',
                            id='threshold-slider',
                            min=0.95,
                            max=0.99,
                            step=0.005,
                            marks={i: {'label': '{}%'.format(int(i * 100))} for i in np.linspace(.95, .99, 5)},
                            value=0.98
                        )
                    ])
                ]
            ),

            html.Div(className='three columns',
                     style={
                         'min-width': '55%',
                         'overflow-y': 'auto',
                         'overflow-x': 'hidden'
                     },
                     children=[
                         html.Div(id='segments-div', children=[dcc.Graph(id='segments-graph')]),
                         html.Div(id='state_mode-div', children=[dcc.Graph(id='statemode-graph')]),
                         html.Div(id='data1-div', children=[dcc.Graph(id='data1-graph')]),
                         html.Div(id='data2-div', children=[dcc.Graph(id='data2-graph')])
                     ],
                     ),

            html.Div(className='three columns',
                     style={
                         'min-width': '20%',
                         'overflow-y': 'auto',
                         'overflow-x': 'hidden'
                     },
                     children=[
                         drc.Card([
                             html.Div(id='evaluation-card'),
                             html.Div(id='pr-curve-div', children=[dcc.Graph(id='pr-curve-graph')])
                         ]),
                         drc.Card([html.Div(id='fp-table')]),
                         drc.Card([html.Div(id='fn-table')])
                     ],
                     ),
            html.Div(id='hidden-div', style={'display': 'none'}),
            html.Div(id='hidden-div-statemode', style={'display': 'none'}),
            html.Div(id='hidden-div-segments', style={'display': 'none'}),
        ]),
    ]
    return layout


def current_beam(filename):
    if 'B1' in filename:
        return 1
    if 'B2' in filename:
        return 2
    return 1  # default beam for web explorer


@app.callback(Output('beam-dropdown', 'value'),
              [Input('predictions', 'value')])
def set_beam_after_predictionfile_change(filename):
    return current_beam(filename)


@app.callback(Output('dataset1-dropdown', 'options'),
              [Input('beam-dropdown', 'value')])
def set_dataset_options(selected_beam):
    return [{'label': i, 'value': i} for i in available_indicators if
            (util.beam_to_num(selected_beam) in i) or ('B' + str(selected_beam) in i)]


@app.callback(Output('dataset2-dropdown', 'options'),
              [Input('beam-dropdown', 'value')])
def set_dataset_options(selected_beam):
    return [{'label': i, 'value': i} for i in available_indicators if
            (util.beam_to_num(selected_beam) in i) or ('B' + str(selected_beam) in i)]


def update_prediction_file(prediction_file):
    global truth_and_pred_filename
    global truth_and_pred_df
    global predictions

    # only update on change
    if prediction_file == truth_and_pred_filename:
        return

    try:
        truth_and_pred_filename, truth_and_pred_df = util.load_df_from_csv_with_name(prediction_file)
        predictions = True
    except Exception as e:
        print("Error occurred when getting prediction.\n {}".format(e))
        truth_and_pred_filename, truth_and_pred_df = '', ''
        predictions = False


@app.callback(Output('evaluation-card', 'children'),
              [Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value')])
def update_threshold(prediction_file, segment_score, threshold):
    update_prediction_file(prediction_file)

    if not predictions:
        return 'No predicting performed.'

    pred_to_use = get_pred_to_use(segment_score)
    threshold_score = truth_and_pred_df[pred_to_use].quantile(threshold)
    _, precision, recall, tp, fp, fn, tn = evaluation.make_predictions_for_threshold(truth_and_pred_df,
                                                                                     threshold_score,
                                                                                     pred_to_use)

    return html.Div([
        html.P('Threshold score: {}'.format(threshold_score)),
        html.P('TP:  {:4} | FP:  {:4}  || sum: {:4}'.format(tp, fp, tp + fp)),
        html.P("FN:  {:4} | TN:  {:4}  || sum: {:4}".format(fn, tn, fn + tn)),
        html.P('PRECISION: {:.2f}, RECALL: {:.2f}'.format(precision, recall))
    ])


@app.callback(Output('fp-table', 'children'),
              [Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value')])
def update_fp_table(prediction_file, segment_score, threshold):
    update_prediction_file(prediction_file)
    if not predictions:
        return 'No predicting performed.'

    pred_to_use = 'y_pred'
    if segment_score == "top_k":
        pred_to_use = "y_pred_top_k"
    if segment_score == "top_percentage":
        pred_to_use = "y_pred_top_percentage"

    _, _, _, FP, _ = evaluation.cluster_predictions(truth_and_pred_df, threshold, pred_to_use)
    x_FP, y_FP, label_FP = segments_to_plot(FP)
    fp_df = pd.DataFrame(columns=['start', 'end'])
    fp_df = segment_list_to_pandas_df(x_FP, fp_df)
    table = drc.NamedDataTable(
        name='False Positives',
        columns=[{"name": i, "id": i} for i in fp_df.columns],
        data=fp_df.to_dict("rows"),
        style_table={
            'overflowX': 'scroll',
            'overflowY': 'scroll',
            'maxHeight': '500'
        }
    )
    return table


@app.callback(Output('fn-table', 'children'),
              [Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value')])
def update_fn_table(prediction_file, segment_score, threshold):
    update_prediction_file(prediction_file)
    if not predictions:
        return 'No predicting performed.'

    pred_to_use = get_pred_to_use(segment_score)

    _, _, FN, _, _ = evaluation.cluster_predictions(truth_and_pred_df, threshold, pred_to_use)
    x_FN, y_FN, label_FN = segments_to_plot(FN)
    fn_df = pd.DataFrame(columns=['start', 'end'])
    fn_df = segment_list_to_pandas_df(x_FN, fn_df)
    table = drc.NamedDataTable(
        name='False Negatives',
        columns=[{"name": i, "id": i} for i in fn_df.columns],
        data=fn_df.to_dict("rows"),
        style_table={
            'overflowX': 'scroll',
            'overflowY': 'scroll',
            'maxHeight': '500'
        }
    )
    return table


def segment_list_to_pandas_df(segments, df):
    i = 1
    segment_list = []
    for el in segments:
        if i == 3:
            df.loc[len(df)] = segment_list
            segment_list = []
            i = 1
        else:
            segment_list.append(el)
            i = i + 1
    df['start'] = pd.to_datetime(df['start']).dt.strftime("%Y-%m-%d %H:%M")
    df['end'] = pd.to_datetime(df['end']).dt.strftime("%Y-%m-%d %H:%M")
    return df.reset_index()


def get_pred_to_use(segment_score):
    pred_to_use = 'y_pred'
    if segment_score == "top_k":
        pred_to_use = "y_pred_top_k"
    if segment_score == "top_percentage":
        pred_to_use = "y_pred_top_percentage"
    return pred_to_use


def segments_to_plot(segments):
    x = []
    y = []
    label = []
    for segment in segments:
        x.extend([segment.ts_min(), segment.ts_max(), None])
        y.extend([0, 1, None])
        info = 'label: {}<br> y_true: {:d}'.format(segment.get_label(), segment.ground_truth())
        label.extend([info, info, None])
    return x, y, label


@app.callback(Output('hidden-div-segments', 'children'),
              [Input('predictions', 'value'),
               Input('segment-score-method', 'value'),
               Input('threshold-slider', 'value')])
def segments_calculation(prediction_file, segment_score, threshold):
    update_prediction_file(prediction_file)
    if not predictions:
        return 'No segments, since no -best.csv file was found.'

    pred_to_use = get_pred_to_use(segment_score)

    TN, TP, FN, FP, _ = evaluation.cluster_predictions(truth_and_pred_df, threshold, pred_to_use)
    x_TN, y_TN, label_TN = segments_to_plot(TN)
    x_TP, y_TP, label_TP = segments_to_plot(TP)
    x_FN, y_FN, label_FN = segments_to_plot(FN)
    x_FP, y_FP, label_FP = segments_to_plot(FP)

    datadict = {'x_TN': x_TN, 'y_TN': y_TN, 'label_TN': label_TN,
                'x_TP': x_TP, 'y_TP': y_TP, 'label_TP': label_TP,
                'x_FN': x_FN, 'y_FN': y_FN, 'label_FN': label_FN,
                'x_FP': x_FP, 'y_FP': y_FP, 'label_FP': label_FP}
    return json.dumps(datadict)


@app.callback(Output('segments-graph', 'figure'),
              [Input('hidden-div-segments', 'children'),
               Input('statemode-graph', 'relayoutData'),
               Input('data1-graph', 'relayoutData'),
               Input('data2-graph', 'relayoutData'),
               Input('date-picker', 'start_date'),
               Input('date-picker', 'end_date')])
def update_segments_graph(jdata, rld1, rld2, rld3, startd, endd):
    try:
        if jdata is not None:
            data = json.loads(jdata)
    except:
        print("Error in fetching graph data, which returned '{}'".format(jdata))
        return 1

    # get zoomlevel from other graphs
    zrange = get_zoom_level(rld1, rld2, rld3, startd, endd)

    graphfig = {
        'data': [go.Scattergl(x=data['x_TN'], y=data['y_TN'], text=data['label_TN'], mode='lines+markers', name='TN'),
                 go.Scattergl(x=data['x_TP'], y=data['y_TP'], text=data['label_TP'], mode='lines+markers', name='TP',
                              line=dict(color='rgb(0, 0, 0)')),
                 go.Scattergl(x=data['x_FP'], y=data['y_FP'], text=data['label_FP'], mode='lines+markers', name='FP',
                              line=dict(color='rgb(205, 12, 24)', dash='dot')),
                 go.Scattergl(x=data['x_FN'], y=data['y_FN'], text=data['label_FN'], mode='lines+markers', name='FN',
                              line=dict(color='rgb(255, 165, 0)', dash='dot'))],
        'layout': {
            'title': 'Segments (based on STATE:MODE)',
            'height': 150,
            'legend': dict(orientation="h", y=1.5),
            'margin': {'b': 30, 'r': 60, 'l': 60, 't': 60},
            'xaxis': zrange,
            'yaxis': dict(
                autorange=True,
                showgrid=False,
                zeroline=False,
                showline=False,
                showticklabels=False)
        }
    }
    return graphfig


@app.callback(Output('hidden-div', 'children'),
              [Input('segments-graph', 'relayoutData'),
               Input('statemode-graph', 'relayoutData')])
def print_debug(rld, rld2):
    # below line added to disable debug output
    return 0
    print("printing segments relayoutData:")
    print(json.dumps(rld, indent=2))
    print("printing statemode relayoutData:")
    print(json.dumps(rld2, indent=2))
    return 0


# below callback to reduce the execution-time for the lower statemode-graph figure callback
@app.callback(Output('hidden-div-statemode', 'children'),
              [Input('date-picker', 'start_date'),
               Input('date-picker', 'end_date'),
               Input('beam-dropdown', 'value')])
def state_mode_query(startd, endd, beam):
    ua_num = "UA23" if beam == 1 else "UA87"
    data = db.query('MKI.' + ua_num + '.STATE:MODE', filters=True, start=startd, end=endd)
    return data.to_json(date_format='iso', orient='split')  # json.dumps fails for pd


# callback in case data or zoom-level (i.e. relayoutData) changes
@app.callback(Output('statemode-graph', 'figure'),
              [Input('hidden-div-statemode', 'children'),
               Input('segments-graph', 'relayoutData'),
               Input('data1-graph', 'relayoutData'),
               Input('data2-graph', 'relayoutData'), ],
              [State('date-picker', 'start_date'),
               State('date-picker', 'end_date'),
               State('beam-dropdown', 'value')])
def update_state_mode(jdata, rld1, rld2, rld3, startd, endd, beam):
    # get the query and misc data
    ua_num = "UA23" if beam == 1 else "UA87"
    try:
        if jdata is not None:
            data = pd.read_json(jdata, orient='split')
    except:
        print("Error in fetching graph data, which returned '{}'".format(jdata))
        return 1

    # get zoomlevel from other graphs
    zrange = get_zoom_level(rld1, rld2, rld3, startd, endd)

    graphfig = {
        'data': [go.Scattergl(
            x=data.index.tolist(),
            y=data['MKI.' + ua_num + '.STATE:MODE'],
            mode='markers')],
        'layout': {
            'title': 'MKI.' + ua_num + '.STATE:MODE',
            'height': 250,
            'showlegend': False,
            'margin': {'b': 30, 'r': 60, 'l': 60, 't': 60},
            'xaxis': zrange,
            'yaxis': dict(
                autorange=True,
                showgrid=False,
                zeroline=False,
                showline=False,
                ticktext=['unknown', 'on', 'off', 'standby', 'faulty', 'warming_up'],
                tickvals=[0, 1, 2, 3, 4, 5]
            )
        }
    }
    return graphfig


def to_labels(df):
    labels = pd.Series([])
    for i, (comment, event_id) in enumerate(zip(df['SUBSTR_COMMENT_512_'], df['EVENT_ID'])):
        labels[i] = 'ID: ' + str(event_id) + '<br>' + comment.replace("\n", "<br />")
    return labels


def insert_br(s):
    return "<br>".join(re.findall("(?s).{,64}", s))[:-1]


def label_scatter(data, labels, name, ticker, color):
    if not data.empty:
        y = [data[ticker].min() for _ in labels.index]
    else:
        y = [0 for _ in labels.index]
    return go.Scattergl(x=labels.index,
                        y=pd.Series(y),
                        mode='markers',
                        marker=dict(size=10, color=color),
                        text=to_labels(labels),
                        name=name)


@app.callback(Output('pr-curve-graph', 'figure'),
              [Input('predictions', 'value'),
               Input('segment-score-method', 'value')])
def update_pr_curve(prediction_file, segment_score):
    update_prediction_file(prediction_file)

    pred_to_use = get_pred_to_use(segment_score)

    if not predictions:
        return ""

    # Obtain data for PR curve
    predictions_df = truth_and_pred_df.copy()
    y_true = predictions_df.y_true.astype('float')
    y_pred = predictions_df[pred_to_use].astype('float')
    auc, precision, recall = evaluation.pr_curve(y_true=y_true, y_pred=y_pred, fig_filename=None)

    graphfig = {
        'data': [
            {'x': recall, 'y': precision, 'name': 'PR curve'},
        ],
        'layout': {
            'title': 'PR curve (AUC={:.2f})'.format(auc),
            'xaxis': {
                'title': 'Recall'
            },
            'yaxis': {
                'title': 'Precision'
            },
            'margin': {
                'l': 40,
                'r': 20
            }
        }
    }
    return graphfig


@app.callback(Output('data1-graph', 'figure'),
              [Input('dataset1-dropdown', 'value'),
               Input('label-checkboxes', 'value'),
               Input('date-picker', 'start_date'),
               Input('date-picker', 'end_date'),
               Input('beam-dropdown', 'value'),
               Input('statemode-graph', 'relayoutData'),
               Input('segments-graph', 'relayoutData'),
               Input('data2-graph', 'relayoutData')
               ])
def update_data1_graphs(selected_datasets, labels, startd, endd, beam, rld1, rld2, rld3):
    return update_data_graphs(selected_datasets, labels, startd, endd, beam, rld1, rld2, rld3, 1)


@app.callback(Output('data2-graph', 'figure'),
              [Input('dataset2-dropdown', 'value'),
               Input('label-checkboxes', 'value'),
               Input('date-picker', 'start_date'),
               Input('date-picker', 'end_date'),
               Input('beam-dropdown', 'value'),
               Input('statemode-graph', 'relayoutData'),
               Input('segments-graph', 'relayoutData'),
               Input('data1-graph', 'relayoutData')
               ])
def update_data2_graphs(selected_datasets, labels, startd, endd, beam, rld1, rld2, rld3):
    return update_data_graphs(selected_datasets, labels, startd, endd, beam, rld1, rld2, rld3, 2)


def update_data_graphs(selected_datasets, labels, startd, endd, beam, rld1, rld2, rld3, nb):
    data_graphs = []
    data = pd.DataFrame
    ticker = ''
    try:
        for i, ticker in enumerate(selected_datasets):
            data = db.query(ticker, filters=True, start=startd, end=endd)
            data_fig = go.Scattergl(
                x=data.index.tolist(),
                y=data[ticker],
                mode='markers',
                marker=dict(size=4),
                name=ticker)
            data_graphs.append(data_fig)
    except Exception as exception:
        print("error occured during update_graph, ignoring for now..\n", exception)
        traceback.print_exc()

    try:
        logbook = builder.load_logbook_from_file(beam, filename='logbook_B' + str(beam) + '.csv')
        anomaly_df = builder.anomalies(logbook)
        intervention_df = builder.intervention(logbook)
        fault_df = builder.fault(logbook)
        info_df = builder.info(logbook)
        research_df = builder.research(logbook)

        if 'anomaly' in labels:
            data_graphs.append(label_scatter(data, anomaly_df, 'anomaly', ticker, 'rgba(220,20,60,1.0)'))
        if 'intervention' in labels:
            data_graphs.append(label_scatter(data, intervention_df, 'intervention', ticker, 'rgba(255,255,0,0.5)'))
        if 'fault' in labels:
            data_graphs.append(label_scatter(data, fault_df, 'fault', ticker, 'rgba(255,165,0,0.5)'))
        if 'info' in labels:
            data_graphs.append(label_scatter(data, info_df, 'info', ticker, 'rgba(160,160,160,0.5)'))
        if 'research' in labels:
            data_graphs.append(label_scatter(data, research_df, 'research', ticker, 'rgba(102,0,102,0.5)'))
    except Exception as exception:
        print('error occured when fetching or plotting labels, ignoring for now..\n', exception)
        traceback.print_exc()

    # get zoomlevel from other graphs
    zrange = get_zoom_level(rld1, rld2, rld3, startd, endd)

    graphfig = {
        'data': data_graphs,
        'layout': {
            'title': 'Data ' + str(nb),
            'height': 250,
            'legend': dict(orientation='h', y=1.5),
            'showlegend': True,
            'margin': {'b': 30, 'r': 60, 'l': 60, 't': 60},
            'xaxis': zrange,
            'yaxis': dict(
                autorange=True,
            )
        }
    }
    return graphfig


def get_zoom_level(rld1, rld2, rld3, startd, endd):
    relayoutData = None
    for rld in [rld1, rld2, rld3]:
        if rld is not None and AR0 in rld.keys():
            relayoutData = rld
            break

    if relayoutData is not None:  # zoom was changed
        zrange = [relayoutData[AR0], relayoutData[AR1]]
        print("Info: new zoomlevel for graph\n{}".format(json.dumps(zrange, indent=2)))
    else:  # no zoom change
        zrange = [startd, endd]

    return dict(range=zrange)


# Methods for parsing the url state
# maybe this will be included in Dash later: https://github.com/plotly/dash/issues/188
def apply_default_value(params):
    def wrapper(func):
        def apply_value(*args, **kwargs):
            if 'id' in kwargs and kwargs['id'] in params:
                the_value_keys = params[kwargs['id']][0]
                the_values = params[kwargs['id']][1]
                if type(the_values) is list:
                    if len(the_value_keys) == len(the_values):
                        for ix in range(len(the_value_keys)):
                            kwargs[the_value_keys[ix]] = the_values[ix]
                    else:
                        print('We could not properly map keys to values! Please fill in *all* values in component_ids.')
                # kwargs['value'] = params[kwargs['id']][1]
            return func(*args, **kwargs)

        return apply_value

    return wrapper


def parse_state(url):
    parse_result = urlparse(url)
    query_string = parse_qsl(parse_result.query)
    if query_string:
        encoded_state = query_string[0][1]
        state = dict(json.loads(urlsafe_b64decode(encoded_state)))
    else:
        state = dict()
    return state


@app.callback(Output('page-layout', 'children'),
              inputs=[Input('url', 'href')])
def page_load(href):
    if not href:
        return []
    state = parse_state(href)
    return build_layout(state)


component_ids = {
    'beam-dropdown': ['value'],
    'dataset1-dropdown': ['value'],
    'dataset2-dropdown': ['value'],
    'label-checkboxes': ['value'],
    'date-picker': ['start_date', 'end_date'],
    'predictions': ['value'],
    'segment-score-method': ['value'],
    'threshold-slider': ['value']
}


@app.callback(Output('url', 'search'),
              inputs=[Input(id, param[i]) for id, param in component_ids.items() for i in range(len(param))])
def update_url_state(*values):
    l = []
    idx = 0
    for k in component_ids.values():
        amount_to_take = len(k)
        l.append(values[idx:idx + amount_to_take])
        idx = idx + amount_to_take
    state = dict(zip(list(component_ids.keys()), list(zip(component_ids.values(), l))))
    print('Info: current state is {}'.format(state))
    return util.generate_params(state)


if __name__ == '__main__':
    app.run_server(debug=True)
