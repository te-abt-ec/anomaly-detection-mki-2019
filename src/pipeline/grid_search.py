import multiprocessing
import pandas as pd
import time

import sklearn.model_selection
from pipeline.pipeline import pipeline
from util import beam_to_str


def pipeline_with_selection(params):
    beam = params[0]
    feature_set_name, feature_set = params[1]
    scale_data = params[2]
    anomaly_detector = params[3]
    detector_parameters = params[4]
    labels = params[5]
    segment_score = params[6]
    x_train = params[7]
    state_mode = params[8]
    x_test = params[9]

    x_train_selected = x_train[feature_set].copy()
    x_test_selected = x_test[feature_set].copy() if x_test is not None else None

    try:
        pipeline_res = pipeline(beam=beam,
                                scale_data=scale_data,
                                anomaly_detector=anomaly_detector,
                                detector_params=detector_parameters,
                                labels=labels,
                                segment_score=segment_score,
                                x_train=x_train_selected,
                                state_mode=state_mode,
                                x_test=x_test_selected,
                                write_output=False)
    except ValueError:
        pipeline_res = {'auc': -1., 'rank': -1, 'scale': scale_data,
                        'segment_score': segment_score, 'params': detector_parameters,
                        'truth_and_pred_df': pd.DataFrame(), 'time': -1}

    pipeline_res['feature'] = feature_set_name
    return pipeline_res


def grid_search(feature_selection, beam, scale_data, anomaly_detector, detector_params, labels, segment_score, x_train,
                state_mode, x_test=None, scoring='auc', filename=''):
    """
    Performs grid search for a given anomaly detector and the given parameters.
    :param feature_selection: DataFrame of features
    :param beam: 1 or 2, beam for which training and testing data is provided
    :param state_mode: DataFrame with STATE:MODE to create segments
    :param feature_selection: List of list of features to be used in grid search
    :param labels: DataFrame of labels
    :param anomaly_detector: string, AnomalyDetector to use
    :param detector_params: dictionary of parameters for the chosen AnomalyDetector
    :param scale_data: bool, whether or not to scale the data before fitting and scoring
    :param segment_score: string or list of strings, the function to use for segment anomaly scores
    :param filename: Suffix of the filename of the best result
    :param scoring: Parameter to evaluate the grid search result
    """
    # Check that parameters have type list so they can be used in a grid
    for param in (feature_selection, scale_data, segment_score):
        assert (isinstance(param, list))

    # Create grids
    detector_grid = sklearn.model_selection.ParameterGrid(detector_params)
    evaluation_grid = sklearn.model_selection.ParameterGrid({
        'segment_score': segment_score
    })

    # Create configurations
    configs = []
    for scale_method in scale_data:
        for fs in feature_selection:
            for detector_params in detector_grid:
                for evaluation_params in evaluation_grid:
                    configs.append([beam, fs, scale_method, anomaly_detector, detector_params, labels,
                                    evaluation_params['segment_score'], x_train, state_mode, x_test])

    pool = multiprocessing.Pool()
    start_time = time.time()
    results = pool.map(pipeline_with_selection, configs)
    pool.close()
    pool.join()

    total_time = time.time() - start_time
    print('\n| Grid search execution time: {:.1f} seconds\n'.format(total_time))

    results_sorted = sorted(results, key=lambda res: res[scoring], reverse=True)
    for r in results_sorted:
        print(
            '| AUC = {:6.3f}, rank = {:.2f} in for feature={}, scale={:1}, a_score_method={}, params={}, time={:.1f}s '.format(
                r['auc'], r['rank'], r['feature'], r['scale'], r['segment_score'], r['params'], r['time']))

    # Write the best performing truth_and_pred_df to a csv file so it can be reused
    if len(results_sorted):
        best = results_sorted[0]
        best_results_filename = 'grid_search_' + beam_to_str(beam) + '_' + anomaly_detector + filename + '-best.csv'
        best['truth_and_pred_df'].to_csv(best_results_filename)
        print('| Wrote best results to file {}'.format(best_results_filename))

        # store statistics
        exec_times = [res['time'] for res in results]
        scores = [res[scoring] for res in results]
        statistics = pd.DataFrame(data={'scores': scores, 'time': exec_times})
        statistics.to_csv('grid_search_statistics_' + beam_to_str(beam) + '_' + anomaly_detector + filename + '.csv')

    return results
