import numpy as np
import time

import preprocessing.scaler
from anomaly_detection.factory import AnomalyDetectorFactory
from evaluation import clustering, evaluation
from postprocessing.segmentation import df_to_segments, set_ground_truth
from util import beam_to_str


def pipeline(beam, scale_data, anomaly_detector, detector_params, labels, segment_score, x_train, state_mode,
             x_test=None, write_output=True):
    """
    Executes the anomaly detection pipeline for a set of features and labels and other parameters that can be set.
    :param beam: 1 or 2, beam for which training and testing data is provided
    :param scale_data: 'standard' or False (no scaling)
    :param anomaly_detector: string, AnomalyDetector to use
    :param detector_params: dictionary of parameters for the chosen AnomalyDetector
    :param labels: DataFrame of labels
    :param segment_score: string or list of strings, the function to use for segment anomaly scores
    :param x_train: DataFrame of features
    :param state_mode: DataFrame with STATE:MODE to create segments
    :param x_test: DataFrame of features to be predicted
    """
    # CHECK ARGUMENTS
    assert not x_train.empty, 'x_train should be non-empty'
    if x_test is not None:
        assert not x_test.empty, 'x_test should be non-empty'
        assert set(x_train.columns) == set(x_test.columns), 'x_train and x_test should have same features.'

    # TIMING
    start = time.time()

    # PREPROCESSING: scale train and test data
    x_train = scale(x_train, scale_data)
    x_test = scale(x_test, scale_data) if x_test is not None else x_train

    # ANOMALY DETECTION: create detector, train on training data, predict on test data
    detector = AnomalyDetectorFactory.create(anomaly_detector, **detector_params)
    train(detector=detector, features=x_train)
    features_scored = predict(detector, x_test)

    # POSTPROCESSING: transform tuples into segments
    truth_and_pred_df = postprocess(
        features_scored=features_scored,
        state_mode=state_mode,
        labels=labels,
        segment_score_method=segment_score
    )

    # TOTAL TIME
    total_time = time.time() - start

    # EVALUATION: Area Under PR-Curve
    auc, precision, recall = evaluate_pr(truth_and_pred_df)

    # EVALUATION: Ranking
    rank = evaluate_rank(truth_and_pred_df)

    # FEEDBACK: improve results using COBRAS
    filename = 'pipeline_' + beam_to_str(beam) + '_' + anomaly_detector + '.csv'
    if write_output:
        if anomaly_detector == 'gmm':
            threshold = .99
        elif anomaly_detector == 'iforest':
            threshold = .95
        else:
            raise Exception()
        clustering.cluster_segments(truth_and_pred_df, segment_score, beam, threshold, filename)

    result = {
        'truth_and_pred_df': truth_and_pred_df,
        'auc': auc,
        'precision': precision,
        'recall': recall,
        'rank': rank,
        'anomaly_detector': anomaly_detector,
        'params': detector_params,
        'scale': scale_data,
        'segment_score': segment_score,
        'time': total_time
    }

    if write_output:
        result['truth_and_pred_df'].to_csv(filename)
        print('| Wrote best results to file {}'.format(filename))
    return result


def scale(features, method):
    """
    Scale the given features using the given method.
    :param features: DataFrame of features to scale
    :param method: 'standard' or False (no scaling)
    :return: DataFrame of features transformed according to the given method
    """
    if method == 'standard':
        features = preprocessing.scaler.scale(features)
    elif method == 'none':
        print('No scaling performed')
    else:
        raise Exception("Unsupported scale method.")

    return features


def train(detector, features):
    """
    Fits an AnomalyDetector on the given features
    :param detector: AnomalyDetector, the detector
    :param features: DataFrame of features
    """
    start_time = time.time()
    detector.fit(features)
    print("|| Trained detector: {:.1f} seconds".format(time.time() - start_time))


def predict(detector, features):
    """
    Takes a trained anomaly detector and assigns scores to the given features
    :param detector: AnomalyDetector, the detector
    :param features: DataFrame of features
    :return:
    """
    # TODO assert that detector has been trained before
    start_time = time.time()
    scores = detector.anomaly_scores(features)
    features = features.assign(score=scores)
    print("|| Scored features: {:.1f} seconds".format(time.time() - start_time))
    return features


def postprocess(features_scored, state_mode, labels, segment_score_method):
    """
    Takes a DataFrame of scored features, transforms them into Segments, runs the evaluation procedure using
    the anomaly scores of the Segments, and returns the evaluation results.
    :param features_scored: DataFrame of features
    :param state_mode: DataFrame with STATE:MODE to create segments
    :param labels: DataFrame of labels
    :param segment_score_method: string or list of strings, the function to use for segment anomaly scores
    """
    # transform scored tuples into scored segments
    segments = df_to_segments(features_scored, state_mode)

    # add ground truth to segments
    segments = set_ground_truth(segments, labels)

    # create DataFrame that holds ground truth (0 or 1 for each segment) and anomaly score predictions
    truth_and_pred_df = evaluation.segments_scored_to_truth_and_pred_df(segments, segment_score_method)

    # sanity check: check that the amount of rows in truth_and_pred_df is correct
    check_truth_and_pred_df_length(segments, truth_and_pred_df)
    return truth_and_pred_df


def evaluate_pr(truth_and_pred_df):
    # compute area under the precision recall curve
    auc, precision, recall = evaluation.pr_curve(
        y_true=truth_and_pred_df["y_true"].astype(float),
        y_pred=truth_and_pred_df["y_pred"].astype(float),
        fig_filename=None
    )
    print("|| Area under PR curve = {}".format(auc))
    return auc, precision, recall


def evaluate_rank(df):
    """
    Computes the rank of a truth_and_pred_df. Lower rank means better prediction. Perfect ranking is 1.
    The DataFrame is sorted on decreasing anomaly score, then the rank of anomalies is summed
    and divided by the length of the dataframe.
    """
    df_sorted = df.sort_values(by=['y_pred'], ascending=False)[['y_true', 'y_pred']]
    ranking = df_sorted.reset_index(drop=True)
    is_anomaly = ranking['y_true'] == 1
    anomaly_indices = ranking[is_anomaly].index.tolist()
    if not len(anomaly_indices):  # no anomalies, so makes no sense to compute ranking
        print("|| Rank = {}".format(np.NaN))
        return np.NaN
    rank = sum(anomaly_indices) / len(anomaly_indices)
    print("|| Rank = {}".format(rank))
    return rank


def check_truth_and_pred_df_length(segments, truth_and_pred_df):
    """
    Checks that the amount of rows in truth_and_pred_df is equal to
    amount_of_segments - amount_of_labels_that_occur_too_much

    e.g. when there are 3 segments for 1 label, 2 of them occur too much
    => those 3 segments should be represented by the one with the worst anomaly score

    For more details about this approach, see notes.md: 23/04, IPOC segments, ground truth annotation, and evaluation

    :param segments: list of segments
    :param truth_and_pred_df: DataFrame of y_true values and y_pred anomaly scores
    """
    segments_labeled = [s for s in segments if s.get_label() is not None]
    unique_labels = set([s.get_label().id for s in segments_labeled])
    segments_to_remove = len(segments_labeled) - len(unique_labels)

    if len(truth_and_pred_df) != len(segments) - segments_to_remove:
        raise Exception(
            "Wrong amount of labels ({}) in truth_and_pred_df: segments = {}, segments_labeled = {}, unique_labels = {}"
                .format(len(truth_and_pred_df), len(segments), len(segments_labeled), len(unique_labels))
        )
