from sklearn import mixture

import preprocessing.scaler
from anomaly_detection.anomaly_detector import AnomalyDetector


class GMM(AnomalyDetector):
    def __init__(self, n_components=1, covariance_type="full", tol=1e-3, reg_covar=1e-6, max_iter=100, n_init=1,
                 init_params='kmeans', weights_init=None, means_init=None, precisions_init=None, random_state=None,
                 warm_start=False, verbose=0, verbose_interval=10):
        self.gmm = mixture.GaussianMixture(
            n_components=n_components, covariance_type=covariance_type, tol=tol,
            reg_covar=reg_covar, max_iter=max_iter, n_init=n_init, init_params=init_params,
            weights_init=weights_init, means_init=means_init, precisions_init=precisions_init,
            random_state=random_state, warm_start=warm_start, verbose=verbose, verbose_interval=verbose_interval
        )

    def __str__(self):
        return str(self.gmm)

    def set_params(self, **params):
        self.gmm.set_params(**params)
        return self

    def fit(self, df):
        return self.gmm.fit(df)

    def anomaly_scores(self, df):
        scores = self.gmm.score_samples(df)
        # flip scores to make larger scores more anomalous
        scores = -scores
        return preprocessing.scaler.scale_array(scores)

    def predict(self, df):
        return self.gmm.predict(df)
