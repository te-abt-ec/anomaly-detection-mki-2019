from anomaly_detection.dummy_detector import DummyDetector
from anomaly_detection.gmm import GMM
from anomaly_detection.isolationforest import IsolationForest
from anomaly_detection.one_class_svm import OneClassSVM
from anomaly_detection.lof import LOF


class AnomalyDetectorFactory:
    """ Methods that can generate AnomalyDetector implementations. """

    @staticmethod
    def create_constant_detector(constant_label):
        return DummyDetector(strategy="constant", constant=constant_label)

    @staticmethod
    def create_uniformly_random_detector():
        return DummyDetector(strategy="uniform")

    @staticmethod
    def create_stratified_random_detector(contamination):
        return DummyDetector(strategy="stratified", contamination=contamination)

    @staticmethod
    def create(detector, **kwargs):
        """
        :param detector: The name of the anomaly detector method
        :type detector: str
        :return: An instance of the requested anomaly detector
        """
        detector = detector.lower()

        if detector == "gmm":
            return GMM(**kwargs)

        elif detector == "iforest":
            return IsolationForest(**kwargs)

        elif detector == "one_class_svm":
            return OneClassSVM(**kwargs)

        elif detector == "lof":
            return LOF(**kwargs)

        elif detector == "dummy_constant":
            return AnomalyDetectorFactory.create_constant_detector(**kwargs)

        elif detector == "dummy_uniform":
            return AnomalyDetectorFactory.create_uniformly_random_detector()

        elif detector == "dummy_stratified":
            return AnomalyDetectorFactory.create_stratified_random_detector(**kwargs)

        else:
            raise Exception("Unknown anomaly detector")
