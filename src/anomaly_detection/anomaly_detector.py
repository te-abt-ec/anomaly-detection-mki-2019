from abc import ABC, abstractmethod


class AnomalyDetector(ABC):
    """ Base anomaly detector class """

    @abstractmethod
    def set_params(self, **params):
        """ Sets the parameters of the detector. """
        raise NotImplementedError

    @abstractmethod
    def fit(self, df):
        """ Trains the detector on the given DataFrame. """
        raise NotImplementedError

    @abstractmethod
    def anomaly_scores(self, df):
        """
        Given a DataFrame of features, returns anomaly scores for all samples.
        Anomaly scores lie in [0, 1] and larger scores are more anomalous.
        """
        raise NotImplementedError

    @abstractmethod
    def predict(self, df):
        """ Given a DataFrame of features, returns class predictions all samples. """
        raise NotImplementedError
