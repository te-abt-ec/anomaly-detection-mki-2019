import pandas as pd
import re

from pyspark.sql import functions
from pyspark.sql.types import StructType, StructField, IntegerType
from cern.nxcals.pyquery.builders import *

import util
from database.CERNClient import CERNClient
from preprocessing import filter


class CERNSparkClient(CERNClient):
    """
    Client for fetching CERN data using Spark
    """

    def __init__(self, spark, sc, logging=True):
        """
        :param spark: SparkSession
        :param sc: SparkContext
        """
        self.spark = spark
        self.sc = sc
        self.logging = logging

    def get_all_collections(self):
        file = open('../../scripts/output/all_collections.txt', 'r')
        collections = file.read().splitlines()
        file.close()
        return collections

    def query(self, pattern, start=util.DATETIME_EARLIEST, end=util.DATETIME_LATEST, filters=False, resample_to=None,
              fillna=True):
        # make sure that start and end dates are strings
        start = str(start) + ".000"
        end = str(end) + ".000"

        if self.logging:
            print("Fetching data for pattern {0} from {1} to {2}".format(pattern, start, end))

        collections = [col for col in self.get_all_collections() if re.search(pattern, col)]

        schema = StructType([StructField("new_time", IntegerType(), False)])
        df_c = self.spark.createDataFrame([], schema)
        for collection in collections:
            df = VariableQuery.builder(self.spark).system("CMW").startTime(start).endTime(end).variable(
                collection).buildDataset()
            df = df.na.drop()
            df = df.withColumn(collection, df['nxcals_value'])
            df = df.withColumn('new_time', df['nxcals_timestamp'])
            df = df.drop('nxcals_entity_id', 'nxcals_value', 'nxcals_timestamp', 'nxcals_variable_name')
            df_c = df_c.join(df, 'new_time', how='full')

        df_c = df_c.sort(functions.col("new_time").asc())
        df_c = df_c.select('*', (df_c.new_time / 1000000000).alias('nxcals_timestamp_div'))
        df_c = df_c.withColumn('timestamp',
                               functions.from_unixtime('nxcals_timestamp_div', format='yyyy-MM-dd HH:mm:ss'))
        df_c = df_c.drop('new_time', 'nxcals_timestamp_div')

        df_c = df_c.toPandas().set_index('timestamp')
        df_c.index = df_c.index.astype('datetime64[ns]')

        # Apply filters on whole DataFrame instead of separate columns, because some IPOC measurements are filtered
        # by filters for other IPOC measurements since IPOC measurement timestamps are equal within one beam.
        if filters:
            df_c = filter.filter_extremes(df_c)
        # Always resample after the data has been filtered. Otherwise, incorrect values will be added to the results.
        # Never resample when IPOC data is mixed with continuous data. IPOC data should not be resampled.
        if resample_to is not None:
            df_c = df_c.resample(resample_to).mean()

        if self.logging:
            print()

        if fillna:
            # Hold values in case of NaN, first forwards then backwards in case first value is NaN
            df_c = df_c.fillna(method='ffill').fillna(method='backfill')

        return df_c

    def query_elogbook(self, start=util.DATETIME_EARLIEST, end=util.DATETIME_LATEST, beam=None):
        logbook = self._convert_elogbook_json("../../../data-cern/logbook_2015_06_22-2017_09_24.json")
        logbook = logbook.sort_index()

        # Filter out with start and end
        logbook = logbook.ix[start:end]

        # Add beam number
        logbook["BEAM"] = [1 if installation == "MKI2" else 2 for installation in logbook["VALUE"]]

        if beam == 1:
            return logbook[logbook["VALUE"] == "MKI2"]
        elif beam == 2:
            return logbook[logbook["VALUE"] == "MKI8"]
        else:
            return logbook

    @staticmethod
    def _convert_elogbook_json(file):
        # Fetch all entries from JSON file
        logbook_json = pd.read_json(file, lines='true')
        logs = []
        for _, row in logbook_json.iterrows():
            timestamps = row['timestamps']
            val = row['values']
            for ts, v in zip(timestamps, val):
                l = []
                for key, value in sorted(v.items()):
                    if key != "C0":  # do not store mongoDB index
                        l.append(value)
                l.append(ts)
                logs.append(l)
        logbook = pd.DataFrame(logs, columns=['EVENTDATE', 'EVENT_ID', 'PATH', 'SUBSTR_COMMENT_512_',
                                              'TAG', 'USERNAME', 'VALUE', 'TIMESTAMPS'])
        logbook.TIMESTAMPS = pd.to_datetime(logbook.TIMESTAMPS.astype(int), unit='s')
        logbook = logbook.set_index('TIMESTAMPS')
        logbook = logbook.sort_index()

        return logbook
