from abc import ABC, abstractmethod

import util


class CERNClient(ABC):
    """ 
    Base CERN Client class 
    Provides functionality to fetch data.
    """

    @abstractmethod
    def get_all_collections(self):
        raise NotImplementedError

    @abstractmethod
    def query(self, pattern, start=util.DATETIME_EARLIEST, end=util.DATETIME_LATEST, filters=False, resample_to=None,
              fillna=True):
        """
        Queries all collections matching the pattern and collects the data in a single dataframe. 
        Optionally, the data can be filtered and resampled.
        Missing values will be padded with the previous value or the next one in case a series starts with NaN.
        :param pattern: regex string, can be specific ("MKI.A5R8.B2:PRESSURE") or with wildcards ("^.*PRESSURE$")
        :param start: first entry according to start_time field
        :param end: last entry (not included) according to start_time field
        :param filters: if True, filter the data before resampling
        :param resample_to: String, target conversion, number + 'D' for days, 'T' for minutes, 'S' for seconds
        :return: DataFrame with a DateTimeIndex and collections in different columns
        :param fillna: Whether or not to fill missing values using the forward fill approach
        :rtype: pd.DataFrame
        """
        raise NotImplementedError

    @abstractmethod
    def query_elogbook(self, start=util.DATETIME_EARLIEST, end=util.DATETIME_LATEST, beam=None):
        """
        Creates a nicely formatted dataframe of the E-logbook
        Filtering by datetimes has to happen manually after fetching all elogbook entries, as they
        are all stored in a couple of 6 month long MongoDB documents in the MKI.ELOGBOOK_tagged collection.
        So the 'start-time' filter in 'query_collection' can only compare to the few 'start-time' values
        in the collection. Maybe a different kind of MongoDB query could be used that returns entries based on their
        timestamp instead of their document's 'start-time'.
        :param start: start datetime of tags
        :param end: end datetime of tags
        :param beam: which beam's data to use: 1, 2, or both
        :return: df with timestamps, tagged time, id, path, comment, tag username and value
        """
        raise NotImplementedError
