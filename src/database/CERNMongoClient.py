import re

import pandas as pd
import pymongo as pm

import util
from database.CERNClient import CERNClient
from preprocessing import filter


class CERNMongoClient(CERNClient):
    """
    Client for fetching CERN data using MongoDB
    """
    def __init__(self, host='localhost', port=27017, db='cern_ldb', logging=True, hostAsUri=False):
        """
        :param host: default localhost
        :param port: default 27017
        :param db: database name, default cern_ldb
        """
        if hostAsUri:
            self.client = pm.MongoClient(host)
        else:
            self.client = pm.MongoClient(host, port)

        self.db = self.client[db]
        self.logging = logging

    def get_all_collections(self):
        """
        :return: list of strings with all collection names
        """
        return self.db.list_collection_names()

    def query(self, pattern, start=util.DATETIME_EARLIEST, end=util.DATETIME_LATEST, filters=False, resample_to=None,
              fillna=True):
        # make sure that start and end dates are strings
        start = str(start)
        end = str(end)

        if self.logging:
            print("Fetching data for pattern {0} from {1} to {2}".format(pattern, start, end))

        collections = [col for col in self.get_all_collections() if re.search(pattern, col)]

        df = pd.DataFrame()
        for collection in collections:
            data = self._query_collection(collection, start, end)
            data = data[~data.index.duplicated(keep='first')]  # drop duplicates, needed for overlapping archives
            df = pd.concat([df, data], axis=1)

        # drop data 2018 with missing IPOC measurements
        missing_ipoc_indices = ['2018-11-11 15:12:21.949000120',
                                '2018-10-22 08:31:48.134000063',
                                '2018-10-30 10:02:20.870000124']
        for i in missing_ipoc_indices:
            if i in df.index:
                df.drop([pd.to_datetime(i)], inplace=True)

        # Apply filters on whole DataFrame instead of separate columns, because some IPOC measurements are filtered
        # by filters for other IPOC measurements since IPOC measurement timestamps are equal within one beam.
        if filters:
            df = filter.filter_extremes(df)
        # Always resample after the data has been filtered. Otherwise, incorrect values will be added to the results.
        # Never resample when IPOC data is mixed with continuous data. IPOC data should not be resampled.
        if resample_to is not None:
            df = df.resample(resample_to).mean()

        if self.logging:
            print()

        if fillna:
            # Hold values in case of NaN, first forwards then backwards in case first value is NaN
            df = df.fillna(method='ffill').fillna(method='backfill')

        return df

    def _query_collection(self, collection_name, start=util.DATETIME_EARLIEST, end=util.DATETIME_LATEST):
        """
        Queries a collection
        :param collection_name: name of the collection
        :param start: first entry according to start_time field
        :param end: last entry (not included) according to start_time field
        :return: resampled dataframe, columns is collection
        """
        collection = self.db[collection_name]
        if collection.count() == 0:
            print("Error: Empty or non-existing collection!")
        timestamps = []
        values = []
        # Set the start a week back, because the current start could lie in a document of the week
        # before start that contains data beyond start. Similarly, set the end a week later, because the current end
        # could lie somewhere in the beginning of the next document ('start-time' isn't always correct, or something
        # could be off about the timezone of the timestamps)
        # Afterwards, trim the results to the correct range.
        query_start_datetime = str(pd.to_datetime(start) - pd.DateOffset(weeks=1))
        query_end_datetime = str(pd.to_datetime(end) + pd.DateOffset(weeks=1))
        docs = collection.find({
            'start-time': {
                '$gte': query_start_datetime,
                '$lt': query_end_datetime
            }
        }).sort('start-time')

        for doc in docs:
            timestamps.extend(doc['timestamps'])
            values.extend(doc['values'])

        df = pd.DataFrame({collection_name: values}, index=pd.to_datetime(timestamps, unit='s'))
        df.rename_axis('timestamps', inplace=True)
        df.rename_axis("series", axis="columns", inplace=True)
        # Filter out data outside of the given datetime interval. Data given by the find query can lie outside this
        # interval because the documents can contain data for a whole week.
        df = df[start:end]

        controller_vars = ["MKI.UA23.F3.CONTROLLER:KICK_DELAY_TOPLAY",
                           "MKI.UA23.F3.CONTROLLER:KICK_ENABLE_TOPLAY",
                           "MKI.UA23.F3.CONTROLLER:KICK_LENGTH_TOPLAY",
                           "MKI.UA23.F3.CONTROLLER:KICK_STRENGTH_TOPLAY",
                           "MKI.UA23.F3.CONTROLLER:KICK_TIME_TOPLAY",
                           "MKI.UA87.F3.CONTROLLER:KICK_DELAY_TOPLAY",
                           "MKI.UA87.F3.CONTROLLER:KICK_ENABLE_TOPLAY",
                           "MKI.UA87.F3.CONTROLLER:KICK_LENGTH_TOPLAY",
                           "MKI.UA87.F3.CONTROLLER:KICK_STRENGTH_TOPLAY",
                           "MKI.UA87.F3.CONTROLLER:KICK_TIME_TOPLAY"]
        if collection_name in controller_vars:
            df[collection_name] = df[collection_name].apply(lambda x: x[0])
        return df

    def query_elogbook(self, start=util.DATETIME_EARLIEST, end=util.DATETIME_LATEST, beam=None):
        collection = "MKI.ELOGBOOK_tagged"
        # First, need to fetch all entries first because they are all stored in a single MongoDB document.
        entries = self.query(collection, start=start, end=end)
        # Then, filter out with start and end.
        entries = entries[collection].apply(pd.Series).sort_index()
        entries = entries.ix[start:end]

        # Add beam number.
        entries["BEAM"] = [1 if installation == "MKI2" else 2 for installation in entries["VALUE"]]

        # Hardcoded fix for C0 column which occurs in certain logs
        if 'C0' in entries.columns:
            entries = entries.drop(["C0"], axis=1)

        if beam == 1:
            return entries[entries["VALUE"] == "MKI2"]
        elif beam == 2:
            return entries[entries["VALUE"] == "MKI8"]
        else:
            return entries
