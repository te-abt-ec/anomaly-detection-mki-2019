#!/usr/bin/python3.4
"""
Created by Pieter Van Trappen, CERN

Fetches the single elogbook document from the mongodb collection and
asks the user for tagging data: info, intervention, research (MD, ..), etc.

./tag_mongodb_elogbook.py MKI.ELOGBOOK '2015-06-23 07:57:32.000000'

#TODO: Add 'anomaly' to indicate what's detectable with the current data?
"""

import sys

from fetch_data import MongoDB

class Color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

if __name__ == '__main__':
    uoptions = {'i': 'info', 'v': 'intervention', 'r': 'research', 'f': 'fault', 'a': 'anomaly', 'q': '*quit*'}

    if len(sys.argv) < 2:
        print("please specify: %s collection-name (optional 'start-time' value)" % sys.argv[0])
        sys.exit()

    # select a collection
    conn = MongoDB()
    var = sys.argv[1]
    vart = var + '_tagged'
    if vart in conn.db.list_collection_names() and conn.db[vart].count_documents({}) > 0 and \
       conn.db[vart].count_documents({}) == conn.db[var].count_documents({}):
        # last condition: only select the tagged collection if all 'var' ones have been tagged
        print('-----------------------------------------------------------------------------')
        print('Existing %s collection found, will be used' % vart)
        coll = conn.db[vart]
    elif conn.db[var].count_documents({}) > 0:
        coll = conn.db[var]
    else:
        print('No collections with a valid document found, quiting')
        sys.exit()

    # select a document
    doc = None
    if  coll.count_documents({}) > 1 and len(sys.argv) != 3:
        print('More than 1 document found, please specify a projection filter')
        sys.exit()
    elif coll.count_documents({}) > 1 and len(sys.argv) == 3:
        proj = {'start-time':sys.argv[2]}
        print('More than 1 document found, using find(%s) projection...' % proj)
        if vart in conn.db.list_collection_names() and conn.db[vart].count_documents(proj) > 0:
            # switch to the tagged collection if it exists
            coll = conn.db[vart]
        doc = coll.find_one(proj)
    else:
        doc = coll.find_one()
    if not doc:
        print('No valid document found, aborting...')
        sys.exit()

    # loop over the list of dictionaries in 'values'
    for e in doc['values']:
        print('-----------------------------------------------------------------------------')
        print(Color.BOLD + e['EVENTDATE'] + Color.END)
        print(e['USERNAME'])
        print(e['SUBSTR_COMMENT_512_'])
        print('-----------------------------------------------------------------------------')
        print(uoptions)
        # TAG
        # recover previous tag if in uoptions dict
        if e['TAG'] in uoptions.values():
            tagcolor = Color.GREEN
            tag = [k for k, v in uoptions.items() if v == e['TAG']][0]
        else:
            tagcolor = Color.BLUE
            tag = 'i'
        print(tagcolor + "Please specify logbook entry TAG (%s): " % (tag) + Color.END)
        uchoice = input()
        while not (uchoice == '' or uchoice in uoptions):
            uchoice = input(tagcolor + "Please specify logbook entry TAG (%s): " % (tag) + Color.END)
        # default 'info' when enter pressed
        uchoice = tag if uchoice == '' else uchoice
        if uchoice == 'q':
            break
        # replace existing value for TAG
        e['TAG'] = uoptions[uchoice]
        # VALUE and PATH
        # if not existing, let the user assign the installation (MKI2 or MKI8)
        cv = 'VALUE'
        cp = 'PATH'
        if not e[cv] or not e[cp]:
            cops = {'2': ('MKI2', 'LHC.MKI2'), '8': ('MKI8', 'LHC.MKI8'), 'q': ('*quit*')}
            val = '2'
            print(cops)
            print(Color.BLUE + "Please specify logbook entry VALUE (%s): "%(val) + Color.END)
            uchoice = input()
            while not (uchoice == '' or uchoice in cops.keys()):
                uchoice = input(tagcolor + "Please specify logbook entry VALUE (%s): " % (val) + Color.END)
                print(uchoice)
            uchoice = val if uchoice == '' else uchoice
            if uchoice == 'q':
                break
            # replace existing value for TAG
            e[cv] = cops[uchoice][0]
            e[cp] = cops[uchoice][1]

    # write out to new collection (.._tagged)
    startt = doc['start-time']
    samplesize = doc['length']
    if conn.exist_doc(vart, startt, samplesize):
        uchoice = input('Existing document, do you want to overwrite (y,n): ')
        if uchoice in ('n', ''):
            sys.exit()
    conn.db[vart].delete_many({'start-time': startt})
    conn.write_doc(vart, [doc['timestamps'], doc['values']], startt, samplesize)
    print('Written to mongodb')
