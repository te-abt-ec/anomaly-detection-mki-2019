import glob
import json
import logging
import os
from base64 import urlsafe_b64encode
from operator import itemgetter
from urllib.parse import urlencode

import pandas as pd

CERN_DIR = os.path.join(os.path.dirname(__file__), "..")
CERN_DATA_DIR = os.path.join(CERN_DIR, "data-cern")
CERN_LABELS = ["info", "intervention", "research", "fault", "anomaly"]

DETECTORS = ["gmm", "iforest"]

# Earliest and latest measurement timestamp common to all collections
DATETIME_EARLIEST = "2016-03-10 00:00:00"
DATETIME_LATEST = "2019-01-01 00:00:00"

# Earliest and latest date used for training
TRAINTIME_EARLIEST = "2017-05-01 00:00:00"
TRAINTIME_LATEST = "2017-08-01 00:00:00"

# Used for saving figures
FILE_EXTENSION = ".png"


def beam_to_num(beam):
    if beam == 1:
        return "UA23"
    elif beam == 2:
        return "UA87"
    else:
        raise ValueError('Error: beam should be 1 or 2.')


def beam_to_str(beam):
    if beam == 1:
        return "B1"
    elif beam == 2:
        return "B2"
    else:
        raise ValueError('Error: beam should be 1 or 2.')


def num_to_pred(num):
    if num == 0:
        return 'TP'
    elif num == 1:
        return 'TN'
    elif num == 2:
        return 'FP'
    elif num == 3:
        return 'FN'
    else:
        raise ValueError('Only 0, 1, 2 and 3 are supported.')


# maximum distance in HOURS between anomaly occurring and label being entered in the ELOGBOOK
MAX_LOGGING_DELAY = 12

# Sliding window size = 10 minutes recommended by Pieter: Dit is rekening houdend met een spanningsoverslag
# anomalie waar het tot 10 minuten kan duren voor het vacuüm zich herstelt; langer vraagt het normaal gezien niet.
SLIDING_WINDOW_SIZE = 10 * 60

MAGNETS = "(A|B|C|D)"
BEAMS = [1, 2]

# arrays of (query, measurement_name, unit)
MEASUREMENTS_LHC = [
    (r"^LHC\.BCTFR\.A6R4.B(1|2):BEAM_INTENSITY$", "BEAM_INTENSITY", "Total charge"),
    (r"^LHC\.BQM\.B(1|2):BUNCH_LENGTH_MEAN$", "BUNCH_LENGTH_MEAN", "s")
]

MEASUREMENTS_CONTINUOUS = [
    (r"^MKI\.(A|B|C|D)5(L2|R8)\.B(1|2):PRESSURE$", "PRESSURE", "mbar"),
    (r"^MKI\.(A|B|C|D)5(L2|R8)\.B(1|2):TEMP_MAGNET_DOWN$", "TEMP_MAGNET_DOWN", "°C"),
    (r"^MKI\.(A|B|C|D)5(L2|R8)\.B(1|2):TEMP_MAGNET_UP$", "TEMP_MAGNET_UP", "°C"),
    (r"^MKI\.(A|B|C|D)5(L2|R8)\.B(1|2):TEMP_TUBE_DOWN$", "TEMP_TUBE_DOWN", "°C"),
    (r"^MKI\.(A|B|C|D)5(L2|R8)\.B(1|2):TEMP_TUBE_UP$", "TEMP_TUBE_UP", "°C")
]

MEASUREMENTS_CONTROLLER = [
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_COUNT_TOPLAY$", "KICK_COUNT_TOPLAY", "???"),
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_DELAY_TOPLAY$", "KICK_DELAY_TOPLAY", "ns"),
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_ENABLE_TOPLAY$", "KICK_ENABLE_TOPLAY", "???"),
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_LENGTH_TOPLAY$", "KICK_LENGTH_TOPLAY", "ns"),
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_STRENGTH_TOPLAY$", "KICK_STRENGTH_TOPLAY", "V"),
    (r"^MKI\.UA(23|87)\.F3\.CONTROLLER:KICK_TIME_TOPLAY$", "KICK_TIME_TOPLAY", "ns"),
]

MEASUREMENTS_IPOC = [
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):E_KICK$", "E_KICK", "GeV"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):I_STRENGTH$", "I_STRENGTH", "kA"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):T_DELAY$", "T_DELAY", "μs"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):T_FALLTIME$", "T_FALLTIME", "μs"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):T_LENGTH$", "T_LENGTH", "μs"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):T_RISETIME$", "T_RISETIME", "μs"),
    (r"^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):T_START_TH$", "T_START_TH", "μs"),
]

MEASUREMENTS_STATE = [
    (r"^MKI\.UA(23|87)\.STATE:CONTROL$", "CONTROL", None),
    (r"^MKI\.UA(23|87)\.STATE:MODE$", "MODE", None),
    (r"^MKI\.UA(23|87)\.STATE:SOFTSTARTSTATE$", "SOFTSTARTSTATE", None),
    (r"^MKI\.UA(23|87)\.STATE:STATUS$", "STATUS", None),
]


def load_df_from_csv_with_name(filename):
    """
    Returns a DataFame loaded from a certain csv file. If filename is not found exactly but as part of other files,
    returns the one that was modified latest.
    :param filename: string, name of the file
    :return: DataFrame built from a csv file
    """
    print("Loading file '{}'.".format(filename))

    if not os.path.exists(filename):
        alternatives = glob.glob(filename + "*")
        paths_sorted = sorted([(f, os.path.getmtime(f)) for f in alternatives], key=itemgetter(1))
        if len(paths_sorted):
            filename = paths_sorted[-1][0]
            print("File not found, loading '{}' instead.".format(filename))

    return filename, pd.read_csv(filename, index_col=0, parse_dates=True)


def load_df_from_csv(filename):
    _, df = load_df_from_csv_with_name(filename)
    return df


def list_predictions():
    path = './'
    files = glob.glob(path + 'grid_search_*.csv')
    files.extend(glob.glob(path + 'pipeline_*.csv'))
    files.extend(glob.glob(path + 'feature_selection_*.csv'))
    return sorted(files)


def first(iterable, default=None, key=None):
    if key is None:
        for el in iterable:
            if el:
                return el
    else:
        for el in iterable:
            if key(el):
                return el
    return default


def log_and_print(text, **kwargs):
    logging.info(text)
    print(text, **kwargs)


def generate_params(state):
    encoded = urlsafe_b64encode(json.dumps(state).encode())
    params = urlencode(dict(params=encoded))
    return f'?{params}'


def get_html_url_for_timespan(filename, timestamp_min, timestamp_max, beam=1):
    if beam == 1:
        cols1 = ['MKI.A5L2.B1:PRESSURE', 'MKI.B5L2.B1:PRESSURE', 'MKI.C5L2.B1:PRESSURE', 'MKI.D5L2.B1:PRESSURE']
        cols2 = ['MKI.A5L2.B1:TEMP_MAGNET_UP', 'MKI.B5L2.B1:TEMP_MAGNET_UP',
                 'MKI.C5L2.B1:TEMP_MAGNET_UP', 'MKI.D5L2.B1:TEMP_MAGNET_UP']
    else:
        cols1 = ['MKI.A5R8.B2:PRESSURE', 'MKI.B5R8.B2:PRESSURE', 'MKI.C5R8.B2:PRESSURE', 'MKI.D5R8.B2:PRESSURE']
        cols2 = ['MKI.A5R8.B2:TEMP_MAGNET_UP', 'MKI.B5R8.B2:TEMP_MAGNET_UP',
                 'MKI.C5R8.B2:TEMP_MAGNET_UP', 'MKI.D5R8.B2:TEMP_MAGNET_UP']

    state = {
        'beam-dropdown': (['value'], (1,)),
        'dataset1-dropdown': (['value'], (cols1,)),
        'dataset2-dropdown': (['value'], (cols2,)),
        'date-picker': (['start_date', 'end_date'], (timestamp_min, timestamp_max)),
        'predictions': (['value'], ('./' + filename,)),
        'segment-score-method': (['value'], ('max',)),
    }
    return '<a href="http://127.0.0.1:8050/{0}">Open in explorer</a>'.format(generate_params(state))
