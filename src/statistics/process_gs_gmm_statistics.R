dir = 'master/anomaly-detection-mki-2019/text-2019/img/results/gs/gmm/2017/'
var <- gs2017_gmm
attach(var)

pdf(paste(dir,'n_components.pdf', sep=''), height=4, width=6) 
par(cex=2)
par(mar=c(4.1,4.1,0.1,0.1))
boxplot(auc ~ n_components, xlab='n_components', ylab='AUC')
dev.off()

pdf(paste(dir,"covariance_type.pdf", sep=''), height=4, width=6) 
par(cex=2)
par(mar=c(4.1,4.1,0.1,0.1))
boxplot(auc ~ covariance_type, xlab='covariance type', ylab='AUC')
dev.off() 

pdf(paste(dir,"n_init.pdf", sep=''), height=4, width=6) 
par(cex=2)
par(mar=c(4.1,4.1,0.1,0.1))
boxplot(auc ~ n_init, xlab='n_init', ylab='AUC')
dev.off() 

detach(var)

