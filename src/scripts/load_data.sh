#!/usr/bin/env bash
# Load CERN data

if [[ $# -eq 0 ]]
  then
    echo "Error: no host of db supplied. Use 'localhost' on your local machine."
    exit -1
fi

echo "Load CERN data into MongoDB"
mongorestore --host $1 --drop -d cern_ldb --archive=data-cern/cern_ldb-mongodb-2015_06_22-2016_04_12.gz --gzip
mongorestore --host $1 -d cern_ldb --archive=data-cern/cern_ldb-mongodb-2016_04_17-2016_09_09.gz --gzip --noIndexRestore
mongorestore --host $1 -d cern_ldb --archive=data-cern/cern_ldb-mongodb-2016_09_09-2016_11_28.gz --gzip --noIndexRestore
mongorestore --host $1 -d cern_ldb --archive=data-cern/cern_ldb-mongodb-2016_11_28-2017_09_24.gz --gzip --noIndexRestore
mongorestore --host $1 -d cern_ldb --archive=data-cern/cern_ldb-mongodb-elogbook_missing_key-2015_06_22-2017_09_24.gz --gzip --noIndexRestore
mongorestore --host $1 -d cern_ldb --archive=data-cern/cern_ldb-mongodb-2017_09_24-2018_01_02.gz --gzip --noIndexRestore
mongorestore --host $1 -d cern_ldb --archive=data-cern/cern_ldb-mongodb-2018_01_01-2019_01_01.gz --gzip --noIndexRestore
printf "done, reindexing now..\n"

# When using multiple restores, the indexes have to be regenerated before generating the features
pipenv run src/scripts/reindex_mongodb.py $1
printf "done\n"
