#!/usr/bin/env python3

import os
import sys
from pandas.plotting import register_matplotlib_converters

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import plot.data
import util
from database.CERNMongoClient import CERNMongoClient
from preprocessing import filter


def get_filename(measurement_type, measurement, year, resample, beams=False, filtered=False, raw=False):
    "measurement_{}_year_{}-resampled_to_{}-beams"
    filename = str(measurement_type) + "_" + str(measurement) + "_" + str(year)
    if resample is not None:
        filename += "_resampled_to_" + str(resample)
    if beams:
        filename += "_beams"
    if filtered:
        filename += "_filtered"
    if raw:
        filename += "_vs_raw"

    dir = os.path.join(os.path.dirname(__file__), "figures", "data")
    if not os.path.isdir(dir):
        os.makedirs(dir)
    return os.path.join(dir, filename)


def explore_ipoc():
    global pattern, name, unit
    # Query all ipoc data into one df so that filtering can be applied correctly
    ipoc_df = db.query("^MKI\.UA(23|87)\.IPOC\.(A|B|C|D)B(1|2):", start=start, end=end, filters=False)
    ipoc_df_filtered = filter.filter_extremes(ipoc_df)
    for pattern, name, unit in util.MEASUREMENTS_IPOC:
        col = ipoc_df.filter(regex=pattern)
        col_filtered = ipoc_df_filtered.filter(regex=pattern)

        plot.data.ipoc(
            col, name, unit, show=show,
            filename=get_filename("ipoc", name, y, None)
        )
        plot.data.ipoc_beams(
            col, name, unit, show=show,
            filename=get_filename("ipoc", name, y, None, True)
        )
        plot.data.ipoc_beams(
            col_filtered, name + ", filtered", unit, show=show,
            filename=get_filename("ipoc", name, y, None, True, True)
        )
        plot.data.ipoc_beams_filtered(
            col, col_filtered, name, unit, show=show,
            filename=get_filename("ipoc", name, y, None, True, True, True)
        )


def explore_lhc():
    global pattern, name, unit, df, title
    for pattern, name, unit in util.MEASUREMENTS_LHC:
        df = db.query(pattern, start=start, end=end, filters=False, resample_to=resample_to)

        title = name + ", resampled to {}".format(resample_to)

        plot.data.continuous(
            df, title, unit, show=show,
            filename=get_filename("lhc", name, y, resample_to)
        )
        plot.data.continuous_beams(
            df, title, unit, show=show,
            filename=get_filename("lhc", name, y, resample_to, True)
        )


def explore_continuous():
    global pattern, name, unit, df, title
    for pattern, name, unit in util.MEASUREMENTS_CONTINUOUS:
        df = db.query(pattern, start=start, end=end, filters=False, resample_to=resample_to)
        df_filtered = filter.filter_extremes(df)

        title = name + ", resampled to {}".format(resample_to)

        plot.data.continuous(
            df, title, unit, show=show,
            filename=get_filename("continuous", name, y, resample_to)
        )
        plot.data.continuous_beams(
            df, title, unit, show=show,
            filename=get_filename("continuous", name, y, resample_to, True)
        )
        plot.data.continuous_beams(
            df_filtered, title + ", filtered", unit, show=show,
            filename=get_filename("continuous", name, y, resample_to, True, True)
        )
        plot.data.continuous_beams_filtered(
            df, df_filtered, title, unit, show=show,
            filename=get_filename("continuous", name, y, resample_to, True, True, True)
        )


def explore_state():
    global pattern, measurement, unit, data, fname
    for pattern, measurement, unit in util.MEASUREMENTS_STATE:
        data = db.query(pattern, start=start, end=end)
        # state data is stored as strings, need to convert to numeric
        for c in data:
            data[c] = data[c].astype(float)
        fname = get_filename("state", measurement, y, resample_to)

        if not data.empty:
            plot.data.ipoc(data, measurement, unit, show=show, filename=fname)


def explore_controller():
    global pattern, measurement, unit, data, fname
    for pattern, measurement, unit in util.MEASUREMENTS_CONTROLLER:
        data = db.query(pattern, start=start, end=end)
        fname = get_filename("controller", measurement, y, resample_to)

        plot.data.ipoc(data, measurement, unit, show=show, filename=fname)


if __name__ == "__main__":
    db = CERNMongoClient()
    register_matplotlib_converters()

    time_periods = [
        ("2016-03-10", "2016-12-31", "2016"),
        ("2017-01-01", "2017-12-31", "2017"),
        (util.DATETIME_EARLIEST, util.DATETIME_LATEST, "all")
    ]

    resample_to = "1min"
    show = False

    for (start, end, y) in time_periods:
        print("start = {}, end = {}, y = {}".format(start, end, y))

        #explore_ipoc()
        #explore_lhc()
        explore_continuous()
        #explore_state()
        #explore_controller()
