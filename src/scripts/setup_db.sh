#!/usr/bin/env bash
# Start the database

CMD=mongo
pkill $CMD
# certain systems have already started mongod as a service, only if there's no remaining mongo*, it should be restarted
pgrep $CMD
if [[ $? -eq 1 ]]; then
    mkdir -p /data/db
    touch mongodb.log
    echo > mongodb.log # empty log for new session

    echo "Start MongoDB"
    # see https://stackoverflow.com/questions/15443106/how-to-check-if-mongodb-is-up-and-ready-to-accept-connections-from-bash-script
    mongod  --fork --logpath mongodb.log --logappend

    # wait until mongo logs that it's ready (or timeout after 60s)
    COUNTER=0
    grep -q 'waiting for connections on port' mongodb.log
    while [[ $? -ne 0 && $COUNTER -lt 60 ]] ; do
	sleep 2
	let COUNTER+=2
	echo "Waiting for mongo to initialize... ($COUNTER seconds so far)"
	grep -q 'waiting for connections on port' mongodb.log
    done
    printf "done\n"
fi