#!/usr/bin/env python3

import argparse
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from database.CERNMongoClient import CERNMongoClient
import util
from pipeline.pipeline import pipeline
from preprocessing import builder


def build_16_17(db, parallel):
    # ENTIRE 2016
    START_2016 = "2016-03-10"
    END_2016 = "2016-12-31"

    # ENTIRE 2017
    START_2017 = "2017-03-10"
    END_2017 = "2017-12-31"

    builder.build_features_to_file(db, beam, kind='2016', start=START_2016, end=END_2016,
                                   sliding_window_size=util.SLIDING_WINDOW_SIZE, parallel=parallel)
    builder.build_features_to_file(db, beam, kind='2017', start=START_2017, end=END_2017,
                                   sliding_window_size=util.SLIDING_WINDOW_SIZE, parallel=parallel)

    if parallel:
        db = CERNMongoClient(host=db)
    builder.build_state_mode_to_file(db, beam, kind='2016', start=START_2016, end=END_2016)
    builder.build_state_mode_to_file(db, beam, kind='2017', start=START_2017, end=END_2017)


def build_quarters(db, parallel):
    # Q1 and Q2 of 2017
    START_Q1_Q2_2017 = "2017-01-01"
    END_Q1_Q2_2017 = "2017-06-01"

    # Q3 and Q4 of 2017
    START_Q3_Q4_2017 = "2017-06-01"
    END_Q3_Q4_2017 = "2017-12-31"

    builder.build_features_to_file(db, beam, kind='Q1_Q2_2017', start=START_Q1_Q2_2017, end=END_Q1_Q2_2017,
                                   sliding_window_size=util.SLIDING_WINDOW_SIZE, parallel=parallel)
    builder.build_features_to_file(db, beam, kind='Q3_Q4_2017', start=START_Q3_Q4_2017, end=END_Q3_Q4_2017,
                                   sliding_window_size=util.SLIDING_WINDOW_SIZE, parallel=parallel)

    if parallel:
        db = CERNMongoClient(host=db)
    builder.build_state_mode_to_file(db, beam, kind='Q3_Q4_2017', start=START_Q3_Q4_2017, end=END_Q3_Q4_2017)


def validate_16_17():
    global detector, parameters
    # LOAD DATA
    X_2016 = builder.load_features_from_file(beam, kind='2016')
    X_2017 = builder.load_features_from_file(beam, kind='2017')
    state_mode_2016 = builder.load_state_mode_from_file(beam, kind='2016')
    state_mode_2017 = builder.load_state_mode_from_file(beam, kind='2017')

    print('* Training model on 2016, predicting anomalies for 2017 *')
    res = pipeline(beam=beam,
                   scale_data='standard',
                   anomaly_detector=detector,
                   detector_params=parameters,
                   labels=labels,
                   segment_score='max',
                   x_train=X_2016,
                   state_mode=state_mode_2017,
                   x_test=X_2017)
    res['truth_and_pred_df'].to_csv('cross_validation_' + util.beam_to_str(beam) + '_train16_test17.csv')

    print('* Training model on 2017, predicting anomalies for 2016 *')
    res = pipeline(beam=beam,
                   scale_data='standard',
                   anomaly_detector=detector,
                   detector_params=parameters,
                   labels=labels,
                   segment_score='max',
                   x_train=X_2017,
                   state_mode=state_mode_2016,
                   x_test=X_2016)
    res['truth_and_pred_df'].to_csv('cross_validation_' + util.beam_to_str(beam) + '_train17_test16.csv')


def validate_quarters():
    global detector, parameters
    # LOAD DATA
    X_Q1_Q2 = builder.load_features_from_file(beam, kind='Q1_Q2_2017')
    X_Q3_Q4 = builder.load_features_from_file(beam, kind='Q3_Q4_2017')
    state_mode_Q3_Q4 = builder.load_state_mode_from_file(beam, kind='Q3_Q4_2017')

    print('* Training model on Q1/2 2017, predicting anomalies for Q3/4 2017 *')
    res = pipeline(beam=beam,
                   scale_data='standard',
                   anomaly_detector=detector,
                   detector_params=parameters,
                   labels=labels,
                   segment_score='max',
                   x_train=X_Q1_Q2,
                   state_mode=state_mode_Q3_Q4,
                   x_test=X_Q3_Q4)
    res['truth_and_pred_df'].to_csv('cross_validation_' + util.beam_to_str(beam) + '_train_Q1_Q2_test_Q3_Q4.csv')


if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--beam', dest='beam', type=int, default=1, help='Beam (default: 1)')
    parser.add_argument('--host', dest='host', default='localhost', help='Host of MongoDB (default: localhost)')
    parallel_parser = parser.add_mutually_exclusive_group(required=False)
    parallel_parser.add_argument('--parallel', dest='parallel', action='store_true')
    parallel_parser.add_argument('--no-parallel', dest='parallel', action='store_false')
    parser.set_defaults(parallel=False)

    args = parser.parse_args()

    beam = args.beam
    parallel = args.parallel
    if parallel:
        db = args.host
    else:
        db = CERNMongoClient(host=args.host)

    build_16_17(db, parallel)
    build_quarters(db, parallel)

    db = CERNMongoClient(host=args.host)

    # Logbook
    builder.build_logbook_to_file(db, beam)
    logbook = builder.load_logbook_from_file(beam)
    labels = builder.anomalies(logbook)

    # SET ANOMALY DETECTOR AND PARAMETERS
    detector = 'gmm'
    parameters = {
        'n_components': 6,
        'covariance_type': 'full',
        'tol': 1e-3,
        'reg_covar': 1e-6,
        'max_iter': 100,
        'n_init': 1,
        'init_params': 'kmeans',
        'verbose': 1,
    }

    validate_16_17()
    validate_quarters()
