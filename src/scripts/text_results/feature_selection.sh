#!/usr/bin/env bash
# Execute all Feature selections

FEATURE_SELECTION_STEPS=10

echo "Feature selection GMM for beam 1"
nohup pipenv run src/scripts/feature_selection.py -b 1 -d gmm -i 20 -fntrn train2016 -fntst test2016 -fno 2016 -p general > fs_b1_gmm_2016.txt &
nohup pipenv run src/scripts/feature_selection.py -b 1 -d gmm -i 20 -fntrn train2017 -fntst test2017 -fno 2017 -p 2017 > fs_b1_gmm_2017.txt
nohup pipenv run src/scripts/feature_selection.py -b 1 -d gmm -i 20 -fntrn train2018 -fntst test2018 -fno 2018 -p 2018 > fs_b1_gmm_2018.txt &
echo "...B1 done"

echo "Feature selection GMM for beam 2"
#pipenv run src/scripts/feature_selection.py -b 2 -d gmm -i $FEATURE_SELECTION_STEPS -fntrn train2016 -fntst test2016 -fno 2016 > fs_b2_gmm_2016.txt
#pipenv run src/scripts/feature_selection.py -b 2 -d gmm -i $FEATURE_SELECTION_STEPS -fntrn train2017 -fntst test2017 -fno 2017 > fs_b2_gmm_2017.txt
#pipenv run src/scripts/feature_selection.py -b 2 -d gmm -i $FEATURE_SELECTION_STEPS -fntrn train2018 -fntst test2018 -fno 2018 > fs_b2_gmm_2018.txt
echo "...B2 done"

echo "Feature selection iForest for beam 1"
nohup pipenv run src/scripts/feature_selection.py -b 1 -d iforest -i 20 -fntrn train2016 -fntst test2016 -fno 2016 > fs_b1_iforest_2016.txt &
nohup pipenv run src/scripts/feature_selection.py -b 1 -d iforest -i 20 -fntrn train2017 -fntst test2017 -fno 2017 -p 2017 > fs_b1_iforest_2017.txt &
nohup pipenv run src/scripts/feature_selection.py -b 1 -d iforest -i 20 -fntrn train2018 -fntst test2018 -fno 2018 -p 2018 > fs_b1_iforest_2018.txt &
echo "...B1 done"

echo "Feature selection iForest for beam 2"
#pipenv run src/scripts/feature_selection.py -b 2 -d iforest -i $FEATURE_SELECTION_STEPS -fntrn train2016 -fntst test2016 -fno 2016 > fs_b2_iforest_2016.txt
#pipenv run src/scripts/feature_selection.py -b 2 -d iforest -i $FEATURE_SELECTION_STEPS -fntrn train2017 -fntst test2017 -fno 2017 > fs_b2_iforest_2017.txt
#pipenv run src/scripts/feature_selection.py -b 2 -d iforest -i $FEATURE_SELECTION_STEPS -fntrn train2018 -fntst test2018 -fno 2018 > fs_b2_iforest_2018.txt
echo "...B2 done"
