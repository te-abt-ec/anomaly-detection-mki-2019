#!/usr/bin/env bash

# Execute all grid searches
echo "Live evaluation GMM for beam 1, 2017"
nohup pipenv run src/scripts/live_evaluation.py -b 1 -y 2017 -d gmm --feature_sel all > live_b1_gmm_2017_all.txt &
nohup pipenv run src/scripts/live_evaluation.py -b 1 -y 2017 -d gmm --feature_sel optimal > live_b1_gmm_2017_optimal.txt &
echo "...B1 2017 done"

echo "Live evaluation GMM for beam 1, 2018"
nohup pipenv run src/scripts/live_evaluation.py -b 1 -y 2018 -d gmm --feature_sel all > live_b1_gmm_2018_all.txt &
nohup pipenv run src/scripts/live_evaluation.py -b 1 -y 2018 -d gmm --feature_sel optimal > live_b1_gmm_2018_optimal.txt &
echo "...B1 2018 done"

echo "Live evaluation GMM for beam 2"
pipenv run src/scripts/live_evaluation.py -b 2 -y 2017 -d gmm > live_b1_gmm_2018.txt
pipenv run src/scripts/live_evaluation.py -b 2 -y 2018 -d gmm > live_b1_gmm_2018.txt
echo "...B1 done"