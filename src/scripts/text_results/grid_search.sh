#!/usr/bin/env bash

# Execute all grid searches
echo "Grid search GMM for beam 1"
nohup pipenv run src/scripts/grid_search_gmm.py -b 1 -fntrn train2016 -fntst test2016 -fno 2016 -e > gs_b1_gmm_2016.txt &
#pipenv run src/scripts/grid_search_gmm.py -b 1 -fntrn train2017 -fntst test2017 -fno 2017 -e > gs_b1_gmm_2017.txt
#pipenv run src/scripts/grid_search_gmm.py -b 1 -fntrn train2018 -fntst test2018 -fno 2018 -e > gs_b1_gmm_2018.txt
echo "...B1 done"

echo "Grid search GMM for beam 2"
#pipenv run src/scripts/grid_search_gmm.py -b 2 -fntrn train2016 -fntst test2016 -fno 2016 -e > gs_b2_gmm_2016.txt
#pipenv run src/scripts/grid_search_gmm.py -b 2 -fntrn train2017 -fntst test2017 -fno 2017 -e > gs_b2_gmm_2017.txt
#pipenv run src/scripts/grid_search_gmm.py -b 2 -fntrn train2018 -fntst test2018 -fno 2018 -e > gs_b2_gmm_2018.txt
echo "...B2 done"

echo "Grid search iForest for beam 1"
nohup pipenv run src/scripts/grid_search_iforest.py -b 1 -fntrn train2016 -fntst test2016 -fno 2016 -e > gs_b1_iforest_2016.txt &
pipenv run src/scripts/grid_search_iforest.py -b 1 -fntrn train2017 -fntst test2017 -fno 2017 -e > gs_b1_iforest_2017.txt
pipenv run src/scripts/grid_search_iforest.py -b 1 -fntrn train2018 -fntst test2018 -fno 2018 -e > gs_b1_iforest_2018.txt
echo "...B1 done"

echo "Grid search iForest for beam 2"
#pipenv run src/scripts/grid_search_iforest.py -b 2 -fntrn train2016 -fntst test2016 -fno 2016 -e > gs_b2_iforest_2016.txt
#pipenv run src/scripts/grid_search_iforest.py -b 2 -fntrn train2017 -fntst test2017 -fno 2017 -e > gs_b2_iforest_2017.txt
#pipenv run src/scripts/grid_search_iforest.py -b 2 -fntrn train2018 -fntst test2018 -fno 2018 -e > gs_b2_iforest_2018.txt
echo "...B1 done"
