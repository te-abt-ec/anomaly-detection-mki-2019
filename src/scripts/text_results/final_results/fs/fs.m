% load data into fsgmmstatistics and fsiforeststatistics using uiimport

rank16 = fsgmmstatistics.VarName2;
rank17 = fsgmmstatistics.VarName3;
rank18 = fsgmmstatistics.VarName4;

figure
x0=0;
y0=0;
width=800;
height=400;
set(gcf,'position',[x0,y0,width,height])
plot(rank16, '-o')
xlim([1 20])
ylabel('Best rank')
xlabel('Iteration')
title('\fontsize{25} Feature selection (GMM)')
set(gca,'FontSize',18)
hold on
plot(rank17, '-o')
plot(rank18, '-o')
legend('2016', '2017','2018')
hold off

rank16 = fsiforeststatistics.VarName2;
rank17 = fsiforeststatistics.VarName3;
rank18 = fsiforeststatistics.VarName4;

figure
x0=0;
y0=0;
width=800;
height=400;
set(gcf,'position',[x0,y0,width,height])
plot(rank16, '-o')
xlim([1 20])
ylabel('Best rank')
xlabel('Iteration')
title('\fontsize{25} Feature selection (iForest)')
set(gca,'FontSize',15)
hold on
plot(rank17, '-o')
plot(rank18, '-o')
legend('2016', '2017','2018')
hold off