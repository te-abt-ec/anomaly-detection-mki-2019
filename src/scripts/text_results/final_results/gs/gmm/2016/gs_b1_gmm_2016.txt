Loading file '/root/anomaly-detection-mki-2019/src/scripts/../../data-cern/features_B1_train2016.csv'.
Loading file '/root/anomaly-detection-mki-2019/src/scripts/../../data-cern/state_mode_B1_test2016.csv'.
Loading file '/root/anomaly-detection-mki-2019/src/scripts/../../data-cern/logbook_B1.csv'.
|| Trained detector: 801.6 seconds
|| Scored features: 6.3 seconds
|| Area under PR curve = 0.11622383121339311
|| Rank = 324.27777777777777
|| Trained detector: 507.2 seconds
|| Scored features: 5.1 seconds
|| Area under PR curve = 0.20878286212328984
|| Rank = 329.94444444444446
|| Trained detector: 13.5 seconds
|| Scored features: 0.7 seconds
|| Area under PR curve = 0.16798475270424615
|| Rank = 204.72222222222223
|| Trained detector: 18.6 seconds
|| Scored features: 0.9 seconds
|| Area under PR curve = 0.05791851948997384
|| Rank = 517.9444444444445
|| Trained detector: 15.0 seconds
|| Scored features: 0.8 seconds
|| Area under PR curve = 0.059591308647200636
|| Rank = 547.1111111111111
|| Trained detector: 67.3 seconds
|| Scored features: 5.5 seconds
|| Area under PR curve = 0.09875323760817711
|| Rank = 298.6666666666667
|| Trained detector: 37.6 seconds
|| Scored features: 1.6 seconds
|| Area under PR curve = 0.11926750264297102
|| Rank = 252.5
|| Trained detector: 323.2 seconds
|| Scored features: 4.3 seconds
|| Area under PR curve = 0.11904323429574439
|| Rank = 250.11111111111111
|| Trained detector: 103.5 seconds
|| Scored features: 0.6 seconds
|| Area under PR curve = 0.05250622802940761
|| Rank = 523.2222222222222
|| Trained detector: 172.2 seconds
|| Scored features: 2.7 seconds
|| Area under PR curve = 0.16545664790345474
|| Rank = 418.94444444444446
|| Trained detector: 77.0 seconds
|| Scored features: 5.2 seconds
|| Area under PR curve = 0.11896952062766344
|| Rank = 249.77777777777777
|| Trained detector: 467.0 seconds
|| Scored features: 5.4 seconds
|| Area under PR curve = 0.11569396602322811
|| Rank = 255.66666666666666
|| Trained detector: 49.9 seconds
|| Scored features: 2.4 seconds
|| Area under PR curve = 0.16545664790345474
|| Rank = 418.94444444444446
|| Trained detector: 7.8 seconds
|| Scored features: 1.1 seconds
|| Area under PR curve = 0.11926750264297102
|| Rank = 252.5
|| Trained detector: 171.9 seconds
|| Scored features: 2.3 seconds
|| Area under PR curve = 0.11916154572739937
|| Rank = 252.66666666666666
|| Trained detector: 451.2 seconds
|| Scored features: 4.0 seconds
|| Area under PR curve = 0.11790437687251917
|| Rank = 253.38888888888889
|| Trained detector: 9.5 seconds
|| Scored features: 1.6 seconds
|| Area under PR curve = 0.11926750264297102
|| Rank = 252.5
|| Trained detector: 291.9 seconds
|| Scored features: 8.3 seconds
|| Area under PR curve = 0.08052227396953739
|| Rank = 289.55555555555554
|| Trained detector: 5.0 seconds
|| Scored features: 0.8 seconds
|| Area under PR curve = 0.11003328079575006
|| Rank = 296.5
|| Trained detector: 17.9 seconds
|| Scored features: 0.6 seconds
|| Area under PR curve = 0.11003328079575006
|| Rank = 296.5
|| Trained detector: 92.9 seconds
|| Scored features: 0.5 seconds
|| Area under PR curve = 0.1143135729311231
|| Rank = 212.5
|| Trained detector: 95.3 seconds
|| Scored features: 0.5 seconds
|| Area under PR curve = 0.03779833559199876
|| Rank = 628.3333333333334
|| Trained detector: 83.2 seconds
|| Scored features: 3.0 seconds
|| Area under PR curve = 0.12706003975157995
|| Rank = 246.16666666666666
|| Trained detector: 21.8 seconds
|| Scored features: 2.7 seconds
|| Area under PR curve = 0.11838206818519739
|| Rank = 254.61111111111111
|| Trained detector: 74.9 seconds
|| Scored features: 7.8 seconds
|| Area under PR curve = 0.11581584374443328
|| Rank = 253.94444444444446
|| Trained detector: 161.8 seconds
|| Scored features: 8.5 seconds
|| Area under PR curve = 0.1071499895322574
|| Rank = 256.3888888888889
|| Trained detector: 14.9 seconds
|| Scored features: 1.1 seconds
|| Area under PR curve = 0.05250622802940761
|| Rank = 523.2222222222222
|| Trained detector: 103.9 seconds
|| Scored features: 0.4 seconds
|| Area under PR curve = 0.0314571550994033
|| Rank = 682.2777777777778
|| Trained detector: 48.2 seconds
|| Scored features: 1.3 seconds
|| Area under PR curve = 0.11926750264297102
|| Rank = 252.5
|| Trained detector: 802.9 seconds
|| Scored features: 2.4 seconds
|| Area under PR curve = 0.11185197070799331
|| Rank = 260.27777777777777

| Grid search execution time: 945.4 seconds

| AUC =  0.209, rank = 329.94 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'full', 'init_params': 'kmeans', 'n_components': 4, 'n_init': 5, 'verbose': 0}, time=585.6s 
| AUC =  0.168, rank = 204.72 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'diag', 'init_params': 'kmeans', 'n_components': 2, 'n_init': 1, 'verbose': 0}, time=85.1s 
| AUC =  0.165, rank = 418.94 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'full', 'init_params': 'kmeans', 'n_components': 2, 'n_init': 1, 'verbose': 0}, time=116.0s 
| AUC =  0.165, rank = 418.94 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'full', 'init_params': 'kmeans', 'n_components': 2, 'n_init': 5, 'verbose': 0}, time=242.2s 
| AUC =  0.127, rank = 246.17 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'full', 'init_params': 'kmeans', 'n_components': 4, 'n_init': 1, 'verbose': 0}, time=154.3s 
| AUC =  0.119, rank = 252.50 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'full', 'init_params': 'kmeans', 'n_components': 1, 'n_init': 1, 'verbose': 0}, time=90.7s 
| AUC =  0.119, rank = 252.50 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'full', 'init_params': 'kmeans', 'n_components': 1, 'n_init': 5, 'verbose': 0}, time=115.0s 
| AUC =  0.119, rank = 252.50 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'tied', 'init_params': 'kmeans', 'n_components': 1, 'n_init': 1, 'verbose': 0}, time=79.9s 
| AUC =  0.119, rank = 252.50 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'tied', 'init_params': 'kmeans', 'n_components': 1, 'n_init': 5, 'verbose': 0}, time=103.1s 
| AUC =  0.119, rank = 252.67 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'tied', 'init_params': 'kmeans', 'n_components': 2, 'n_init': 5, 'verbose': 0}, time=241.7s 
| AUC =  0.119, rank = 250.11 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'tied', 'init_params': 'kmeans', 'n_components': 4, 'n_init': 5, 'verbose': 0}, time=386.1s 
| AUC =  0.119, rank = 249.78 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'tied', 'init_params': 'kmeans', 'n_components': 4, 'n_init': 1, 'verbose': 0}, time=153.2s 
| AUC =  0.118, rank = 254.61 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'tied', 'init_params': 'kmeans', 'n_components': 2, 'n_init': 1, 'verbose': 0}, time=90.4s 
| AUC =  0.118, rank = 253.39 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'tied', 'init_params': 'kmeans', 'n_components': 8, 'n_init': 5, 'verbose': 0}, time=479.0s 
| AUC =  0.116, rank = 324.28 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'full', 'init_params': 'kmeans', 'n_components': 6, 'n_init': 5, 'verbose': 0}, time=861.6s 
| AUC =  0.116, rank = 253.94 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'tied', 'init_params': 'kmeans', 'n_components': 6, 'n_init': 1, 'verbose': 0}, time=151.7s 
| AUC =  0.116, rank = 255.67 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'tied', 'init_params': 'kmeans', 'n_components': 6, 'n_init': 5, 'verbose': 0}, time=501.8s 
| AUC =  0.114, rank = 212.50 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'diag', 'init_params': 'kmeans', 'n_components': 2, 'n_init': 5, 'verbose': 0}, time=157.7s 
| AUC =  0.112, rank = 260.28 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'full', 'init_params': 'kmeans', 'n_components': 8, 'n_init': 5, 'verbose': 0}, time=826.5s 
| AUC =  0.110, rank = 296.50 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'diag', 'init_params': 'kmeans', 'n_components': 1, 'n_init': 1, 'verbose': 0}, time=78.9s 
| AUC =  0.110, rank = 296.50 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'diag', 'init_params': 'kmeans', 'n_components': 1, 'n_init': 5, 'verbose': 0}, time=74.0s 
| AUC =  0.107, rank = 256.39 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'tied', 'init_params': 'kmeans', 'n_components': 8, 'n_init': 1, 'verbose': 0}, time=229.9s 
| AUC =  0.099, rank = 298.67 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'full', 'init_params': 'kmeans', 'n_components': 6, 'n_init': 1, 'verbose': 0}, time=137.0s 
| AUC =  0.081, rank = 289.56 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'full', 'init_params': 'kmeans', 'n_components': 8, 'n_init': 1, 'verbose': 0}, time=370.2s 
| AUC =  0.060, rank = 547.11 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'diag', 'init_params': 'kmeans', 'n_components': 8, 'n_init': 1, 'verbose': 0}, time=76.7s 
| AUC =  0.058, rank = 517.94 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'diag', 'init_params': 'kmeans', 'n_components': 6, 'n_init': 1, 'verbose': 0}, time=81.1s 
| AUC =  0.053, rank = 523.22 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'diag', 'init_params': 'kmeans', 'n_components': 4, 'n_init': 1, 'verbose': 0}, time=88.3s 
| AUC =  0.053, rank = 523.22 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'diag', 'init_params': 'kmeans', 'n_components': 4, 'n_init': 5, 'verbose': 0}, time=168.5s 
| AUC =  0.038, rank = 628.33 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'diag', 'init_params': 'kmeans', 'n_components': 8, 'n_init': 5, 'verbose': 0}, time=126.3s 
| AUC =  0.031, rank = 682.28 in for feature=all, scale=standard, a_score_method=max, params={'covariance_type': 'diag', 'init_params': 'kmeans', 'n_components': 6, 'n_init': 5, 'verbose': 0}, time=152.7s 
| Wrote best results to file grid_search_B1_gmm2016-best.csv
