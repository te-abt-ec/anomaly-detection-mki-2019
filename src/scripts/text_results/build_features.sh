#!/usr/bin/env bash
# Build features

echo "Building features for beam 1"
pipenv run src/scripts/build_features.py -b 1 -s 2016-03-10 -e 2017-01-01 -fntrn 2016 -fntst 2016 --parallel
pipenv run src/scripts/build_features.py -b 1 -s 2017-01-01 -e 2018-01-01 -fntrn 2017 -fntst 2017 --parallel
pipenv run src/scripts/build_features.py -b 1 -s 2018-01-01 -e 2019-01-01 -fntrn 2018 -fntst 2018 --parallel
echo "...B1 done"

echo "Building features for beam 2"
pipenv run src/scripts/build_features.py -b 2 -s 2016-03-10 -e 2017-01-01 -fntrn 2016 -fntst 2016 --parallel
pipenv run src/scripts/build_features.py -b 2 -s 2017-01-01 -e 2018-01-01 -fntrn 2017 -fntst 2017 --parallel
pipenv run src/scripts/build_features.py -b 2 -s 2018-01-01 -e 2019-01-01 -fntrn 2018 -fntst 2018 --parallel
echo "...B2 done"