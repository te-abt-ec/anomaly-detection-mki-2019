#!/usr/bin/env python3

import argparse
import os
import sys
import pandas as pd
import warnings

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from database.CERNMongoClient import CERNMongoClient
from pipeline.pipeline import pipeline
from pipeline.grid_search import grid_search
from preprocessing import builder
from evaluation import evaluation
import util


def live_evaluate(mode):
    results = []
    df_TN = []
    df_TP = []
    df_FN = []
    df_FP = []

    for iteration, (train_start, train_end, test_start, test_end) in enumerate(configs):
        print('| Iteration {} predicting: {} - {}'.format(iteration, test_start, test_end))
        TN, TP, FN, FP, result = train_historical_and_predict_next_week(mode, iteration, test_end, test_start,
                                                                        train_end, train_start)
        df_TN.extend(TN)
        df_TP.extend(TP)
        df_FN.extend(FN)
        df_FP.extend(FP)
        results.append(result)

    base_filename = 'live_evaluation'

    # Concat all predictions
    print("|| Concatenate predictions of all iterations")
    truth_filenames = ['live_truth_and_pred_df_' + str(i) + '.csv' for i in range(0, len(configs))]
    all_iterations = [util.load_df_from_csv(filename) for filename in truth_filenames]
    truth_and_pred_df = pd.concat(all_iterations, ignore_index=True)

    truth_and_pred_df_filename = base_filename + '_' + util.beam_to_str(beam) + '_' + detector + year + '.csv'
    truth_and_pred_df.to_csv(truth_and_pred_df_filename)
    print('Wrote truth_and_pred_df to', truth_and_pred_df_filename)

    # Aggregated statistics
    print('* Statistics of live evaluation *')
    print('TP: {}\tFP: {}'.format(len(df_TP), len(df_FP)))
    print('FN: {}\tTN: {}'.format(len(df_FN), len(df_TN)))
    df = pd.DataFrame.from_records(data=results, columns=['iteration', 'test_start', 'test_end', 'auc', 'rank'])
    filename = base_filename + '_statistics.csv'
    df.to_csv(filename)
    print('Wrote statistics to', filename)


def train_historical_and_predict_next_week(mode, iteration, test_end, test_start, train_end, train_start):
    # get the historical data
    x_train = features_df[train_start:train_end]
    x_train = x_train[feature_set] if feature_set is not None else x_train
    state_mode_train = state_mode[train_start:train_end]

    # get the new data for predictions
    x_test = features_df[test_start:test_end]
    x_test = x_test[feature_set] if feature_set is not None else x_test
    state_mode_test = state_mode[test_start:test_end]
    try:
        if mode == 'static':
            # train on old data, predict on new data using fixed parameters
            res = pipeline(beam=beam,
                           scale_data='standard',
                           anomaly_detector=detector,
                           detector_params=parameters,
                           labels=labels,
                           segment_score=segment_score,
                           x_train=x_train,
                           state_mode=state_mode_test,
                           x_test=x_test,
                           write_output=False)
            res['truth_and_pred_df'].to_csv('live_truth_and_pred_df_' + str(iteration) + '.csv')
        elif mode == 'adaptive':
            # execute grid search on training data to obtain the optimal params
            print('Execute grid search to determine parameters...')
            gs_res = grid_search(
                feature_selection=[('all', list(x_train.columns))],
                beam=beam,
                scale_data=['standard'],
                anomaly_detector=detector,
                detector_params={
                    'n_components': [1, 2, 4, 6, 8],
                    'covariance_type': ['full'],
                    'n_init': [1],
                    'init_params': ['kmeans'],
                    'verbose': [0]
                },
                labels=[('anomalies', labels)],
                segment_score=[segment_score],
                x_train=x_train,
                state_mode=state_mode_train,
                x_test=x_train
            )
            print('... end grid search')
            sorted_gs_res = sorted(gs_res, key=lambda res: res['auc'], reverse=True)
            best_gs = sorted_gs_res[0]

            # execute pipeline on test data with best params of training data
            if best_gs['auc'] == -1:  # something when wrong
                print('*** warning: grid search had no results, probably something is wrong ***')
                best_gs['scale'] = 'standard'
                best_gs['anomaly_detector'] = detector
                best_gs['params'] = parameters
                best_gs['segment_score'] = segment_score

            print('Execute pipeline with optimal parameters...')
            res = pipeline(
                beam=beam,
                scale_data=best_gs['scale'],
                anomaly_detector=best_gs['anomaly_detector'],
                detector_params=best_gs['params'],
                labels=labels,
                segment_score=best_gs['segment_score'],
                x_train=x_train,
                state_mode=state_mode_test,
                x_test=x_test,
                write_output=False
            )
            print('... end pipeline')
        else:
            raise ValueError('Mode should be pipeline or grid_search')

        res['truth_and_pred_df'].to_csv('live_truth_and_pred_df_' + str(iteration) + '.csv')

        # Evaluate iteration and store results
        TN, TP, FN, FP, _ = evaluation.cluster_predictions(res['truth_and_pred_df'], threshold, 'y_pred')

        # Print statistics of current iteration
        print('TN: {}'.format(len(TN)))
        print('TP: {}'.format(len(TP)))
        for s in TP:
            print(s)
        print('FP: {}'.format(len(FP)))
        for s in FP:
            print(s)
        print('FN: {}'.format(len(FN)))
        for s in FN:
            print(s)

        result = (iteration, test_start, test_end, res['auc'], res['rank'])

        return TN, TP, FN, FP, result
    except AssertionError as e:  # empty X_test
        print(e)
        df = pd.DataFrame()
        df.to_csv('live_truth_and_pred_df_' + str(iteration) + '.csv')


if __name__ == "__main__":
    warnings.filterwarnings("ignore", category=UserWarning)  # get rid of sklearn warnings

    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--beam', dest='beam', type=int, default=1, help='Beam (default: 1)')
    parser.add_argument('-y', '--year', dest='year', default='2017', help="Year: 2017 or 2018 (default: '2017')")
    parser.add_argument('-d', '--detector', dest='detector', default='gmm', help="Anomaly detector (default: 'gmm')")
    parser.add_argument('--feature_sel', dest='feature_sel', default='all',
                        help="Feature selection: 'all' or 'optimal' (default: 'all')")
    parser.add_argument('--host', dest='host', default='localhost', help='Host of MongoDB (default: localhost)')

    args = parser.parse_args()
    beam = args.beam
    year = args.year
    detector = args.detector
    ft_sel = args.feature_sel
    host = args.host
    db = CERNMongoClient(host=host, logging=False)
    segment_score = 'max'

    if detector == 'gmm':
        threshold = .99
    elif detector == 'iforest':
        threshold = .95
    else:
        raise Exception('Unsupported detector')

    if year == '2018':
        train_start = "2018-03-23 00:00:00"
        train_end = pd.to_datetime(train_start)
        nb_of_weeks = 36
        if ft_sel == 'all':
            feature_set = None
            parameters = {
                'n_components': 2,
                'covariance_type': 'full',
                'n_init': 5,
                'init_params': 'kmeans'
            }
        elif ft_sel == 'optimal':
            print("Optimal feature set")
            feature_set = ['MKI.A5L2.B1:TEMP_TUBE_DOWN_fft_freq_max',
                           'MKI.UA23.IPOC.CB1:T_DELAY',
                           'MKI.UA23.STATE:SOFTSTARTSTATE',
                           'MKI.UA23.F3.CONTROLLER:KICK_LENGTH_TOPLAY',
                           'MKI.UA23.STATE:CONTROL',
                           'MKI.B5L2.B1:PRESSURE:SW_MEAN_DIFF_600_s',
                           'MKI.B5L2.B1:TEMP_TUBE_UP',
                           'MKI.D5L2.B1:TEMP_MAGNET_UP:SW_MEAN_DIFF_600_s',
                           'MKI.A5L2.B1:TEMP_MAGNET_UP:SW_MEAN_DIFF_600_s',
                           'MKI.A5L2.B1:PRESSURE_fft_freq_percentage_intensity5',
                           'MKI.C5L2.B1:PRESSURE_fft_int_median',
                           'MKI.B5L2.B1:TEMP_TUBE_DOWN_fft_freq_max',
                           'MKI.B5L2.B1:PRESSURE_fft_freq_max',
                           'MKI.C5L2.B1:TEMP_TUBE_DOWN:SW_MEAN_DIFF_600_s',
                           'MKI.B5L2.B1:TEMP_MAGNET_UP_fft_freq_percentage_intensity5',
                           'MKI.A5L2.B1:PRESSURE:SW_MEAN_DIFF_600_s']
            parameters = {
                'n_components': 2,
                'covariance_type': 'full',
                'n_init': 1,
                'init_params': 'kmeans'
            }
        else:
            raise Exception("Error")
    elif year == '2017':
        train_start = "2017-04-12 00:00:00"
        train_end = pd.to_datetime(train_start)
        nb_of_weeks = 33
        if ft_sel == 'all':
            feature_set = None
            parameters = {
                'n_components': 8,
                'covariance_type': 'full',
                'n_init': 5,
                'init_params': 'kmeans'
            }
        elif ft_sel == 'optimal':
            feature_set = ['MKI.UA23.IPOC.CB1:T_DELAY', 'MKI.UA23.STATE:SOFTSTARTSTATE', 'MKI.UA23.STATE:CONTROL',
                           'MKI.B5L2.B1:TEMP_TUBE_UP', 'MKI.C5L2.B1:TEMP_TUBE_DOWN_fft_freq_max',
                           'MKI.D5L2.B1:PRESSURE', 'MKI.A5L2.B1:TEMP_MAGNET_DOWN:SW_MEAN_DIFF_600_s',
                           'MKI.A5L2.B1:TEMP_TUBE_UP:SW_MEAN_DIFF_600_s', 'MKI.C5L2.B1:PRESSURE',
                           'MKI.B5L2.B1:TEMP_TUBE_UP_fft_freq_max',
                           'MKI.B5L2.B1:TEMP_MAGNET_UP_fft_freq_percentage_intensity20',
                           'MKI.A5L2.B1:TEMP_MAGNET_UP_fft_freq_max']
            parameters = {
                'n_components': 2,
                'covariance_type': 'full',
                'n_init': 1,
                'init_params': 'kmeans'
            }
        else:
            raise Exception("Error")


    elif year == '2016':
        train_start = "2016-03-10 00:00:00"
        train_end = pd.to_datetime(train_start)
        nb_of_weeks = 35
        feature_set = None

        parameters = {
            'n_components': 1,
            'covariance_type': 'full',
            'tol': 1e-3,
            'reg_covar': 1e-6,
            'max_iter': 100,
            'n_init': 1,
            'init_params': 'kmeans',
            'verbose': 1,
        }
    else:
        raise AssertionError('Given year should be configured for live evaluation.')

    print('Using parameters {}'.format(parameters))
    print('Using threshold for predictions: {}'.format(threshold))
    print('Using feature set {}'.format('all' if feature_set is None else feature_set))

    configs = []
    for end_week in range(0, nb_of_weeks):
        train_end = train_end + pd.DateOffset(weeks=1)
        test_start = train_end
        test_end = train_end + pd.DateOffset(weeks=1)
        configs.append((train_start, str(train_end), str(test_start), str(test_end)))

    print('* Live evaluation: train_start, train_end, test_start, test_end *')
    for i, c in enumerate(configs):
        print(i, c)
    print('-------------------')

    # LOAD DATA
    features_df = builder.load_features_from_file(beam, kind='train' + year)
    state_mode = builder.load_state_mode_from_file(beam, kind='test' + year)
    logbook = builder.load_logbook_from_file(beam)
    labels = builder.anomalies(logbook)

    print('** STATIC LIVE EVALUATION **')
    live_evaluate(mode='static')

    # print('\n** ADAPTIVE LIVE EVALUATION **')
    # live_evaluate(mode='adaptive')
