#!/usr/bin/env python3

import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from database.fetch_data import *

if __name__ == '__main__':
    if len(sys.argv)>1:
        m = MongoDB(host=sys.argv[1])
    else:
        m = MongoDB()


    print("start reindexing on all collections ..")
    m.reindex_all()
    print(".. done\n")
