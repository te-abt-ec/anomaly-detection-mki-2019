#!/usr/bin/env python3

import argparse
import logging
import os
import sys
import time
import multiprocessing
import numpy as np
import pandas as pd
import warnings
from operator import itemgetter

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import util
from util import log_and_print
from pipeline import pipeline
from preprocessing import builder
from plot.features import plot_feature_selection


def get_neighboring_solutions(current_solution, all_features):
    """
    Get the neighboring solutions of a given solution.
    :param current_solution: The current solution
    :param all_features: A list with the names of all features
    :return: a list of feature sets close to the given solution
    """
    neighbors = []

    # Neighbor type 1: delete 1 feature
    if len(current_solution) > 1:
        for feature in current_solution:
            neighbors.append([f for f in current_solution if f != feature])

    # Neighbor type 2: add 1 feature
    for feature in [f for f in all_features if f not in current_solution]:
        neighbors.append(current_solution + [feature])

    return neighbors


def select_neighbor(neighbors, compute_score):
    """
    Given a number of neighboring solutions, evaluate the cost function
    and return the neighbor with the lowest cost.
    :param neighbors: A list of neighboring solutions. Each solution is a list of feature names.
    :param compute_score: A function used for computing the score of a neighboring solution.
    """
    if not neighbors:
        raise ValueError('No neighbors provided: {}'.format(neighbors))

    # Compute the score for each neighbor and append to solutions
    pool = multiprocessing.Pool()
    solutions = pool.map(compute_score, neighbors)
    pool.close()
    pool.join()

    # Sort solutions: best solution (highest score) first
    solutions.sort(key=itemgetter(0), reverse=higher_is_better)
    log_and_print('Best solutions')
    for score, _, solution in solutions:
        log_and_print('Score {:.2f} for features {}'.format(score, solution))

    best_score, best_df, best_solution = solutions[0]
    return best_solution, best_df, best_score


def compute_score_gmm(neighbor):
    """
    Compute the score of a neighbor. Higher score is better.
    :param neighbor: The neighboring solution for which to compute the score.
    :param scoring: The type of score to compute: auc or ranking.
    :return: The score of the neighbor.
    """
    try:
        res = pipeline.pipeline(
            beam=beam,
            scale_data=gmm_scale_data,
            anomaly_detector='gmm',
            detector_params=gmm_detector_params,
            labels=labels_anomalies,
            segment_score='max',
            x_train=df[neighbor],
            state_mode=state_mode
        )
    except ValueError:
        res = {'auc': -1., 'ranking': -1., 'truth_and_pred_df': pd.DataFrame()}

    log_and_print('AUC score {:.2f}, rank: {:.2f} for features {}'.format(res['auc'], res['rank'], neighbor))
    return res[scoring], res['truth_and_pred_df'], neighbor


def compute_score_isolation_forest(neighbor):
    # isolation forest sometimes fails with small # of features
    try:
        res = pipeline.pipeline(
            beam=beam,
            scale_data=iforest_scale_data,
            anomaly_detector="iforest",
            detector_params=iforest_detector_params,
            labels=labels_anomalies,
            segment_score='max',
            x_train=df[neighbor],
            state_mode=state_mode,
        )
    except ValueError:
        res = {'auc': -1., 'ranking': -1., 'truth_and_pred_df': pd.DataFrame()}

    log_and_print('AUC: {:.2f}, ranking: {:.2f} for features {}'.format(res['auc'], res['rank'], neighbor))
    return res[scoring], res['truth_and_pred_df'], neighbor


if __name__ == "__main__":
    warnings.filterwarnings("ignore", category=UserWarning)  # get rid of sklearn warnings

    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--beam', dest='beam', type=int, default=1, help='Beam (default: 1)')
    parser.add_argument('-d', '--detector', dest='detector', default='gmm', help='Anomaly detector (default: gmm)')
    parser.add_argument('-fntrn', '--filename_train', dest='filename_train', default='train')
    parser.add_argument('-fntst', '--filename_test', dest='filename_test', default='test')
    parser.add_argument('-p', '--params', dest='params', default='general', help='Hyperparameters to use (default: general)')
    parser.add_argument('-fno', '--filename_output', dest='filename_output', default='')
    parser.add_argument('-i', '--iterations', dest='iterations', type=int, default=5)
    args = parser.parse_args()

    beam = args.beam
    detector = args.detector
    fntrn = args.filename_train
    fntst = args.filename_test
    params = args.params
    fno = args.filename_output
    max_iterations = args.iterations

    scoring = 'rank'
    higher_is_better = False

    # Check arguments
    assert detector in util.DETECTORS, 'Povide a valid anomaly_detector'
    if detector == "gmm":
        compute_score = compute_score_gmm
    elif detector == "iforest":
        compute_score = compute_score_isolation_forest
    else:
        raise ValueError('anomaly_detector should be gmm or isolation_forest')

    # Set params of GMM
    if params == '2017':
        gmm_detector_params = {
            "n_components": 2,
            "covariance_type": 'full',
            "init_params": 'kmeans',
            "n_init": 1,
            "verbose": 0
        }
        gmm_scale_data = 'standard'
    elif params == '2018':
        gmm_detector_params = {
            "n_components": 2,
            "covariance_type": 'full',
            "init_params": 'kmeans',
            "n_init": 1,
            "verbose": 0
        }
        gmm_scale_data = 'standard'
    else:
        assert(params == 'general')
        gmm_detector_params = {
            "n_components": 2,
            "covariance_type": 'full',
            "init_params": 'kmeans',
            "n_init": 1,
            "verbose": 0
        }
        gmm_scale_data = 'standard'

    # Set params of iForest
    if params == '2017':
        iforest_detector_params = {
            "n_estimators": 100,
            "max_samples": 256,
            "contamination": 0.01,
            "max_features": 1.,
            "verbose": 0
        }
        iforest_scale_data = 'standard'
    elif params == '2018':
        iforest_detector_params = {
            "n_estimators": 100,
            "max_samples": 256,
            "contamination": 0.01,
            "max_features": 1.,
            "verbose": 0
        }
        iforest_scale_data = 'standard'
    else:
        assert(params=='general')
        iforest_detector_params = {
            "n_estimators": 100,
            "max_samples": 256,
            "contamination": 0.01,
            "max_features": 1.,
            "verbose": 0
        }
        iforest_scale_data = 'standard'

    # Configure logging
    base_filename = 'feature_selection_' + util.beam_to_str(beam) + '_' + detector + fno
    logging.basicConfig(filename=base_filename + '.log', level=logging.INFO)

    start_time = time.time()

    log_and_print("** Greedy search for feature selection **")
    log_and_print("  Max. number of iterations: {}".format(max_iterations))

    # Data consisting of all possible features
    df = builder.load_features_from_file(beam, kind=fntrn)
    state_mode = builder.load_state_mode_from_file(beam, kind=fntst)
    labels = builder.load_logbook_from_file(beam)
    labels_anomalies = builder.anomalies(labels)

    # Configure local search
    if beam == 1:
        solution = ['MKI.C5L2.B1:TEMP_MAGNET_UP_fft_freq_max', 'MKI.A5L2.B1:TEMP_TUBE_DOWN_fft_freq_max',
                    'MKI.UA23.IPOC.CB1:T_DELAY', 'LHC.BCTFR.A6R4.B1:BEAM_INTENSITY', 'MKI.UA23.STATE:SOFTSTARTSTATE',
                    'MKI.UA23.F3.CONTROLLER:KICK_LENGTH_TOPLAY', 'MKI.UA23.STATE:CONTROL',
                    'MKI.B5L2.B1:PRESSURE:SW_MEAN_DIFF_600_s', 'MKI.B5L2.B1:TEMP_TUBE_UP',
                    'MKI.B5L2.B1:TEMP_TUBE_UP:SW_MEAN_DIFF_600_s']
        exclude_features = []
    else:
        solution = ['MKI.C5R8.B2:TEMP_MAGNET_UP_fft_freq_max', 'MKI.C5R8.B2:TEMP_MAGNET_UP',
                    'MKI.A5R8.B2:TEMP_TUBE_DOWN_fft_freq_max', 'MKI.A5R8.B2:TEMP_TUBE_UP', 'MKI.UA87.IPOC.CB2:T_DELAY',
                    'MKI.UA87.IPOC.AB2:I_STRENGTH', 'LHC.BCTFR.A6R4.B2:BEAM_INTENSITY', 'MKI.UA87.STATE:SOFTSTARTSTATE',
                    'MKI.UA87.F3.CONTROLLER:KICK_LENGTH_TOPLAY', 'MKI.UA87.STATE:CONTROL']
        exclude_features = []

    all_features = list(df.columns)
    use_features = all_features.copy()
    for exclude in exclude_features:
        if exclude in use_features:
            use_features.remove(exclude)

    log_and_print('Initial solution {}'.format(solution))

    score = None
    truth_and_pred_df = None
    history = np.zeros(max_iterations)
    for i in range(0, max_iterations):
        log_and_print("| Iteration {}".format(i))
        neighbors = get_neighboring_solutions(solution, use_features)
        solution, truth_and_pred_df, score = select_neighbor(neighbors, compute_score)
        history[i] = score
        log_and_print('Best of iteration {}: score: {:.2f} for feature set {}'.format(i, score, solution))

    best_results_filename = base_filename + '-best.csv'
    truth_and_pred_df.to_csv(best_results_filename)

    plot_feature_selection(history, rank=scoring, filename=base_filename)

    log_and_print('| Final solution with score {:.2f}: {}'.format(score, solution))
    log_and_print('| Wrote best results to file {}'.format(best_results_filename))
    log_and_print("| Feature selection execution time: {} seconds".format(time.time() - start_time))
