#!/usr/bin/env python3

import argparse
import os
import pandas as pd
import sys
import warnings

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str

if __name__ == '__main__':
    warnings.filterwarnings("ignore", category=UserWarning)  # get rid of sklearn warnings

    # PARSE ARGUMENTS
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--beam', dest='beam', type=int, default=1, help='Beam (default: 1)')
    parser.add_argument('-fntrn', '--filename_train', dest='filename_train', default='train')
    parser.add_argument('-fntst', '--filename_test', dest='filename_test', default='test')
    parser.add_argument('-fno', '--filename_output', dest='filename_output', default='')

    extended_parser = parser.add_mutually_exclusive_group(required=False)
    extended_parser.add_argument('-e', '--extended', dest='extended', action='store_true')
    parser.set_defaults(extended=False)
    args = parser.parse_args()

    beam = args.beam
    extended = args.extended
    fntrn = args.filename_train
    fntst = args.filename_test
    fno = args.filename_output

    # LOAD DATA
    df = builder.load_features_from_file(beam, kind=fntrn)
    state_mode = builder.load_state_mode_from_file(beam, kind=fntst)
    logbook = builder.load_logbook_from_file(beam)
    labels = builder.anomalies(logbook)

    if extended:
        feature_selection = [('all', list(df.columns))]
        n_estimators = [50, 100, 200]
        max_samples = [256, 256 * 10, 256 * 20]
        contamination = [0.005, 0.01]
        max_features = [1.]
        scale_data = ['standard', 'none']
        segment_score = ['max']
    else:
        feature_selection = [('all', list(df.columns))]
        n_estimators = [100]
        max_samples = [256]
        contamination = [0.01]
        max_features = [1.]
        scale_data = ['standard']
        segment_score = ['max']

    # PERFORM GRID SEARCH
    results = grid_search(
        feature_selection=feature_selection,
        beam=beam,
        scale_data=scale_data,
        anomaly_detector='iforest',
        detector_params={
            'n_estimators': n_estimators,
            'max_samples': max_samples,
            'contamination': contamination,
            'max_features': max_features,
            'verbose': [0]
        },
        labels=labels,
        segment_score=segment_score,
        x_train=df,
        state_mode=state_mode,
        filename=fno
    )

    if len(results):
        features_res = [res['feature'] for res in results]
        auc_res = [res['auc'] for res in results]
        rank_res = [res['rank'] for res in results]
        scale = [res['scale'] for res in results]
        time_res = [res['time'] for res in results]

        n_estimators_res = [res['params']['n_estimators'] for res in results]
        max_samples_res = [res['params']['max_samples'] for res in results]
        contamination_res = [res['params']['contamination'] for res in results]
        max_features_res = [res['params']['max_features'] for res in results]

        statistics = pd.DataFrame(
            data={'auc': auc_res, 'rank': rank_res, 'features': features_res, 'n_estimators': n_estimators_res,
                  'max_samples': max_samples_res, 'contamination': contamination_res, 'max_features': max_features_res,
                  'scale': scale, 'time': time_res})
        statistics.to_csv('grid_search_statistics_' + beam_to_str(beam) + '_' + 'iforest' + fno + '.csv')
