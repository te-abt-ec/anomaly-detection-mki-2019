#!/usr/bin/env python3

import argparse
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from pipeline.pipeline import pipeline
from preprocessing import builder

if __name__ == "__main__":
    # PARSE ARGUMENTS
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--beam', dest='beam', type=int, default=1, help='Beam (default: 1)')
    parser.add_argument('-fntrn', '--filename_train', dest='filename_train', default='train')
    parser.add_argument('-fntst', '--filename_test', dest='filename_test', default='test')
    args = parser.parse_args()

    beam = args.beam
    fntrn = args.filename_train
    fntst = args.filename_test

    # LOAD DATA
    df = builder.load_features_from_file(beam=beam, kind=fntrn)
    state_mode = builder.load_state_mode_from_file(beam=beam, kind=fntst)
    logbook = builder.load_logbook_from_file(beam=beam)
    labels = builder.anomalies(logbook)

    # SELECT FEATURES
    selected_features = list(df.columns)

    # SET ANOMALY DETECTOR AND PARAMETERS
    pipeline(
        beam=beam,
        scale_data='standard',
        anomaly_detector='iforest',
        detector_params={
            "n_estimators": 100,
            "max_samples": 256,
            "contamination": 0.01,
            "max_features": 1.,
            "verbose": 1  # 0, 1, or 2
        },
        labels=labels,
        segment_score='max',
        x_train=df[selected_features],
        state_mode=state_mode)
