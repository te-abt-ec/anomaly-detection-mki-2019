#!/usr/bin/env python3

import argparse
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import util
from database.CERNMongoClient import CERNMongoClient
from preprocessing import builder

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--beam', dest='beam', type=int, default=1, help='Beam (default: 1)')
    parser.add_argument('--host', dest='host', default='localhost', help='Host of MongoDB (default: localhost)')
    parser.add_argument('-s', '--start', dest='start', type=str, default=util.TRAINTIME_EARLIEST)
    parser.add_argument('-e', '--end', dest='end', type=str, default=util.TRAINTIME_LATEST)
    parser.add_argument('-sw', '--sliding_window', dest='sliding_window', type=int, default=util.SLIDING_WINDOW_SIZE)
    parser.add_argument('-fntrn', '--filename_train', dest='filename_train', default='')
    parser.add_argument('-fntst', '--filename_test', dest='filename_test', default='')

    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--parallel', dest='parallel', action='store_true')
    feature_parser.add_argument('--no-parallel', dest='parallel', action='store_false')
    parser.set_defaults(parallel=False)
    args = parser.parse_args()

    beam = args.beam
    start = args.start
    end = args.end
    sw_size = args.sliding_window
    parallel = args.parallel
    fntrn = args.filename_train
    fntst = args.filename_test

    if parallel:
        db = args.host
    else:
        db = CERNMongoClient(host=args.host)

    # Build features for training the model
    builder.build_features_to_file(db, beam, kind='train' + fntrn, start=start, end=end, sliding_window_size=sw_size,
                                   parallel=parallel)

    if parallel:
        db = CERNMongoClient(host=args.host)
    builder.build_state_mode_to_file(db, beam, kind='test' + fntst, start=start, end=end)

    # Export logbook for the current beam
    builder.build_logbook_to_file(db, beam)
