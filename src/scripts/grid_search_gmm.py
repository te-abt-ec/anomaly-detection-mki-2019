#!/usr/bin/env python3

import argparse
import os
import pandas as pd
import sys
import warnings

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from pipeline.grid_search import grid_search
from preprocessing import builder
from util import beam_to_str

if __name__ == '__main__':
    warnings.filterwarnings("ignore", category=UserWarning)  # get rid of sklearn warnings

    # PARSE ARGUMENTS
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--beam', dest='beam', type=int, default=1, help='Beam (default: 1)')
    parser.add_argument('-fntrn', '--filename_train', dest='filename_train', default='train')
    parser.add_argument('-fntst', '--filename_test', dest='filename_test', default='test')
    parser.add_argument('-fno', '--filename_output', dest='filename_output', default='')

    extended_parser = parser.add_mutually_exclusive_group(required=False)
    extended_parser.add_argument('-e', '--extended', dest='extended', action='store_true')
    parser.set_defaults(extended=False)
    args = parser.parse_args()

    beam = args.beam
    extended = args.extended
    fntrn = args.filename_train
    fntst = args.filename_test
    fno = args.filename_output

    # LOAD DATA
    df = builder.load_features_from_file(beam, kind=fntrn)
    state_mode = builder.load_state_mode_from_file(beam, kind=fntst)
    logbook = builder.load_logbook_from_file(beam)
    labels = builder.anomalies(logbook)

    """ GMM parameters:
    * n_components: until 6 increasing performance, from 8 decreasing
    """

    if extended:
        feature_selection = [('all', list(df.columns))]
        n_components = [1, 2, 4, 6, 8]
        covariance_type = ['full', 'tied', 'diag']
        n_init = [1, 5]
        init_params = ['kmeans']
        scale_data = ['standard']
        segment_score = ['max']
        scoring = 'auc'
    else:
        feature_selection = [('all', list(df.columns))]
        n_components = [1, 2, 4, 8]
        covariance_type = ['full']
        n_init = [1]
        init_params = ['kmeans']
        scale_data = ['standard']
        segment_score = ['max']
        scoring = 'auc'

    # PERFORM GRID SEARCH
    results = grid_search(
        feature_selection=feature_selection,
        beam=beam,
        scale_data=scale_data,
        anomaly_detector='gmm',
        detector_params={
            'n_components': n_components,
            'covariance_type': covariance_type,
            'n_init': n_init,
            'init_params': init_params,
            'verbose': [0]
        },
        labels=labels,
        segment_score=segment_score,
        x_train=df,
        state_mode=state_mode,
        scoring=scoring,
        filename=fno
    )

    for res in results:
        filename = 'grid_search_' + beam_to_str(beam) + '_gmm_' + str(int(res['rank'])) + '-best.csv'
        res['truth_and_pred_df'].to_csv(filename)

    if len(results):
        features_res = [res['feature'] for res in results]
        auc_res = [res['auc'] for res in results]
        rank_res = [res['rank'] for res in results]
        scale = [res['scale'] for res in results]
        time_res = [res['time'] for res in results]

        n_components_res = [res['params']['n_components'] for res in results]
        cov_type = [res['params']['covariance_type'] for res in results]
        n_init = [res['params']['n_init'] for res in results]
        init_params = [res['params']['init_params'] for res in results]

        statistics = pd.DataFrame(
            data={'auc': auc_res, 'rank': rank_res, 'features': features_res, 'n_components': n_components_res,
                  'covariance_type': cov_type, 'n_init': n_init, 'init_params': init_params, 'scale': scale,
                  'time': time_res})
        statistics.to_csv('grid_search_statistics_' + beam_to_str(beam) + '_' + 'gmm' + fno + '.csv')
